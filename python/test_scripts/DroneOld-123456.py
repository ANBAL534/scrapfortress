import json
from random import randint
from time import sleep

# IMPORT FILES ABOVE THIS LINE
import game


class GameObject(game.Game, game.Drone):

    def __init__(self):
        super().__init__(game.Game.entity_id, game.Game.user_id)
        self.needs_recharge = False
        self.refinery = None
        self.drone_factory = None
        self.drone_role = None
        self.loaded_ore = False
        self.loaded_energy = False
        self.energy_needed_to_go_back = 0

    def start(self):
        # Search whole map for our refinery if any and set it in self.refinery
        game_map = json.loads(self.get_map())
        for y in game_map:
            if self.refinery is None:
                for x in y:
                    if x is not None:
                        try:
                            if x['user']['id'] == self.user_id and x['mapCode'] == 'R':
                                self.refinery = x
                            elif x['user']['id'] == self.user_id and x['mapCode'] == 'F':
                                self.drone_factory = x
                        except KeyError:
                            pass
            else:
                break

    def loop(self):
        position = json.loads(self.get_position())  # Always have our position updated and available

        if int(self.get_energy()) <= 0 or self.needs_recharge:
            self.recharge_energy_if_needed(position)
            return

        if self.refinery is None:  # Build a refinery if we haven't one yet
            self.build_refinery(position)
        elif self.drone_factory is None:
            self.build_drone_factory(position)
        else:
            if self.drone_role is None:
                self.decide_drone_role()
            if self.drone_role == 'miner':
                self.mine()
            elif self.drone_role == 'mover':
                self.mover()

    '''
    If our energy gets to 0, we will recharge it to 10 (spending all turns until charged)
    '''
    def recharge_energy_if_needed(self, position):
        if int(self.get_energy()) <= 0:
            self.needs_recharge = True
        if self.needs_recharge:
            self.recharge_at(int(position['x']), int(position['y']))
            if int(self.get_energy()) >= int(self.get_max_energy()):
                self.needs_recharge = False

    '''
    If self.refinery is None this drone will build it
    '''
    def build_refinery(self, position):
        if self.get_entity_at(x=int(position['x']) + 1, y=int(position['y'])) == 'null':
            # If the space on our right is empty, build our refinery
            self.start_build_at(x=int(position['x']) + 1, y=int(position['y']), build_code="R")
        else:
            # Refinery needs building, so select space on our right and build if it's our refinery
            ent = json.loads(self.get_entity_at(x=int(position['x']) + 1, y=int(position['y'])))
            if ent['mapCode'] == 'R' and int(ent['buildProgress']) > 0:  # It might be a wall or something
                response = self.build_at(x=int(position['x']) + 1, y=int(position['y']))
                if response != 'null':
                    ent = json.loads(self.get_entity_at(x=int(position['x']) + 1, y=int(position['y'])))
                    if int(ent['buildProgress']) == 0:
                        self.refinery = ent
            else:
                # We might be near a wall and our refinery wont be built, so move somewhere else and try again
                self.move_towards(0, 0)

    '''
    If self.drone_factory is None this drone will build it
    '''
    def build_drone_factory(self, position):
        if self.get_entity_at(x=int(position['x']) - 1, y=int(position['y'])) == 'null':
            # If the space on our left is empty, build our drone factory
            self.start_build_at(x=int(position['x']) - 1, y=int(position['y']), build_code="F")
        else:
            # drone factory needs building, so select space on our left and build if it's our drone factory
            ent = json.loads(self.get_entity_at(x=int(position['x']) - 1, y=int(position['y'])))
            if ent['mapCode'] == 'F' and int(ent['buildProgress']) > 0:  # It might be a wall or something
                response = self.build_at(x=int(position['x']) - 1, y=int(position['y']))
                if response != 'null':
                    ent = json.loads(self.get_entity_at(x=int(position['x']) - 1, y=int(position['y'])))
                    if int(ent['buildProgress']) == 0:
                        self.drone_factory = ent
            else:
                # We might be near a wall and our drone factory wont be built, so move somewhere else and try again
                self.move_towards(0, 0)

    def decide_drone_role(self):
        miner_number = 0
        mover_number = 0
        game_map = json.loads(self.get_map())
        for y in game_map:
            for x in y:
                try:
                    # Count here other type of drones in roles
                    if x['user']['id'] == self.user_id and x['mapCode'] == 'D' and 'miner' in x['dataStack']:
                        miner_number += 1
                    if x['user']['id'] == self.user_id and x['mapCode'] == 'D' and 'mover' in x['dataStack']:
                        mover_number += 1
                except (TypeError, KeyError) as e:
                    pass

        print('There are {} other miner drones.'.format(str(miner_number)))
        print('There are {} other mover drones.'.format(str(mover_number)))
        if mover_number == 0:
            print('{} drone will become a mover'.format(str(game.Game.entity_id)))
            self.drone_role = "mover"
            self.set_name("mover " + str(randint(0, 99999)))
            stack = json.loads(self.get_data_stack())
            stack.append("mover")
            self.set_data_stack(json.dumps(stack))
        elif miner_number < 5:
            print('{} drone will become a miner'.format(str(game.Game.entity_id)))
            self.drone_role = "miner"
            self.set_name("miner " + str(randint(0, 99999)))
            stack = json.loads(self.get_data_stack())
            stack.append("miner")
            self.set_data_stack(json.dumps(stack))
        elif mover_number < 3:
            print('{} drone will become a mover'.format(str(game.Game.entity_id)))
            self.drone_role = "mover"
            self.set_name("mover " + str(randint(0, 99999)))
            stack = json.loads(self.get_data_stack())
            stack.append("mover")
            self.set_data_stack(json.dumps(stack))

    def mine(self):
        position = json.loads(self.get_position())
        if not self.loaded_ore:  # If we are not loaded with ore we try to find closest and mine it
            closest_ore = None
            game_map = json.loads(self.get_map())
            for y in game_map:
                for x in y:
                    try:
                        if (x['mapCode'] == 'E') \
                                and (closest_ore is None
                                or distance_2_points(
                                         x['position']['x'], x['position']['y'], position['x'], position['y']) <
                                   distance_2_points(
                                         closest_ore['position']['x'], closest_ore['position']['y'], position['x'], position['y'])):
                            closest_ore = x
                    except (TypeError, KeyError) as e:
                        pass
            try:
                result = self.mine_energy_ore_at(closest_ore['position']['x'], closest_ore['position']['y'])
            except TypeError:
                self.move_towards(0, 0)
                result = -1
            if result != 'false' and int(result) == 0:
                self.loaded_ore = True
        else:  # If we are already loaded with ore we try to drop it at the refinery
            if distance_2_points(position['x'], position['y'], self.refinery['position']['x'], self.refinery['position']['y']) > 1:
                self.move_towards(self.refinery['position']['x'], self.refinery['position']['y'])
            else:
                if self.store_energy_ore_at(self.refinery['position']['x'], self.refinery['position']['y']) == 'true':
                    self.loaded_ore = False

    def mover(self):
        position = json.loads(self.get_position())
        if not self.loaded_energy:  # If we are not loaded with energy we try to find closest refinery and recharge it
            closest_refinery = None
            game_map = json.loads(self.get_map())
            for y in game_map:
                for x in y:
                    try:
                        if (x['mapCode'] == 'R') and (closest_refinery is None or distance_2_points(x['position']['x'], x['position']['y'], position['x'], position['y']) < distance_2_points(closest_refinery['position']['x'], closest_refinery['position']['y'], position['x'], position['y'])):
                            closest_refinery = x
                    except (TypeError, KeyError) as e:
                        pass
            try:
                result = self.recharge_at(closest_refinery['position']['x'], closest_refinery['position']['y'])
            except TypeError:
                self.move_towards(0, 0)
                result = -1
            if result != 'false' and int(result) >= 0:
                self.loaded_energy = True
                self.energy_needed_to_go_back = distance_2_points(position['x'], position['y'], self.drone_factory['position']['x'], self.drone_factory['position']['y'])
        else:  # If we are already recharged from the refinery, we want to drop the energy to the factory
            if self.store_energy_at(self.drone_factory['position']['x'], self.drone_factory['position']['y']) == 'true'\
                    and self.energy_needed_to_go_back == self.get_energy():
                self.loaded_energy = False


def distance_2_points(x1, y1, x2, y2):
    return abs(x1 - x2) + abs(y1 - y2)


def main():
    go = GameObject()
    go.start()
    while True:
        go.loop()
        go.turn()
        sleep(0.1)


if __name__ == '__main__':
    main()
