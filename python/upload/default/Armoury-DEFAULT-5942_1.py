from time import sleep

# IMPORT FILES ABOVE THIS LINE
import game


class GameObject(game.Game, game.DroneFactory):

    def __init__(self):
        super().__init__(game.Game.entity_id, game.Game.user_id)

    def start(self):
        return

    def loop(self):
        self.manufacture()


def main():
    go = GameObject()
    go.start()
    while True:
        go.loop()
        go.turn()
        sleep(0.1)


if __name__ == '__main__':
    main()
