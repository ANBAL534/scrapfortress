import json
from time import sleep

# IMPORT FILES ABOVE THIS LINE
import game


class GameObject(game.Game, game.Drone):

    def __init__(self):
        super().__init__(game.Game.entity_id, game.Game.user_id)
        self.needs_recharge = False
        self.drone_role = "NoRole"
        self.miner_recharge = False
        self.explorer_destination = None
        self.defender_destination = None
        self.armed = None
        self.wants_arms = True
        self.freshly_spawned = True

    def start(self):
        self.drone_role = self.get_role()

    def loop(self):
        if self.drone_role == "NoRole":
            self.drone_role = self.get_role()

        # if self.combine_with_a_drone_around():
        #     return  # This drone have just combined with another one and was consumed because of that
        if self.freshly_spawned_action():
            return  # This drone have just used it's turn to try and recharge as is freshly spawned
        self.recharge_energy_if_needed_action()
        self.expander_factory_role_action()
        self.expander_upgrades_role_action()
        self.expander_armoury_role_action()
        self.mover_role_action()
        self.miner_role_action()
        self.explorer_role_action()
        self.defender_role_action()
        self.raider_role_action()

    def freshly_spawned_action(self):
        if self.freshly_spawned:
            position = json.loads(self.get_position())
            try:
                refinery = self.get_closest_refinery()
                if refinery == "null":
                    self.freshly_spawned = False
                    return True
                refinery = json.loads(refinery)
            except TypeError:
                self.freshly_spawned = False
                return True
            if (int(refinery["position"]["x"]) - int(position["x"])) + (int(refinery["position"]["y"]) - int(position["y"])) <= 1:
                self.recharge_at(int(refinery['position']['x']), int(refinery['position']['y']))
                if int(self.get_energy()) >= int(self.get_max_energy()):
                    self.freshly_spawned = False
                return True
            else:
                result = self.move_towards(refinery["position"]["x"], refinery["position"]["y"])
                if result == "-2":
                    self.recharge_at(int(position['x']), int(position['y']))
            return True
        else:
            return False

    '''
    If our energy gets to 0, we will recharge it to max (spending all turns until charged)
    '''
    def recharge_energy_if_needed_action(self):
        position = json.loads(self.get_position())

        if int(self.get_energy()) <= 0:
            self.needs_recharge = True
        if self.needs_recharge:
            self.recharge_at(int(position['x']), int(position['y']))
            if int(self.get_energy()) >= int(self.get_max_energy()):
                self.needs_recharge = False

    def combine_with_a_drone_around(self):
        result = int(self.combine_around())
        if result == -1:
            return False
        else:
            return True

    def expander_factory_role_action(self):
        if self.drone_role == "ExpanderFactory":
            position = json.loads(self.get_position())
            expand_site = json.loads(self.get_nearest_free_expand_position())

            if expand_site == position:
                if self.get_entity_at(x=int(position['x']) + 1, y=int(position['y'])) == 'null':
                    # If the space on our right is empty, build our refinery
                    self.start_build_at(x=int(position['x']) + 1, y=int(position['y']), build_code="R")
                    return
                if json.loads(self.get_entity_at(x=int(position['x']) + 1, y=int(position['y'])))['mapCode'] == 'R':
                    # Now we choose between keep building the refinery or if already built, to build the factory
                    if json.loads(self.get_entity_at(x=int(position['x']) + 1, y=int(position['y'])))['buildProgress'] != 0:
                        result = self.build_at(x=int(position['x']) + 1, y=int(position['y']))
                        if result == "-2" or result == "0":
                            self.needs_recharge = True
                        return
                    elif self.get_entity_at(x=int(position['x']) - 1, y=int(position['y'])) == 'null':
                        # If the space on our right is empty, build our refinery
                        self.start_build_at(x=int(position['x']) - 1, y=int(position['y']), build_code="F")
                        return
                    elif json.loads(self.get_entity_at(x=int(position['x']) - 1, y=int(position['y'])))['mapCode'] == 'F':
                        # Now we choose between keep building the refinery or if already built, to build the factory
                        if json.loads(self.get_entity_at(x=int(position['x']) - 1, y=int(position['y'])))['buildProgress'] != 0:
                            result = self.build_at(x=int(position['x']) - 1, y=int(position['y']))
                            if result == "-2" or result == "0":
                                self.needs_recharge = True
                            return
                        else:
                            # If the factory is already built we get the mover role automatically
                            self.drone_role = "Mover"

            else:
                result = self.move_towards(expand_site['x'], expand_site['y'])
                if result == "-2":
                    self.needs_recharge = True

    def expander_upgrades_role_action(self):
        if self.drone_role == "ExpanderFactory":
            position = json.loads(self.get_position())
            expand_site = json.loads(self.get_nearest_free_expand_position())

            if expand_site == position:
                if self.get_entity_at(x=int(position['x']) + 1, y=int(position['y'])) == 'null':
                    # If the space on our right is empty, build our refinery
                    self.start_build_at(x=int(position['x']) + 1, y=int(position['y']), build_code="R")
                    return
                if json.loads(self.get_entity_at(x=int(position['x']) + 1, y=int(position['y'])))['mapCode'] == 'R':
                    # Now we choose between keep building the refinery or if already built, to build the factory
                    if json.loads(self.get_entity_at(x=int(position['x']) + 1, y=int(position['y'])))['buildProgress'] != 0:
                        result = self.build_at(x=int(position['x']) + 1, y=int(position['y']))
                        if result == "-2" or result == "0":
                            self.needs_recharge = True
                        return
                    elif self.get_entity_at(x=int(position['x']) - 1, y=int(position['y'])) == 'null':
                        # If the space on our right is empty, build our refinery
                        self.start_build_at(x=int(position['x']) - 1, y=int(position['y']), build_code="C")
                        return
                    elif json.loads(self.get_entity_at(x=int(position['x']) - 1, y=int(position['y'])))['mapCode'] == 'C':
                        # Now we choose between keep building the refinery or if already built, to build the factory
                        if json.loads(self.get_entity_at(x=int(position['x']) - 1, y=int(position['y'])))['buildProgress'] != 0:
                            result = self.build_at(x=int(position['x']) - 1, y=int(position['y']))
                            if result == "-2" or result == "0":
                                self.needs_recharge = True
                            return
                        else:
                            # If the factory is already built we get the mover role automatically
                            self.drone_role = "Mover"

            else:
                result = self.move_towards(expand_site['x'], expand_site['y'])
                if result == "-2":
                    self.needs_recharge = True

    def expander_armoury_role_action(self):
        if self.drone_role == "ExpanderArmory":
            position = json.loads(self.get_position())
            expand_site = json.loads(self.get_nearest_free_expand_position())

            if expand_site == position:
                if self.get_entity_at(x=int(position['x']) + 1, y=int(position['y'])) == 'null':
                    # If the space on our right is empty, build our refinery
                    self.start_build_at(x=int(position['x']) + 1, y=int(position['y']), build_code="R")
                    return
                if json.loads(self.get_entity_at(x=int(position['x']) + 1, y=int(position['y'])))['mapCode'] == 'R':
                    # Now we choose between keep building the refinery or if already built, to build the factory
                    if json.loads(self.get_entity_at(x=int(position['x']) + 1, y=int(position['y'])))['buildProgress'] != 0:
                        result = self.build_at(x=int(position['x']) + 1, y=int(position['y']))
                        if result == "-2" or result == "0":
                            self.needs_recharge = True
                        return
                    elif self.get_entity_at(x=int(position['x']) - 1, y=int(position['y'])) == 'null':
                        # If the space on our right is empty, build our refinery
                        self.start_build_at(x=int(position['x']) - 1, y=int(position['y']), build_code="A")
                        return
                    elif json.loads(self.get_entity_at(x=int(position['x']) - 1, y=int(position['y'])))['mapCode'] == 'A':
                        # Now we choose between keep building the refinery or if already built, to build the factory
                        if json.loads(self.get_entity_at(x=int(position['x']) - 1, y=int(position['y'])))['buildProgress'] != 0:
                            result = self.build_at(x=int(position['x']) - 1, y=int(position['y']))
                            if result == "-2" or result == "0":
                                self.needs_recharge = True
                            return
                        else:
                            # If the factory is already built we get the mover role automatically
                            self.drone_role = "Mover"

            else:
                result = self.move_towards(expand_site['x'], expand_site['y'])
                if result == "-2":
                    self.needs_recharge = True

    def mover_role_action(self):
        if self.drone_role == "Mover":
            position = json.loads(self.get_position())

            entity_right = self.get_entity_at(x=int(position['x']) + 1, y=int(position['y']))
            if entity_right == 'null' or json.loads(entity_right)["mapCode"] != 'R':
                destination = json.loads(self.get_needed_mover_position())  # TODO this can still be null
            else:
                destination = position

            if position != destination:
                result = self.move_towards(destination['x'], destination['y'])
                if result == "-2":
                    self.needs_recharge = True
            elif int(self.get_energy()) > 0:
                result = self.store_energy_at(x=int(position['x']) - 1, y=int(position['y']))
                if result == "-2" or result == "0":
                    # recharge at the refinery on its right
                    self.recharge_at(x=int(position['x']) + 1, y=int(position['y']))
            else:
                result = self.recharge_at(x=int(position['x']) + 1, y=int(position['y']))
                if result == "-2":
                    self.needs_recharge = True

    def miner_role_action(self):
        if self.drone_role == "EnergyOreMiner":
            assigned_refinery = self.get_assigned_refinery()  # We want to get first the assigned refinery so miners between 2 refineries do not fuse together

            if int(self.get_energy_ore()) < int(self.get_max_energy_ore()):
                # If we are not fully loaded up with ore, go for it
                closest_energy_ore = self.get_closest_energy_ore()
                if closest_energy_ore != 'null':
                    closest_energy_ore = json.loads(closest_energy_ore)
                    result = self.mine_energy_ore_at(closest_energy_ore['position']['x'], closest_energy_ore['position']['y'])
                    if result == "-2" or result == "0":
                        self.needs_recharge = True
            else:
                # If we are fully loaded with ore, we want to drop it at the assigned refinery
                if assigned_refinery != 'null':
                    assigned_refinery = json.loads(assigned_refinery)
                    result = self.store_energy_ore_at(assigned_refinery['position']['x'], assigned_refinery['position']['y'])
                    if result == "-2":
                        self.needs_recharge = True

    def explorer_role_action(self):
        if self.drone_role == "Explorer":
            if self.explorer_destination is None or self.explorer_destination == json.loads(self.get_position()):
                self.explorer_destination = json.loads(self.get_exploring_destination())

            entity_at_given_dest = self.get_entity_at(self.explorer_destination["x"], self.explorer_destination["y"])
            if entity_at_given_dest == "null":
                result = self.move_towards(self.explorer_destination["x"], self.explorer_destination["y"])
                if result == "-2":
                    self.needs_recharge = True
            else:
                self.explorer_destination = json.loads(self.get_exploring_destination())
                result = self.move_towards(self.explorer_destination["x"], self.explorer_destination["y"])
                if result == "-2":
                    self.needs_recharge = True

            self.mark_visible_entitites()

    def defender_role_action(self):
        if self.drone_role == "Defender":
            if not self.armed and self.wants_arms:
                closest_armory = self.get_closest_armoury()
                if closest_armory != "null":
                    closest_armory = json.loads(closest_armory)
                    result = self.get_armed_at(closest_armory["position"]["x"], closest_armory["position"]["y"])
                    if result == "1":
                        self.armed = True
                    elif result == "-2":
                        self.needs_recharge = True
                else:
                    self.wants_arms = False  # If no armoury is present at fist try go on without armor

            if self.defender_destination is None or self.defender_destination == json.loads(self.get_position()):
                self.defender_destination = json.loads(self.get_patrol_point())

            closest_enemy = self.get_closest_enemy()
            if closest_enemy == "null":
                self.defender_destination = json.loads(self.get_patrol_point())
                result = self.move_towards(self.defender_destination["x"], self.defender_destination["y"])
                if result == "-2":
                    self.needs_recharge = True
            else:
                closest_enemy = json.loads(closest_enemy)
                result = self.attack_at(closest_enemy["position"]["x"], closest_enemy["position"]["y"])
                if result == "-2" or result == "0":
                    self.needs_recharge = True

            self.mark_visible_entitites()

    def raider_role_action(self):
        if self.drone_role == "Raider":
            if not self.armed and self.wants_arms:
                closest_armory = self.get_closest_armoury()
                if closest_armory != "null":
                    closest_armory = json.loads(closest_armory)
                    result = self.get_armed_at(closest_armory["position"]["x"], closest_armory["position"]["y"])
                    if result == "1":
                        self.armed = True
                    elif result == "-2":
                        self.needs_recharge = True
                else:
                    self.wants_arms = False  # If no armoury is present at fist try go on without armor

            closest_enemy = self.get_closest_enemy()
            if closest_enemy == "null":
                # No enemies on sight, go for the last known position of an enemy
                last_enemy_pos = self.get_closest_last_enemy_position_known()
                if last_enemy_pos != "null":  # We might not have seen any enemies yet
                    last_enemy_pos = json.loads(last_enemy_pos)
                    result = self.move_towards(last_enemy_pos["x"], last_enemy_pos["y"])
                    if result == "-2":
                        self.needs_recharge = True
                    return
            else:
                # If we have a enemy on sight, go kill it
                closest_enemy = json.loads(closest_enemy)
                result = self.attack_at(closest_enemy["position"]["x"], closest_enemy["position"]["y"])
                if result == "-2" or result == "0":
                    self.needs_recharge = True

            self.mark_visible_entitites()



def distance_2_points(x1, y1, x2, y2):
    return abs(x1 - x2) + abs(y1 - y2)


def main():
    go = GameObject()
    go.start()
    while True:
        go.loop()
        go.turn()
        sleep(0.1)


if __name__ == '__main__':
    main()
