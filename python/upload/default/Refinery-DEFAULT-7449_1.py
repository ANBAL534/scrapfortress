# IMPORT FILES ABOVE THIS LINE
import game


class GameObject(game.Game, game.Refinery):

    def __init__(self):
        super().__init__(game.Game.entity_id, game.Game.user_id)

    def start(self):
        return

    def loop(self):
        self.refine()
