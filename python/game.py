import inspect
import socket


class Game:
    entity_id = None  # These won't be used as will be overwritten at upload by hard-coded constants
    user_id = None

    def __init__(self, entity_id, user_id):
        self.entity_id = entity_id
        self.user_id = user_id

    def start(self):
        """
        Override this method
        Executed once after loading the class for the first time
        :return:
        """
        return

    def loop(self):
        """
        Override this method
        Executed once every turn in an infinite loop
        :return:
        """
        return

    def get_map(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    # Remove this method before production!
    def turn(self):
        comms("Caller", self.entity_id, self.user_id)


class Entity:

    def __init__(self, entity_id, user_id):
        self.entity_id = entity_id
        self.user_id = user_id

    def get_id(self):
        return self.entity_id

    def get_user_id(self):
        return self.user_id

    def get_name(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def set_name(self, name):
        return comms(__class__.__name__, self.entity_id, self.user_id, name=name)

    def is_walkable(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_position(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_life(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_max_life(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_entity_at(self, x, y):
        return comms(__class__.__name__, self.entity_id, self.user_id, x=x, y=y)

    def get_data_stack(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    '''
    data_stack must be a json array of strings
    '''
    def set_data_stack(self, data_stack):
        return comms(__class__.__name__, self.entity_id, self.user_id, data_stack=data_stack)


class Drone(Entity):

    def __init__(self, entity_id, user_id):
        super().__init__(entity_id, user_id)

    def get_energy(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_max_energy(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_attack(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_repair(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_energy_ore(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_max_energy_ore(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_shortest_path(self, x, y):
        return comms(__class__.__name__, self.entity_id, self.user_id, x=x, y=y)

    def get_role(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_nearest_free_expand_position(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_needed_mover_position(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_closest_energy_ore(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_patrol_point(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_exploring_destination(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_closest_armoury(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_assigned_refinery(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_closest_enemy(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_closest_refinery(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def mark_visible_entitites(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_closest_last_enemy_position_known(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def combine_around(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def move(self, x, y):
        return comms(__class__.__name__, self.entity_id, self.user_id, x=x, y=y)

    def move_towards(self, x, y):
        return comms(__class__.__name__, self.entity_id, self.user_id, x=x, y=y)

    def recharge_at(self, x, y):
        return comms(__class__.__name__, self.entity_id, self.user_id, x=x, y=y)

    def get_armed_at(self, x, y):
        return comms(__class__.__name__, self.entity_id, self.user_id, x=x, y=y)

    '''
    Will try to mine ore at the specified position or will move towards it if too far away.
    @:return Nomber of units mined or -1 if we were too far away. false if we have already used our turn.
    '''
    def mine_energy_ore_at(self, x, y):
        return comms(__class__.__name__, self.entity_id, self.user_id, x=x, y=y)

    def build_at(self, x, y):
        return comms(__class__.__name__, self.entity_id, self.user_id, x=x, y=y)

    def start_build_at(self, x, y, build_code):
        return comms(__class__.__name__, self.entity_id, self.user_id, x=x, y=y, code=build_code)

    def store_energy_at(self, x, y):
        return comms(__class__.__name__, self.entity_id, self.user_id, x=x, y=y)

    def store_energy_ore_at(self, x, y):
        return comms(__class__.__name__, self.entity_id, self.user_id, x=x, y=y)

    def attack_at(self, x, y):
        return comms(__class__.__name__, self.entity_id, self.user_id, x=x, y=y)

    def repair_at(self, x, y):
        return comms(__class__.__name__, self.entity_id, self.user_id, x=x, y=y)


class Refinery(Entity):

    def __init__(self, entity_id, user_id):
        super().__init__(entity_id, user_id)

    def get_building_progress(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_energy(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_max_energy(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_energy_ore(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def refine(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)


class DroneFactory(Entity):

    def __init__(self, entity_id, user_id):
        super().__init__(entity_id, user_id)

    def get_building_progress(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_energy(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_max_energy(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_drone_progress(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_energy_for_drone(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def manufacture(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)


class Armoury(Entity):

    def __init__(self, entity_id, user_id):
        super().__init__(entity_id, user_id)

    def get_building_progress(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_energy(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_max_energy(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_equipments(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_equipment_progress(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def get_energy_for_equipment(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)

    def manufacture(self):
        return comms(__class__.__name__, self.entity_id, self.user_id)


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.settimeout(1000.555)
s.connect(("localhost", 8012))
def comms(klass, entity_id, user_id, **kwargs):
    # Entity_class_name-entity_id-user_id-function-argument1-argument2...
    base_request = '{0}-{1}-{2}-{3}'.format(str(klass), str(entity_id), str(user_id), inspect.stack()[1][3])
    for key, value in kwargs.items():
        base_request += '-' + str(value)
    base_request += '\n'
    s.sendall(bytes(base_request, 'utf8'))
    try:
        data = recv_end(s)
    except socket.timeout:
        raise TimeoutError(str(entity_id))
    # s.sendall(bytes("END\n", 'utf8'))
    return str(data)


End = '\n'
def recv_end(the_socket):
    total_data = []
    data = ''
    while True:
        data = str(the_socket.recv(1), 'utf8')
        if data == '\r':
            continue
        if End in data:
            total_data.append(data[:data.find(End)])
            break
        total_data.append(data)
        if len(total_data) > 1:
            # check if end_of_data was split
            last_pair = total_data[-2] + total_data[-1]
            if End in last_pair:
                total_data[-2] = last_pair[:last_pair.find(End)]
                total_data.pop()
                break
    return ''.join(total_data)
