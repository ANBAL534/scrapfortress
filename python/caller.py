import os
import random
import sys
import time
from importlib import reload
from json import JSONDecodeError
from os import listdir
from time import sleep

from game import comms


class Turn:
    upload_dirs = "upload"
    objects = []  # List of dictionaries with form: {"filename": filename, "object": Object()}
    existing_filenames = {}
    game_paused = False

    def __init__(self):
        self.available_folders = []
        for folder_name in sys.argv[1].split(","):
            self.available_folders.append(folder_name)

    @staticmethod
    def current_milli_time():
        return int(round(time.time() * 1000))

    @staticmethod
    def get_class(kls):
        parts = kls.split('.')
        module = ".".join(parts[:-1])
        m = __import__(module)
        reload(m)
        for comp in parts[1:]:
            m = getattr(m, comp)
        return m

    def turn(self, turn_number):
        start_load = self.current_milli_time()
        existing_files = []
        for dirname in listdir(self.upload_dirs):
            if dirname in self.available_folders:
                for modname in listdir(os.path.join(self.upload_dirs, dirname)):
                    if modname.endswith(".py") and "-DEFAULT-" not in modname:
                        existing_files.append(modname)

                        if modname not in self.existing_filenames:
                            self.existing_filenames[modname] = turn_number
                        # Check if there is an already loaded class of the file in objects
                        loaded = False
                        for dict_entities in self.objects:
                            try:
                                if dict_entities["filename"] == modname:
                                    loaded = True
                            except KeyError:
                                pass
                        if not loaded:
                            # content = file.read()
                            # Remove old dict entry if any and add new entry with updated data and Object()
                            for i, dict_entities in enumerate(self.objects):
                                if dict_entities["filename"] == modname:
                                    self.objects.pop(i)
                                    print("Removed (old) {0}".format(modname))
                                    break
                            klass = self.get_class(
                                "{0}.{1}.{2}.GameObject".format(self.upload_dirs, dirname, modname.replace(".py", "")))
                            entity_object = klass()
                            self.objects.append({"filename": modname, "object": entity_object, "paused": False})
                            print("Added {0}".format(modname))
                            try:
                                entity_object.start()  # Execute start routine for new object
                            except Exception as e:
                                print(str(e))
                            print("{0}.{1}.{2}.GameObject start() function executed".format(self.upload_dirs, dirname,
                                                                                            modname.replace(".py", "")))
        end_load = self.current_milli_time()

        # Remove objects in the dictionary but not in the existing_files array
        for dict_entities in self.objects:
            if dict_entities["filename"] not in existing_files:
                self.objects.pop(self.objects.index(dict_entities))
                print("Entity ({}) removed from objects array".format(str(dict_entities["filename"])))

        # After updating upload files we execute all loops
        start_loop = self.current_milli_time()
        random.shuffle(self.objects)  # shuffle the dictionaries so the turn order is random
        for dict_entities in self.objects:
            status = 'OK'
            if not dict_entities["paused"]:  # After calling to reset turn for the entity, we check if the game is paused depending on the response on the reset
                try:
                    dict_entities["object"].loop()
                except TimeoutError:
                    pass  # Some entity have died before it's loop turn, ignore as it will be removed next turn
                except JSONDecodeError:
                    pass  # Some entity have died before it's loop turn, ignore as it will be removed next turn
                except ValueError:
                    pass  # Some entity have died before it's loop turn, ignore as it will be removed next turn
                # print("{0}.GameObject loop() function executed".format(dict_entities["filename"].replace(".py", "")))
        end_loop = self.current_milli_time()

        # After executing all loops we reset turns for all entities
        start_reset = self.current_milli_time()

        def turn():
            for dict_entities in self.objects:
                if comms("Caller", dict_entities["object"].entity_id, dict_entities["object"].user_id) == "OK":
                    dict_entities["paused"] = False
                    self.game_paused = False
                elif comms("Caller", dict_entities["object"].entity_id, dict_entities["object"].user_id) == "PAUSED":
                    dict_entities["paused"] = True
                    self.game_paused = True

        def all_turn():
            try:
                if comms("Caller", self.objects[0]["object"].entity_id, self.objects[0]["object"].user_id) == "OK":
                    self.game_paused = False
                elif comms("Caller", self.objects[0]["object"].entity_id, self.objects[0]["object"].user_id) == "PAUSED":
                    self.game_paused = True

                for dict_entities in self.objects:
                    if not self.game_paused:
                        dict_entities["paused"] = False
                    else:
                        dict_entities["paused"] = True
            except IndexError:
                return

        if self.game_paused:
            turn()
        else:
            all_turn()

        end_reset = self.current_milli_time()

        if len(self.objects) == 0:
            print("Game ended")
            exit(0)

    #     print('''
    # Turn {} statistics for {} entities:
    #     - Loading new python files took: {}ms
    #     - Executing entities turns took: {}ms
    #     - Resetting entities turns took: {}ms
    #     '''.format(str(turn_number), str(len(self.objects)), str(end_load - start_load), str(end_loop - start_loop),
    #                str(end_reset - start_reset)))


def main():
    turn_object = Turn()

    turn_counter = 0
    try:
        os.mkdir(turn_object.upload_dirs)
    except FileExistsError:
        pass
    print("Caller started...")
    while True:
        start_turn = turn_object.current_milli_time()

        # try:
        #     turn_object.turn(turn_counter)
        # except TypeError:
        #     print("TypeError detected, ignoring, game may have ended...")
        #     pass  # As the game may have ended in the middle of something in a loop
        turn_object.turn(turn_counter)

        turn_counter += 1
        end_turn = turn_object.current_milli_time()
        print("Turn executed. Took " + str(end_turn - start_turn) + "ms")
        runsecs = float(end_turn - start_turn) / float(1000)
        try:
            if "afap" not in sys.argv:
                try:
                    sleep(0.25 - runsecs)
                except ValueError:
                    pass

            if "afap" in sys.argv and runsecs < 0.001:
                sleep(50)
        except IndexError:
            try:
                sleep(0.25 - runsecs)
            except ValueError:
                pass


if __name__ == '__main__':
    main()
