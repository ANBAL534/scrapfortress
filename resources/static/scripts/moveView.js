function moveView(direction) {
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState === XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
            if (xmlhttp.status === 200) {
                updateCanvas();
            }
            else if (xmlhttp.status === 400) {
                alert('There was an error 400');
            }
            else {
                alert('something else other than 200 was returned');
            }
        }
    };

    if (direction === 'up') {
        xmlhttp.open("GET", "/move_cam_up", true);
    } else if (direction === 'down') {
        xmlhttp.open("GET", "/move_cam_down", true);
    } else if (direction === 'left') {
        xmlhttp.open("GET", "/move_cam_left", true);
    } else if (direction === 'right') {
        xmlhttp.open("GET", "/move_cam_right", true);
    } else if (direction === 'lessFow') {
        xmlhttp.open("GET", "/less_fow", true);
    } else if (direction === 'moreFow') {
        xmlhttp.open("GET", "/more_fow", true);
    } else if (direction === 'pause') {
        xmlhttp.open("GET", "/pause_game", true);
    } else if (direction === 'resume') {
        xmlhttp.open("GET", "/resume_game", true);
    }
    xmlhttp.send();
}