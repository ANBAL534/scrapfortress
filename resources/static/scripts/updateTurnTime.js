function modifyTime(parsed) {
    var c = document.getElementById('turn_time');
    if (c.innerHTML !== parsed)
        c.innerHTML = parsed + "ms average turn time";
}

function updateTime() {
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState === XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
            if (xmlhttp.status === 200) {
                modifyTime(xmlhttp.responseText);
            }
            else if (xmlhttp.status === 400) {
                alert('There was an error 400');
            }
            else {
                alert('something else other than 200 was returned');
            }
        }
    };

    xmlhttp.open('GET', '/average_turn_time', true);
    xmlhttp.send();
}

function bucleTT() {
    var delay = ( function() {
        var timer = 0;
        return function(callback, ms) {
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();
    delay(function(){
        updateTime();
        bucleTT();
    }, 600 ); // end delay
}

bucleTT();