function drawCanvas(parsedMap, parsedEnemyMap, parsedLastSeenMap) {
    var c = document.getElementById('map');
    var ctx = c.getContext('2d');
    var images = {};

    ctx.drawImage(document.getElementById('reset-image'),0,0);

    for (eCode in parsedMap.eCodes) {
        images[parsedMap.eCodes[eCode]] = document.getElementById(parsedMap.eCodes[eCode] + '-letter');
    }

    var x = 1;
    var y = 1;
    var startxMap = parsedMap.startx;
    var startyMap = parsedMap.starty;
    var lines = parsedMap.mapString.split('\n');
    var enemyLines = parsedEnemyMap.mapString.split('\n');
    var lastSeenLines = parsedLastSeenMap.mapString.split('\n');
    for (line in lines) {
        for (letter in lines[line]) {
            var code = '';
            if (lines[line][letter] === ' ')
                code = "Space";
            else
                code = lines[line][letter] + "";
            ctx.drawImage(images[code],10*x,10*y, 10, 10);

            if (enemyLines[line][letter] === 'X')
                ctx.drawImage(document.getElementById('enemy-overlay'),10*x,10*y, 10, 10);
            else if (lastSeenLines[line][letter] === '?')
                ctx.drawImage(document.getElementById('enemy-last-seen'),10*x,10*y, 10, 10);

            if (y === 1)
                ctx.fillText(x+startxMap-2, 10*x+1, 10*y, 7)
            else
                ctx.fillText("+", 10*x+2, 10*y-1, 10)

            x++;
        }
        ctx.fillText(y+startyMap-2, 10*x, 10*y, 10)
        y++;
        x = 1;
    }
}

function updateCanvas() {
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState === XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
            if (xmlhttp.status === 200) {
                if (xmlhttp.responseText === 'Game Finished') {
                    window.location = "/menu"
                } else {
                    //drawCanvas(JSON.parse(xmlhttp.responseText));
                    getEnemyMap(JSON.parse(xmlhttp.responseText));
                }
            }
            else if (xmlhttp.status === 400) {
                alert('There was an error 400');
            }
            else {
                alert('something else other than 200 was returned');
            }
        }
    };

    xmlhttp.open("GET", "/get_map", true);
    xmlhttp.send();
}

function getEnemyMap(parsedMap) {
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState === XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
            if (xmlhttp.status === 200) {
                if (xmlhttp.responseText === 'Game Finished') {
                    window.location = "/menu";
                } else {
                    getLastSeenMap(parsedMap, JSON.parse(xmlhttp.responseText));
                }
            }
            else if (xmlhttp.status === 400) {
                alert('There was an error 400');
            }
            else {
                alert('something else other than 200 was returned');
            }
        }
    };

    xmlhttp.open("GET", "/get_enemy_map", true);
    xmlhttp.send();
}

function getLastSeenMap(parsedMap, parsedEnemyMap) {
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState === XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
            if (xmlhttp.status === 200) {
                if (xmlhttp.responseText === 'Game Finished') {
                    window.location = "/menu";
                } else {
                    drawCanvas(parsedMap, parsedEnemyMap, JSON.parse(xmlhttp.responseText));
                }
            }
            else if (xmlhttp.status === 400) {
                alert('There was an error 400');
            }
            else {
                alert('something else other than 200 was returned');
            }
        }
    };

    xmlhttp.open("GET", "/get_last_seen_map", true);
    xmlhttp.send();
}

function bucleC() {
    var delay = ( function() {
        var timer = 0;
        return function(callback, ms) {
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();
    delay(function(){
        updateCanvas();
        bucleC();
    }, 150 ); // end delay
}

bucleC();