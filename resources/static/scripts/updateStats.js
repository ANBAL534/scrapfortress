function modifyListStats(parsed) {
    var c = document.getElementById('statsList');
    if (c.innerHTML !== parsed)
        c.innerHTML = parsed;
}

function updateStats() {
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState === XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
            if (xmlhttp.status === 200) {
                modifyListStats(xmlhttp.responseText);
            }
            else if (xmlhttp.status === 400) {
                alert('There was an error 400');
            }
            else {
                alert('something else other than 200 was returned');
            }
        }
    };

    xmlhttp.open('GET', '/entityStats', true);
    xmlhttp.send();
}

function bucleFS() {
    var delay = ( function() {
        var timer = 0;
        return function(callback, ms) {
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();
    delay(function(){
        updateStats();
        bucleFS();
    }, 600 ); // end delay
}

bucleFS();