function modifyList(parsed) {
    var c = document.getElementById('formList');
    if (c.innerHTML !== parsed)
        c.innerHTML = parsed;
}

function updateForm() {
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState === XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
            if (xmlhttp.status === 200) {
                modifyList(xmlhttp.responseText);
            }
            else if (xmlhttp.status === 400) {
                alert('There was an error 400');
            }
            else {
                alert('something else other than 200 was returned');
            }
        }
    };

    xmlhttp.open("GET", "/uploadPy", true);
    xmlhttp.send();
}

function bucleF() {
    var delay = ( function() {
        var timer = 0;
        return function(callback, ms) {
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();
    delay(function(){
        updateForm();
        bucleF();
    }, 600 ); // end delay
}

bucleF();