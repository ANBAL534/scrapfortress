package Game.server

import Game.entity.*
import Game.entity.roles.RolesEnum
import Game.world.Coordinates
import Game.world.Dungeon
import Web.users.User
import Web.utils.TimeFormat
import com.beust.klaxon.Converter
import com.beust.klaxon.DefaultConverter
import com.beust.klaxon.JsonValue
import com.beust.klaxon.Klaxon
import kotlinx.html.currentTimeMillis
import java.io.BufferedReader
import java.io.PrintWriter
import java.net.ServerSocket
import java.net.SocketException
import kotlin.concurrent.thread
import kotlin.random.Random
import kotlin.system.measureTimeMillis


/**
 * Will listen for connections and execute commands to drones or returning information on demand of the user's scripts
 */
class Server: Thread() {

    val PORT = 8012

    override fun run () {
        val server = ServerSocket(PORT)
        val klaxon = Klaxon()
        println("${TimeFormat.formattedTime} Game server started at port $PORT")
        while (true) {
            val connection = server.accept()  // Accept a caller.py connection
            println("${TimeFormat.formattedTime} Server connection accepted")
            thread {  // Every caller.py will have it's own thread
                var startTime: Long = currentTimeMillis()
                
                val connection = connection
                val printWriter = PrintWriter(connection.getOutputStream(), true)
                val br = BufferedReader(connection.getInputStream().reader())
                while (true) {
                    lateinit var received: String
                    try {
                        received = br.readLine()  // Entity_class_name-entity_id-user_id-function-argument1-argument2...
                    } catch (e: SocketException) {
                        println(e.message)
                        break
                    } catch (e: IllegalStateException) {
                        println(e.message)
                        break
                    }
                    if (received == "END") {
                        connection.close()
                        break
                    }
                    val splitted = received.split("-")
                    val klassName = splitted[0]
                    val entityId = splitted[1].toInt()
                    val userId = splitted[2].toInt()
                    val function = splitted[3]
                    val arguments = mutableListOf<String>()
                    if (splitted.size > 4) {  // It has arguments
                        for (i in 4 until splitted.size)
                            arguments.add(splitted[i])
                    }

                    val dungeon = try {
                            Dungeon.findDungeonFromEntity(Entity.findEntityById(entityId)!!)!!
                        } catch (e: Exception) {
                            printWriter.println("DEAD")
                            continue
                        }
                    val dungeonLock = dungeon.dungeonLock

                    dungeonLock.lock()

                    val took = measureTimeMillis {
                        try {
                            when (klassName) {
                                "Entity" ->
                                    when (function) {
                                        // Entity
                                        "get_name" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon)!!
                                            val result = entityFound.name
                                            printWriter.println(entityFound.name)
                                            //println("${TimeFormat.formattedTime} get_name -> $result")
                                        }
                                        "set_name" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon)!!
                                            val old = entityFound.name
                                            entityFound.name = arguments[0]
                                            printWriter.println(entityFound.name)
                                            //println("${TimeFormat.formattedTime} set_name -> $old (old name) to ${entityFound.name} (new name)")
                                        }
                                        "is_walkable" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon)!!
                                            val result = entityFound.isWalkable.toString()
                                            printWriter.println(entityFound.isWalkable.toString())
                                            //println("${TimeFormat.formattedTime} is_walkable -> $result")
                                        }
                                        "get_position" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon)!!
                                            val result = klaxon.toJsonString(entityFound.position)
                                            printWriter.println(result)
                                            //println("${TimeFormat.formattedTime} get_position -> $result")
                                        }
                                        "get_life" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon)!!
                                            val result = entityFound.life
                                            printWriter.println(result.toString())
                                            //println("${TimeFormat.formattedTime} get_life -> $result")
                                        }
                                        "get_max_life" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon)!!
                                            val result = entityFound.maxLife
                                            printWriter.println(result.toString())
                                            //println("${TimeFormat.formattedTime} get_max_life -> $result")
                                        }
                                        "get_entity_at" -> {
                                            val callerEntity = Entity.findEntityById(entityId, dungeon)!!
                                            val took = measureTimeMillis {
                                                var entityFound = callerEntity.findEntityByPosition(
                                                    Coordinates.getCoordinates(
                                                        arguments[0].toInt(),
                                                        arguments[1].toInt(),
                                                        0
                                                    )
                                                )
                                                if (!Entity.findEntitiesByUserId(userId).any { it.isAround(
                                                        Coordinates.getCoordinates(
                                                            arguments[0].toInt(),
                                                            arguments[1].toInt(),
                                                            0
                                                        )
                                                    ) })
                                                    entityFound = null
                                                val result =
                                                    if (entityFound == null)
                                                        null
                                                    else
                                                        klaxon.toJsonString(entityFound)
                                                printWriter.println(result)
                                                //println("${TimeFormat.formattedTime} get_entity_at -> $result")
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} get_entity_at of drone ${callerEntity.id} took $took ms")
                                            }
                                        }
                                        "get_data_stack" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon)!!
                                            val result =
                                                if (userId != entityFound.user?.id ?: -1)
                                                    klaxon.toJsonString(listOf<String>())
                                                else
                                                    klaxon.toJsonString(entityFound.dataStack)
                                            printWriter.println(result)
                                            //println("${TimeFormat.formattedTime} get_data_stack -> $result")
                                        }
                                        "set_data_stack" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon)!!
                                            val old = entityFound.dataStack
                                            entityFound.dataStack =
                                                if (userId != entityFound.user?.id ?: -1)
                                                    old
                                                else
                                                    klaxon.parseArray<String>(arguments[0])!!.toMutableList()
                                            printWriter.println(entityFound.dataStack)
                                            dungeon.gridChanges[entityFound.user]!!.apply { remove(entityFound) }.add(entityFound)
                                            //println("${TimeFormat.formattedTime} set_data_stack -> ${klaxon.toJsonString(entityFound.dataStack)} (new stack)")
                                        }
                                    }
                                "Drone" ->
                                    when (function) {
                                        // Drone
                                        "get_energy" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone
                                            val result = entityFound.storedEnergy
                                            printWriter.println(result.toString())
                                            //println("${TimeFormat.formattedTime} get_energy -> $result")
                                        }
                                        "get_max_energy" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone
                                            val result = entityFound.maxEnergy
                                            printWriter.println(result.toString())
                                            //println("${TimeFormat.formattedTime} get_max_energy -> $result")
                                        }
                                        "get_attack" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone
                                            val result = entityFound.maxAttack
                                            printWriter.println(result.toString())
                                            //println("${TimeFormat.formattedTime} get_attack -> $result")
                                        }
                                        "get_repair" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone
                                            val result = entityFound.maxRepair
                                            printWriter.println(result.toString())
                                            //println("${TimeFormat.formattedTime} get_repair -> $result")
                                        }
                                        "get_energy_ore" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone
                                            val result = entityFound.storedEnergyOre
                                            printWriter.println(result.toString())
                                            //println("${TimeFormat.formattedTime} get_energy_ore -> $result")
                                        }
                                        "get_max_energy_ore" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone
                                            val result = entityFound.maxEnergyOre
                                            printWriter.println(result.toString())
                                            //println("${TimeFormat.formattedTime} get_max_energy_ore -> $result")
                                        }
                                        "get_shortest_path" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone
                                            val took = measureTimeMillis {
                                                val result = dungeon.pathfinder.findShortestPath(
                                                    entityFound.position,
                                                    Coordinates.getCoordinates(arguments[0].toInt(), arguments[1].toInt(), 0)
                                                )
                                                printWriter.println(klaxon.toJsonString(result))
                                                //println("${TimeFormat.formattedTime} get_shortest_path -> $result")
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} get_shortest_path of drone ${entityFound.id} took $took ms")
                                            }
                                        }
                                        "get_role" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone
                                            val result =
                                                if (entityFound.role == RolesEnum.NoRole) {
                                                    val givenRole = dungeon.roleQueues[entityFound.user]!!.getFirstRole()
                                                    entityFound.role = givenRole
                                                    givenRole
                                                } else
                                                    entityFound.role
                                            printWriter.println(result.name)
                                            //println("${TimeFormat.formattedTime} get_role -> $result")
                                        }
                                        "get_nearest_free_expand_position" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone

                                            val took = measureTimeMillis {
                                                var closest =
                                                    if (dungeon.energyOreExpansions.keys.any { it - entityFound.position == 0 })
                                                        entityFound.position  // We might be over one expansion point already
                                                    else
                                                        Coordinates.getCoordinates(0,0,0)

                                                for (expandPosition in dungeon.energyOreExpansions.keys)
                                                    if (expandPosition - entityFound.position < closest - entityFound.position &&
                                                        dungeon.gridEntities[expandPosition.y][expandPosition.x] == null)
                                                        closest = expandPosition

                                                val result =
                                                    if (closest == Coordinates.getCoordinates(0,0,0))
                                                        "null"
                                                    else
                                                        klaxon.toJsonString(closest)
                                                printWriter.println(result)
                                                //println("${TimeFormat.formattedTime} get_nearest_free_expand_position -> $result")
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} get_nearest_free_expand_position of drone ${entityFound.id} took $took ms")
                                            }
                                        }
                                        "get_patrol_point" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone

                                            val took = measureTimeMillis {
                                                val ourExpansionCoords = mutableListOf<Coordinates>()
                                                for (expandPosition in dungeon.energyOreExpansions.keys)
                                                    if (dungeon.gridEntities[expandPosition.y][expandPosition.x]?.user == entityFound.user)
                                                        ourExpansionCoords.add(expandPosition)

                                                var validPatrolPoint: Coordinates? = null
                                                while (validPatrolPoint == null) {
                                                    val x = Random.nextInt(0, dungeon.size)
                                                    val y = Random.nextInt(0, dungeon.size)
                                                    val selectedCoords = Coordinates.getCoordinates(x, y, 0)
                                                    for (expansionPoint in ourExpansionCoords) {
                                                        if (selectedCoords - expansionPoint <= 6) {
                                                            validPatrolPoint = selectedCoords
                                                            break
                                                        }
                                                    }
                                                }

                                                printWriter.println(klaxon.toJsonString(validPatrolPoint))
                                                //println("${TimeFormat.formattedTime} get_patrol_point -> $result")
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} get_patrol_point from drone ${entityFound.id} took $took")
                                            }
                                        }
                                        "get_needed_mover_position" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone

                                            val took = measureTimeMillis {
                                                var closest =
                                                    if (dungeon.energyOreExpansions.keys.any { it - entityFound.position == 0 })
                                                        entityFound.position  // We might be over one expansion point already
                                                    else
                                                        Coordinates.getCoordinates(0,0,0)
                                                for (expandPosition in dungeon.energyOreExpansions.keys)
                                                    if (expandPosition - entityFound.position < closest - entityFound.position &&
                                                        dungeon.gridEntities[expandPosition.y][expandPosition.x] == null &&
                                                        dungeon.gridEntities[expandPosition.y][expandPosition.x+1]?.user == entityFound.user)
                                                        closest = expandPosition

                                                val result =
                                                    if (closest == Coordinates.getCoordinates(0,0,0))
                                                        "null"
                                                    else
                                                        klaxon.toJsonString(closest)
                                                printWriter.println(result)
                                                //println("${TimeFormat.formattedTime} get_needed_mover_position -> $result")
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} get_needed_mover_position from drone ${entityFound.id} took $took ms")
                                            }
                                        }
                                        "get_closest_energy_ore" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone

                                            val took = measureTimeMillis {
                                                try {
                                                    var closest: EnergyOre = dungeon.energyOres.first()
                                                    for (energyOre in dungeon.energyOres) {
                                                        if ((energyOre.position - entityFound.position < closest.position - entityFound.position))
                                                            closest = energyOre
                                                    }

                                                    val result = klaxon.toJsonString(closest)
                                                    printWriter.println(result)
                                                    //println("${TimeFormat.formattedTime} get_closest_energy_ore -> $result")
                                                } catch (e: NoSuchElementException) {
                                                    printWriter.println("null")
                                                    //println("${TimeFormat.formattedTime} get_closest_energy_ore -> null")
                                                }
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} get_closest_energy_ore from drone ${entityFound.id} took $took ms")
                                            }
                                        }
                                        "get_closest_armoury" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone

                                            val took = measureTimeMillis {
                                                var closest: Armoury? =
                                                    try {
                                                        Entity.findEntityByPosition(
                                                            dungeon.energyOreExpansions.keys.first {
                                                                val foundArmoury = Entity.findEntityByPosition(Coordinates.getCoordinates(it.x-1, it.y, 0), dungeon)
                                                                foundArmoury != null && foundArmoury is Armoury && foundArmoury.user == entityFound.user
                                                            },
                                                            dungeon
                                                        ) as Armoury
                                                    } catch (e: NoSuchElementException) {
                                                        null
                                                    }

                                                if (closest != null) {
                                                    for (expansionPositions in dungeon.energyOreExpansions.keys) {
                                                        val foundEntity = Entity.findEntityByPosition(Coordinates.getCoordinates(expansionPositions.x-1, expansionPositions.y, 0), dungeon)
                                                        if (foundEntity != null && foundEntity is Armoury && foundEntity.user == entityFound.user &&
                                                            foundEntity.position - entityFound.position < closest!!.position - entityFound.position)
                                                            closest = foundEntity
                                                    }

                                                    val result = klaxon.toJsonString(closest!!)
                                                    printWriter.println(result)
                                                    //println("${TimeFormat.formattedTime} get_closest_armoury -> $result")
                                                } else {
                                                    printWriter.println("null")
                                                }
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} get_closest_armoury from drone ${entityFound.id} took $took ms")
                                            }
                                        }
                                        "get_closest_refinery" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone

                                            val took = measureTimeMillis {
                                                val closest: Refinery? = try {
                                                    var ref = dungeon.buildings.first { it is Refinery && it.user == entityFound.user } as Refinery
                                                    dungeon.buildings.forEach {
                                                        if (it is Refinery && it.user == entityFound.user && it.position - entityFound.position < ref.position - entityFound.position)
                                                            ref = it
                                                    }
                                                    ref
                                                } catch (e: NoSuchElementException) {
                                                    null
                                                }

                                                if (closest != null) {
                                                    val result = klaxon.toJsonString(closest)
                                                    printWriter.println(result)
                                                    //println("${TimeFormat.formattedTime} get_closest_armoury -> $result")
                                                } else {
                                                    printWriter.println("null")
                                                }
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} get_closest_armoury from drone ${entityFound.id} took $took ms")
                                            }
                                        }
                                        "get_exploring_destination" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone

                                            val took = measureTimeMillis {
                                                var result: Coordinates? = null
                                                var valid = false
                                                while (!valid) {
                                                    val selectedCoords = dungeon.energyOreExpansions.keys.elementAt(Random.nextInt(dungeon.energyOreExpansions.keys.size))
                                                    if (Entity.findEntityByPosition(selectedCoords, dungeon)?.user != entityFound.user) {
                                                        result = selectedCoords
                                                        valid = true
                                                    }
                                                }
                                                if (result == null)
                                                    printWriter.println("null")
                                                else
                                                    printWriter.println(klaxon.toJsonString(result))
                                                //println("${TimeFormat.formattedTime} get_exploring_destination -> $result")
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} get_exploring_destination  from drone ${entityFound.id} took $took ms")
                                            }
                                        }
                                        "get_assigned_refinery" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone

                                            val took = measureTimeMillis {
                                                var result: Refinery? = entityFound.assignedRefinery

                                                if (result == null) {
                                                    val allUserDrones = dungeon.drones.filter { it.user == entityFound.user }
                                                    val allUserRefineries = dungeon.buildings.filter { it is Refinery && it.user == entityFound.user }.associateBy({ it as Refinery }, { 0 }).toMutableMap()

                                                    for(drone in allUserDrones)
                                                        if (drone.assignedRefinery != null)
                                                            allUserRefineries[drone.assignedRefinery!!] = allUserRefineries[drone.assignedRefinery!!]!! + drone.level

                                                    for (refineryPair in allUserRefineries)
                                                        if (refineryPair.value < 3) {
                                                            result = refineryPair.key
                                                            entityFound.assignedRefinery = refineryPair.key
                                                        }
                                                }

                                                if (result == null)
                                                    printWriter.println("null")
                                                else
                                                    printWriter.println(klaxon.toJsonString(result!!))
                                                //println("${TimeFormat.formattedTime} get_assigned_refinery -> $result")
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} get_assigned_refinery  from drone ${entityFound.id} took $took ms")
                                            }
                                        }
                                        "get_closest_enemy" -> {
                                            val callerEntity = Entity.findEntityById(entityId)!!

                                            val took = measureTimeMillis {
                                                val callerEntities = Entity.findEntitiesByUserId(userId)
                                                val entitiesFound = mutableListOf<Entity>()

                                                // We want to prioritize drones than buildings, the y are more dangerous
                                                for (drone in dungeon.drones)
                                                    if (callerEntities.any { it.isAround(drone.position) } && drone.user != callerEntity.user)
                                                        entitiesFound.add(drone)

                                                if (entitiesFound.isEmpty()) {
                                                    // No enemy drones on sight or only buildings left
                                                    for (building in dungeon.buildings)
                                                        if (callerEntities.any { it.isAround(building.position) } && building.user != callerEntity.user)
                                                            entitiesFound.add(building)
                                                }

                                                var closest =
                                                    try {
                                                        entitiesFound.first()
                                                    } catch (e: NoSuchElementException) {
                                                        null
                                                    }

                                                if (closest != null) {
                                                    for (enemy in entitiesFound)
                                                        if (enemy.position - callerEntity.position < closest!!.position - callerEntity.position)
                                                            closest = enemy

                                                    val result = klaxon.toJsonString(closest!!)
                                                    printWriter.println(result)
                                                    //println("${TimeFormat.formattedTime} get_closest_enemy -> $result")
                                                } else {
                                                    printWriter.println("null")
                                                }
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} get_closest_enemy from drone ${callerEntity.id} took $took ms")
                                            }
                                        }
                                        "get_closest_last_enemy_position_known" -> {
                                            val callerEntity = Entity.findEntityById(entityId)!!

                                            var closest: Coordinates? = null
                                            dungeon.lastEnemiesSeenPositions[callerEntity.user]!!.values.forEach { enemyCoords ->
                                                if (closest == null || enemyCoords - callerEntity.position < closest!! - callerEntity.position)
                                                    closest = enemyCoords
                                            }

                                            val result = closest?.let { klaxon.toJsonString(it) } ?: "null"
                                            printWriter.println(result)
                                            //println("${TimeFormat.formattedTime} get_closest_last_enemy_position_known -> $result")
                                        }
                                        "mark_visible_entitites" -> {
                                            val callerEntity = Entity.findEntityById(entityId)!!

                                            val took = measureTimeMillis {
                                                val callerEntities = Entity.findEntitiesByUserId(userId)

                                                // Remove positions around our entities
                                                val entitiesToRemoveLastPos = mutableListOf<Entity>()
                                                callerEntities.forEach { entity ->
                                                    dungeon.lastEnemiesSeenPositions[entity.user!!]!!.forEach {
                                                        if (entity.isAround(it.value))
                                                            entitiesToRemoveLastPos.add(it.key)
                                                    }
                                                }
                                                entitiesToRemoveLastPos.forEach {
                                                    dungeon.lastEnemiesSeenPositions[callerEntity.user!!]!!.remove(it)
                                                }

                                                // Get all entities from other players
                                                val otherPlayersEntities = mutableListOf<Entity>()
                                                dungeon.players.forEach { player ->
                                                    if (player != callerEntity.user)
                                                        Entity.findEntitiesByUserId(player.id).forEach {
                                                            otherPlayersEntities.add(it)
                                                        }
                                                }

                                                // Check if any of those entities are around any of our entities and add them to last seen
                                                callerEntities.forEach { caller ->
                                                    otherPlayersEntities.forEach { other ->
                                                        if (caller.isAround(other.position)) {
                                                            dungeon.lastEnemiesSeenPositions[caller.user!!]!![other] = other.position.copy()
                                                        }
                                                    }
                                                }

                                                printWriter.println("true")
                                                //println("${TimeFormat.formattedTime} mark_visible_entitites -> $result")
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} mark_visible_entitites from drone ${callerEntity.id} took $took ms")
                                            }
                                        }
                                        "move" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone

                                            val took = measureTimeMillis {
                                                if (!entityFound.newTurn) {
                                                    printWriter.println("false")
                                                    //println("${TimeFormat.formattedTime} move -> false")
                                                } else {
                                                    val result =
                                                        if (entityFound.newTurn)
                                                            entityFound.move(
                                                                Coordinates.getCoordinates(
                                                                    arguments[0].toInt(),
                                                                    arguments[1].toInt(),
                                                                    0
                                                                )
                                                            ).toString()
                                                        else
                                                            null
                                                    entityFound.newTurn = false
                                                    printWriter.println(result)
                                                    dungeon.gridChanges[entityFound.user]!!.apply { remove(entityFound) }.add(entityFound)
                                                    //println("${TimeFormat.formattedTime} move -> $result")
                                                }
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} move from drone ${entityFound.id} took $took ms")
                                            }
                                        }
                                        "move_towards" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone

                                            val took = measureTimeMillis {
                                                if (!entityFound.newTurn) {
                                                    printWriter.println("false")
                                                    //println("${TimeFormat.formattedTime} move_towards -> false")
                                                } else {
                                                    val destination =
                                                        Coordinates.getCoordinates(arguments[0].toInt(), arguments[1].toInt(), 0)
                                                    val result =
                                                        if (entityFound.newTurn) {
                                                            val moveResult = entityFound.moveTowards(destination)
                                                            entityFound.newTurn = moveResult == -2  // The turn wont consume if it didn't moved
                                                            moveResult
                                                        } else
                                                            null
                                                    printWriter.println(result)
                                                    dungeon.gridChanges[entityFound.user]!!.apply { remove(entityFound) }.add(entityFound)
                                                    //println("${TimeFormat.formattedTime} move_towards $destination -> $result  --  ${entityFound.position}")
                                                }
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} move_towards from drone ${entityFound.id} took $took ms")
                                            }
                                        }
                                        "combine_around" -> {  // This way we can reduce the drone count in the games other than from killing
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone

                                            val took = measureTimeMillis {
                                                if (!entityFound.newTurn) {
                                                    printWriter.println("false")
                                                    //println("${TimeFormat.formattedTime} combine_around -> false")
                                                } else {
                                                    // Check if any drones are around
                                                    val aroundDrone: Drone? =
                                                        Entity.findEntityByPosition(Coordinates.getCoordinates(entityFound.position.x+1, entityFound.position.y, 0), dungeon).castOrNull()
                                                            ?: Entity.findEntityByPosition(Coordinates.getCoordinates(entityFound.position.x-1, entityFound.position.y, 0), dungeon).castOrNull()
                                                            ?: Entity.findEntityByPosition(Coordinates.getCoordinates(entityFound.position.x, entityFound.position.y+1, 0), dungeon).castOrNull()
                                                            ?: Entity.findEntityByPosition(Coordinates.getCoordinates(entityFound.position.x, entityFound.position.y-1, 0), dungeon).castOrNull()
                                                    val result =
                                                        if (entityFound.newTurn && aroundDrone != null && aroundDrone.level + entityFound.level <= 3
                                                            && entityFound.role != RolesEnum.ExpanderFactory
                                                            && entityFound.role != RolesEnum.ExpanderArmory
                                                            && entityFound.role != RolesEnum.ExpanderUpgrades
                                                            && aroundDrone.role == entityFound.role  // Both drones must have same role to be combined
                                                            && aroundDrone.assignedRefinery == entityFound.assignedRefinery) {  // Both drones must have the same assigned refinery (if they are not miners it will be both null anyway)

                                                            val newLevel = aroundDrone.levelUp(entityFound)
                                                            dungeon.removeEntity(entityFound) // Combining a drone consumes it
                                                            entityFound.newTurn = false

                                                            dungeon.gridChanges[entityFound.user]!!.apply { remove(entityFound) }.add(entityFound)
                                                            dungeon.gridChanges[entityFound.user]!!.apply { remove(aroundDrone) }.add(aroundDrone)

                                                            newLevel
                                                        } else {
                                                            -1
                                                        }

                                                    printWriter.println(result)
                                                    //println("${TimeFormat.formattedTime} combine_around -> $result  --  ${entityFound.position}")
                                                }
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} combine_around from drone ${entityFound.id} took $took ms")
                                            }
                                        }
                                        "recharge_at" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone

                                            val took = measureTimeMillis {
                                                if (!entityFound.newTurn) {
                                                    printWriter.println("false")
                                                    //println("${TimeFormat.formattedTime} recharge_at -> false")
                                                } else {
                                                    val refineryFound =
                                                        try {
                                                            entityFound.findEntityByPosition(
                                                                Coordinates.getCoordinates(
                                                                    arguments[0].toInt(),
                                                                    arguments[1].toInt(),
                                                                    0
                                                                )
                                                            ) as Refinery?
                                                        } catch (e: ClassCastException) {
                                                            null
                                                        }
                                                    val result =
                                                        if (entityFound.newTurn) {
                                                            when {
                                                                refineryFound == null || entityFound.position - refineryFound.position <= 1 -> {
                                                                    if (entityFound.recharge(refineryFound) > 0) {
                                                                        1  // Recharged some energy
                                                                    } else {
                                                                        0  // Recharge failed or no energy was recharged
                                                                    }
                                                                }
                                                                entityFound.position - refineryFound.position > 1 -> {
                                                                    if (entityFound.moveTowards(refineryFound.position) >= -1) {
                                                                        -1  // Moved successfully (Even if it was only launched it will move later on)
                                                                    } else {
                                                                        -2  // Movement failed
                                                                    }
                                                                }
                                                                else -> {
                                                                    -3  // Other error
                                                                }
                                                            }
                                                        } else
                                                            null
                                                    entityFound.newTurn = false
                                                    printWriter.println(result.toString())
                                                    dungeon.gridChanges[entityFound.user]!!.apply { remove(entityFound) }.add(entityFound)
                                                    if (refineryFound != null)
                                                        dungeon.gridChanges[entityFound.user]!!.apply { remove(refineryFound) }.add(refineryFound)
                                                    //println("${TimeFormat.formattedTime} recharge_at $arguments -> $result  --  ${entityFound.position}")
                                                }
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} recharge_at from drone ${entityFound.id} took $took ms")
                                            }
                                        }
                                        "get_armed_at" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone

                                            val took = measureTimeMillis {
                                                if (!entityFound.newTurn) {
                                                    printWriter.println("false")
                                                    //println("${TimeFormat.formattedTime} recharge_at -> false")
                                                } else {
                                                    val armouryFound =
                                                        try {
                                                            entityFound.findEntityByPosition(
                                                                Coordinates.getCoordinates(
                                                                    arguments[0].toInt(),
                                                                    arguments[1].toInt(),
                                                                    0
                                                                )
                                                            ) as Armoury?
                                                        } catch (e: ClassCastException) {
                                                            null
                                                        }
                                                    val result =
                                                        if (entityFound.newTurn && armouryFound != null) {
                                                            when {
                                                                entityFound.position - armouryFound.position <= 1 -> {
                                                                    if (entityFound.getEquipment(armouryFound)) {
                                                                        1  // equipped
                                                                    } else {
                                                                        0  // not equipped
                                                                    }
                                                                }
                                                                entityFound.position - armouryFound.position > 1 -> {
                                                                    if (entityFound.moveTowards(armouryFound.position) >= -1) {
                                                                        -1  // Moved successfully (Even if it was only launched it will move later on)
                                                                    } else {
                                                                        -2  // Movement failed
                                                                    }
                                                                }
                                                                else -> {
                                                                    -3  // Other error
                                                                }
                                                            }
                                                        } else {
                                                            null
                                                        }
                                                    entityFound.newTurn = false
                                                    printWriter.println(result.toString())
                                                    dungeon.gridChanges[entityFound.user]!!.apply { remove(entityFound) }.add(entityFound)
                                                    if (armouryFound != null)
                                                        dungeon.gridChanges[entityFound.user]!!.apply { remove(armouryFound) }.add(armouryFound)
                                                    //println("${TimeFormat.formattedTime} recharge_at $arguments -> $result  --  ${entityFound.position}")
                                                }
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} recharge_at from drone ${entityFound.id} took $took ms")
                                            }
                                        }
                                        "mine_energy_ore_at" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone

                                            val took = measureTimeMillis {
                                                if (!entityFound.newTurn) {
                                                    printWriter.println("false")
                                                    //println("${TimeFormat.formattedTime} mine_energy_ore_at -> false")
                                                } else {
                                                    val energyOreFound =
                                                        try {
                                                            entityFound.findEntityByPosition(
                                                                Coordinates.getCoordinates(
                                                                    arguments[0].toInt(),
                                                                    arguments[1].toInt(),
                                                                    0
                                                                )
                                                            ) as EnergyOre?
                                                        } catch (e: ClassCastException) {
                                                            null
                                                        }
                                                    if (energyOreFound != null) {
                                                        val result = when {
                                                            entityFound.position - energyOreFound.position <= 1 -> {
                                                                if (entityFound.mineEnergyOre(energyOreFound) >= 0) {
                                                                    1  // mined some ore
                                                                } else {
                                                                    0  // didn't mined some ore
                                                                }
                                                            }
                                                            entityFound.position - energyOreFound.position > 1 -> {
                                                                if (entityFound.moveTowards(energyOreFound.position) >= -1) {
                                                                    -1  // Moved successfully (Even if it was only launched it will move later on)
                                                                } else {
                                                                    -2  // Movement failed
                                                                }
                                                            }
                                                            else -> -3  // Other error
                                                        }

                                                        if (energyOreFound.energyOreRemaining == 0)
                                                            dungeon.removeEntity(energyOreFound)
                                                        entityFound.newTurn = false
                                                        printWriter.println(result.toString())
                                                        dungeon.gridChanges[entityFound.user]!!.apply { remove(entityFound) }.add(entityFound)
                                                        dungeon.gridChanges[entityFound.user]!!.apply { remove(energyOreFound) }.add(energyOreFound)
                                                        //println("${TimeFormat.formattedTime} mine_energy_ore_at -> $result")
                                                    } else {
                                                        printWriter.println("-1")
                                                        //println("${TimeFormat.formattedTime} mine_energy_ore_at -> -1")
                                                    }
                                                }
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} mine_energy_ore_at from drone ${entityFound.id} took $took ms")
                                            }
                                        }
                                        "build_at" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone
                                            if (!entityFound.newTurn) {
                                                printWriter.println("false")
                                                //println("${TimeFormat.formattedTime} build_at -> false")
                                            } else {
                                                val buildingFound = entityFound.findEntityByPosition(
                                                    Coordinates.getCoordinates(
                                                        arguments[0].toInt(),
                                                        arguments[1].toInt(),
                                                        0
                                                    )
                                                ) as Building?
                                                if (buildingFound != null) {
                                                    val result =
                                                        if (entityFound.newTurn) {
                                                            when {
                                                                entityFound.position - buildingFound.position <= 1 -> {
                                                                    if (entityFound.buildBuilding(buildingFound) >= 0) {
                                                                        1  // constructed the building
                                                                    } else {
                                                                        0  // didn't
                                                                    }
                                                                }
                                                                entityFound.position - buildingFound.position > 1 -> {
                                                                    if (entityFound.moveTowards(buildingFound.position) >= -1) {
                                                                        -1  // Moved successfully (Even if it was only launched it will move later on)
                                                                    } else {
                                                                        -2  // Movement failed
                                                                    }
                                                                }
                                                                else -> -3  // Other error
                                                            }
                                                        } else
                                                            null
                                                    entityFound.newTurn = false
                                                    printWriter.println(result)
                                                    dungeon.gridChanges[entityFound.user]!!.apply { remove(entityFound) }.add(entityFound)
                                                    dungeon.gridChanges[entityFound.user]!!.apply { remove(buildingFound) }.add(buildingFound)
                                                    //println("${TimeFormat.formattedTime} build_at $arguments -> $result")
                                                } else {
                                                    printWriter.println("${TimeFormat.formattedTime} -1")
                                                    //println("${TimeFormat.formattedTime} build_at $arguments -> -1")
                                                }
                                            }
                                        }
                                        "start_build_at" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone
                                            if (!entityFound.newTurn) {
                                                printWriter.println("false")
                                                //println("${TimeFormat.formattedTime} start_build_at -> false (Turn needs to be reset)")
                                            } else {
                                                val buildingDestination = entityFound.findEntityByPosition(
                                                    Coordinates.getCoordinates(
                                                        arguments[0].toInt(),
                                                        arguments[1].toInt(),
                                                        0
                                                    )
                                                )
                                                if (buildingDestination != null || Coordinates.getCoordinates(arguments[0].toInt(), arguments[1].toInt(), 0) - entityFound.position > 1) {
                                                    printWriter.println("false")
                                                    //println("${TimeFormat.formattedTime} start_build_at $arguments -> false")
                                                } else {
                                                    val dun = Dungeon.findDungeonFromEntity(entityFound)!!
                                                    val newBuilding = (EntityCode.valueOf(arguments[2]).kclass!!
                                                        .declaredConstructors[0].newInstance(
                                                            -1,
                                                            EntityCode.valueOf(arguments[2]).entityName,
                                                            arguments[2],
                                                            false,
                                                            User.users.find { it.id == userId },
                                                            dun.dungeonId,
                                                            Coordinates.getCoordinates(arguments[0].toInt(), arguments[1].toInt(), 0)
                                                        ) as Building).generateId(dun)
                                                    dun.spawnEntity(newBuilding)
                                                    entityFound.newTurn = false
                                                    printWriter.println("true")
                                                    dungeon.gridChanges[entityFound.user]!!.apply { remove(entityFound) }.add(entityFound)
                                                    dungeon.gridChanges[entityFound.user]!!.apply { remove(newBuilding) }.add(newBuilding)
                                                    //println("${TimeFormat.formattedTime} start_build_at $arguments -> true")
                                                }
                                            }
                                        }
                                        "store_energy_at" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone

                                            val took = measureTimeMillis {
                                                if (!entityFound.newTurn) {
                                                    printWriter.println("false")
                                                    //println("${TimeFormat.formattedTime} store_energy_at -> false")
                                                } else {
                                                    val buildingFound = entityFound.findEntityByPosition(
                                                        Coordinates.getCoordinates(
                                                            arguments[0].toInt(),
                                                            arguments[1].toInt(),
                                                            0
                                                        )
                                                    ) as Building?
                                                    if (buildingFound != null) {
                                                        val result =
                                                            if (entityFound.newTurn) {
                                                                when {
                                                                    entityFound.position - buildingFound.position <= 1 -> {
                                                                        if (entityFound.storeEnergyInBuilding(buildingFound)) {
                                                                            1  // stored the energy
                                                                        } else {
                                                                            0  // didn't
                                                                        }
                                                                    }
                                                                    entityFound.position - buildingFound.position > 1 -> {
                                                                        if (entityFound.moveTowards(buildingFound.position) >= -1) {
                                                                            -1  // Moved successfully (Even if it was only launched it will move later on)
                                                                        } else {
                                                                            -2  // Movement failed
                                                                        }
                                                                    }
                                                                    else -> -3  // Other error
                                                                }
                                                            } else
                                                                null
                                                        entityFound.newTurn = false
                                                        printWriter.println(result)
                                                        dungeon.gridChanges[entityFound.user]!!.apply { remove(entityFound) }.add(entityFound)
                                                        dungeon.gridChanges[entityFound.user]!!.apply { remove(buildingFound) }.add(buildingFound)
                                                        //println("${TimeFormat.formattedTime} store_energy_at $arguments -> $result")
                                                    } else {
                                                        printWriter.println("false")
                                                        //println("${TimeFormat.formattedTime} store_energy_at $arguments -> false")
                                                    }
                                                }
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} store_energy_at from drone ${entityFound.id} took $took ms")
                                            }
                                        }
                                        "store_energy_ore_at" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone

                                            val took = measureTimeMillis {
                                                if (!entityFound.newTurn) {
                                                    printWriter.println("false")
                                                    //println("${TimeFormat.formattedTime} store_energy_ore_at -> false")
                                                } else {
                                                    val refineryFound = entityFound.findEntityByPosition(
                                                        Coordinates.getCoordinates(
                                                            arguments[0].toInt(),
                                                            arguments[1].toInt(),
                                                            0
                                                        )
                                                    ) as Refinery?
                                                    if (refineryFound != null) {
                                                        val result = when {
                                                            entityFound.position - refineryFound.position <= 1 -> {
                                                                if (entityFound.storeEnergyOreInRefinery(refineryFound)) {
                                                                    1  // stored the energy ore
                                                                } else {
                                                                    0  // didn't
                                                                }
                                                            }
                                                            entityFound.position - refineryFound.position > 1 -> {
                                                                if (entityFound.moveTowards(refineryFound.position) >= -1) {
                                                                    -1  // Moved successfully (Even if it was only launched it will move later on)
                                                                } else {
                                                                    -2  // Movement failed
                                                                }
                                                            }
                                                            else -> -3  // Other error
                                                        }
                                                        entityFound.newTurn = false
                                                        printWriter.println(result)
                                                        dungeon.gridChanges[entityFound.user]!!.apply { remove(entityFound) }.add(entityFound)
                                                        dungeon.gridChanges[entityFound.user]!!.apply { remove(refineryFound) }.add(refineryFound)
                                                        //println("${TimeFormat.formattedTime} store_energy_ore_at $arguments -> $result")
                                                    } else {
                                                        printWriter.println("false")
                                                        //println("${TimeFormat.formattedTime} store_energy_ore_at $arguments -> false")
                                                    }
                                                }
                                            }
                                            if(took > 2) {
                                                println("${TimeFormat.formattedTime} store_energy_ore_at from drone ${entityFound.id} took $took ms")
                                            }
                                        }
                                        "attack_at" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone

                                            val took = measureTimeMillis {
                                                if (!entityFound.newTurn) {
                                                    printWriter.println("false")
                                                    //println("${TimeFormat.formattedTime} attack_at -> false")
                                                } else {
                                                    val enemyFound = entityFound.findEntityByPosition(
                                                        Coordinates.getCoordinates(
                                                            arguments[0].toInt(),
                                                            arguments[1].toInt(),
                                                            0
                                                        )
                                                    )
                                                    if (enemyFound != null) {
                                                        val result =
                                                            if (entityFound.newTurn) {
                                                                when {
                                                                    entityFound.position - enemyFound.position <= 1 -> {
                                                                        if (entityFound.attack(enemyFound) >= 0) {
                                                                            1  // attacked the enemy
                                                                        } else {
                                                                            0  // didn't
                                                                        }
                                                                    }
                                                                    entityFound.position - enemyFound.position > 1 -> {
                                                                        if (entityFound.moveTowards(enemyFound.position) >= -1) {
                                                                            -1  // Moved successfully (Even if it was only launched it will move later on)
                                                                        } else {
                                                                            -2  // Movement failed
                                                                        }
                                                                    }
                                                                    else -> -3  // Other error
                                                                }
                                                            } else
                                                                null

                                                        if (enemyFound.life == 0) {
                                                            dungeon.removeEntity(enemyFound)

                                                            // Check if that was the last entity and the game ended
                                                            dungeon.players.forEach {
                                                                if (Entity.findEntitiesByUserId(it.id).isEmpty()) {
                                                                    dungeon.players.forEach {
                                                                        it.inGame = false
                                                                    }
                                                                    dungeon.callerProcess?.destroyForcibly()
                                                                    System.gc()  // After finishing a game we want to call Garbage Collector to remove the remains
                                                                }
                                                            }
                                                        }
                                                        entityFound.newTurn = false
                                                        printWriter.println(result)
                                                        dungeon.gridChanges[entityFound.user]!!.apply { remove(entityFound) }.add(entityFound)
                                                        dungeon.gridChanges[entityFound.user]!!.apply { remove(enemyFound) }.add(enemyFound)
                                                        //println("${TimeFormat.formattedTime} attack_at $arguments -> $result")
                                                    } else {
                                                        printWriter.println("${TimeFormat.formattedTime} -1")
                                                        //println("${TimeFormat.formattedTime} attack_at $arguments -> -1")
                                                    }
                                                }
                                            }
                                            if(took > 200) {
                                                println("${TimeFormat.formattedTime} attack_at from drone ${entityFound.id} took $took ms")
                                            }
                                        }
                                        "repair_at" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Drone

                                            val took = measureTimeMillis {
                                                if (!entityFound.newTurn) {
                                                    printWriter.println("false")
                                                    //println("${TimeFormat.formattedTime} repair_at -> false")
                                                } else {
                                                    val damagedFound = entityFound.findEntityByPosition(
                                                        Coordinates.getCoordinates(
                                                            arguments[0].toInt(),
                                                            arguments[1].toInt(),
                                                            0
                                                        )
                                                    )
                                                    if (damagedFound != null) {
                                                        val result =
                                                            if (entityFound.newTurn) {
                                                                when {
                                                                    entityFound.position - damagedFound.position <= 1 -> {
                                                                        if (entityFound.repair(damagedFound) >= 0) {
                                                                            1  // repaired something
                                                                        } else {
                                                                            0  // didn't
                                                                        }
                                                                    }
                                                                    entityFound.position - damagedFound.position > 1 -> {
                                                                        if (entityFound.moveTowards(damagedFound.position) >= -1) {
                                                                            -1  // Moved successfully (Even if it was only launched it will move later on)
                                                                        } else {
                                                                            -2  // Movement failed
                                                                        }
                                                                    }
                                                                    else -> -3  // Other error
                                                                }
                                                            } else
                                                                null
                                                        entityFound.newTurn = false
                                                        printWriter.println(result)
                                                        dungeon.gridChanges[entityFound.user]!!.apply { remove(entityFound) }.add(entityFound)
                                                        dungeon.gridChanges[entityFound.user]!!.apply { remove(damagedFound) }.add(damagedFound)
                                                        //println("${TimeFormat.formattedTime} repair_at -> $result")
                                                    } else {
                                                        printWriter.println("${TimeFormat.formattedTime} -1")
                                                        //println("${TimeFormat.formattedTime} repair_at -> -1")
                                                    }
                                                }
                                            }
                                            if (took > 5) {
                                                println("${TimeFormat.formattedTime} repair_at from drone ${entityFound.id} took $took ms")
                                            }
                                        }
                                    }
                                "Refinery" ->
                                    when (function) {
                                        // Refinery
                                        "get_building_progress" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Refinery
                                            val result = entityFound.buildProgress.toString()
                                            printWriter.println(result)
                                            //println("${TimeFormat.formattedTime} get_building_progress -> $result")
                                        }
                                        "get_energy" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Refinery
                                            val result = entityFound.storedEnergy.toString()
                                            printWriter.println(result)
                                            //println("${TimeFormat.formattedTime} get_energy -> $result")
                                        }
                                        "get_max_energy" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Refinery
                                            val result = entityFound.maxEnergy.toString()
                                            printWriter.println(result)
                                            //println("${TimeFormat.formattedTime} get_max_energy -> $result")
                                        }
                                        "get_energy_ore" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Refinery
                                            val result = entityFound.storedEnergyOre.toString()
                                            printWriter.println(result)
                                            //println("${TimeFormat.formattedTime} get_energy_ore -> $result")
                                        }
                                        "refine" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as Refinery
                                            if (!entityFound.newTurn) {
                                                printWriter.println("false")
                                                //println("${TimeFormat.formattedTime} refine -> false")
                                            } else {
                                                val result =
                                                    if (entityFound.newTurn)
                                                        entityFound.refine().toString()
                                                    else
                                                        null
                                                entityFound.newTurn = false
                                                printWriter.println(result)
                                                dungeon.gridChanges[entityFound.user]!!.apply { remove(entityFound) }.add(entityFound)
                                                //println("${TimeFormat.formattedTime} refine -> $result")
                                            }
                                        }
                                    }
                                "DroneFactory" ->
                                    when (function) {
                                        // DroneFactory
                                        "get_building_progress" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as DroneFactory
                                            val result = entityFound.buildProgress.toString()
                                            printWriter.println(result)
                                            //println("${TimeFormat.formattedTime} get_building_progress -> $result")
                                        }
                                        "get_energy" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as DroneFactory
                                            val result = entityFound.storedEnergy.toString()
                                            printWriter.println(result)
                                            //println("${TimeFormat.formattedTime} get_energy -> $result")
                                        }
                                        "get_max_energy" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as DroneFactory
                                            val result = entityFound.maxEnergy.toString()
                                            printWriter.println(result)
                                            //println("${TimeFormat.formattedTime} get_max_energy -> $result")
                                        }
                                        "get_drone_progress" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as DroneFactory
                                            val result = entityFound.droneProgress.toString()
                                            printWriter.println(result)
                                            //println("${TimeFormat.formattedTime} get_drone_progress -> $result")
                                        }
                                        "get_energy_for_drone" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as DroneFactory
                                            val result = entityFound.energyNeededForDrone.toString()
                                            printWriter.println(result)
                                            //println("${TimeFormat.formattedTime} get_energy_for_drone -> $result")
                                        }
                                        "manufacture" -> {
                                            val entityFound = Entity.findEntityById(entityId, dungeon) as DroneFactory
                                            if (!entityFound.newTurn) {
                                                printWriter.println("false")
                                                //println("${TimeFormat.formattedTime} manufacture -> false")
                                            } else {
                                                val result =
                                                    if (entityFound.newTurn)
                                                        entityFound.manufacture().toString()
                                                    else
                                                        null
                                                entityFound.newTurn = false
                                                printWriter.println(result)
                                                dungeon.gridChanges[entityFound.user]!!.apply { remove(entityFound) }.add(entityFound)
                                                //println("${TimeFormat.formattedTime} manufacture -> $result")
                                            }
                                        }
                                    }
                                "Game" -> when (function) {
                                    "get_map" -> {
                                        val took = measureTimeMillis {
                                            val userEntities = Entity.findEntitiesByUserId(userId)
                                            val gridCopy = mutableListOf<MutableList<Entity?>>()
                                            for (i in 0 until dungeon.gridEntities.size) {
                                                val mlist = mutableListOf<Entity?>()
                                                for (j in 0 until dungeon.gridEntities.size)
                                                    if (userEntities.any { it.isAround(Coordinates.getCoordinates(j, i, 0)) })
                                                        mlist.add(dungeon.gridEntities[i][j])
                                                    else
                                                        mlist.add(null)
                                                gridCopy.add(mlist)
                                            }

                                            val partialConverter = object: Converter {
                                                override fun canConvert(cls: Class<*>)
                                                        = true

                                                override fun fromJson(jv: JsonValue)
                                                        = ""

                                                override fun toJson(value: Any): String {
                                                    var result = StringBuilder("[")
                                                    val grid = value as List<List<Entity?>>
                                                    val klaxon = Klaxon()
                                                    for (y in 0 until grid.size)  {
                                                        result.append("[")
                                                        for (x in 0 until grid.size) {
                                                            result.append(
                                                                if (grid[y][x] == null)
                                                                    "null,"
                                                                else
                                                                    klaxon.toJsonString(grid[y][x]!!) + ","
                                                            )
                                                        }
                                                        result = StringBuilder(result.removeSuffix(","))
                                                        result.append("],")
                                                    }
                                                    result = StringBuilder(result.removeSuffix(","))
                                                    result.append("]")
                                                    return result.toString()
                                                }
                                            }

                                            val result = klaxon.converter(partialConverter).toJsonString(gridCopy)
                                            klaxon.converter(DefaultConverter(klaxon, hashMapOf()))  // TODO Fix potential memory leak here
                                            printWriter.println(result)
                                            //println("${TimeFormat.formattedTime} get_map -> $result")
                                        }
                                        if (took > 5) {
                                            println("${TimeFormat.formattedTime} get_map from entity $entityId took $took ms")
                                        }
                                    }
                                }
                                "Caller" -> when (function) {
                                    "turn" -> {
                                        // We want to reset the turn independently from game state so we can debug the ai when caller.py is paused
                                        val entityFound = Entity.findEntityById(entityId, dungeon)
                                        entityFound!!.newTurn = true

//                                        // Call moveTowards for a second time to complete the async path if entityFound is a Drone
//                                        if (entityFound is Drone) {
//                                            val pendingPath = dungeon.pathfinder.pendingPaths.filter { it.value.first == entityFound.position }
//                                            if (pendingPath.isNotEmpty())
//                                                entityFound.moveTowards(pendingPath.entries.first().value.second)
//                                        }

                                        if (dungeon.paused) {
                                            printWriter.println("PAUSED")
                                        } else {
                                            entityFound.turnsPassed++
                                            printWriter.println("OK")
                                        }
                                    }
                                    "all_turn" -> {
                                        dungeon.drones.forEach {
                                            it.newTurn = true
                                            it.turnsPassed++
                                        }
                                        dungeon.buildings.forEach {
                                            it.newTurn = true
                                            it.turnsPassed++
                                        }

//                                        val took = measureTimeMillis {
//                                            // Now that the turn has officially ended we must finish async movements
//                                            while (dungeon.pathfinder.pendingPaths.isNotEmpty()) {
//                                                dungeonLock.unlock()
//                                                dungeon.pathfinder.pendingPaths.toList().forEach {
//                                                    (Entity.findEntityByPosition(it.second.first, dungeon) as Drone).moveTowards(it.second.second)  // This would be the second call to moveTowards to complete the movement
//                                                }
//                                                dungeonLock.lock()
//                                            }
//                                        }
                                        val turnTime = currentTimeMillis() - startTime
                                        println("${TimeFormat.formattedTime} This turn took $turnTime ms (The closest to 250ms the better)")

                                        dungeon.lastTurnTimes[dungeon.lastTurnTimeIndex] = turnTime.toInt()
                                        dungeon.lastTurnTimeIndex++
                                        if (dungeon.lastTurnTimeIndex == dungeon.lastTurnTimes.size) {
                                            dungeon.averageTurnTime = dungeon.lastTurnTimes.average().toInt()
                                            dungeon.lastTurnTimeIndex = 0
                                        }

                                        if (dungeon.paused) {
                                            printWriter.println("PAUSED")
                                        } else {
                                            printWriter.println("OK")
                                        }
                                        
                                        startTime = currentTimeMillis()
                                    }
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            println("${TimeFormat.formattedTime} User $userId sent erroneous command")
                            printWriter.println("false")
                            println("${TimeFormat.formattedTime} Exception -> false")
                        }
                    }
                    dungeonLock.unlock()
                    if (dungeon.players.any { !it.inGame }) {
                        connection.close()
                        break  // Will exit this thread
                    }
//                    if (took > 50)
//                        println("${TimeFormat.formattedTime} This server call did took longer than expected ($took ms) -> $received")
                }
            }
        }
    }

    private inline fun <reified T> Entity?.castOrNull(): T? {
        if (this == null)
            return null

        if (this !is T)
            return null

        return this
    }
}