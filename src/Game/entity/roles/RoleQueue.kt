package Game.entity.roles

import java.util.concurrent.locks.ReentrantLock

class RoleQueue () {
    private val roleList = mutableListOf<Pair<Int?, RolesEnum>>()
    private val lock = ReentrantLock(true)

    constructor(roleImport: List<String>) : this() {
        roleImport.forEach {
            roleList.add(Pair(null, RolesEnum.valueOf(it)))
        }
    }

    fun addRoleToQueue(role: RolesEnum, specificEntityId: Int? = null, putItFirst: Boolean = false) {
        lock.lock()

        if (putItFirst) {
            roleList.add(0, Pair(specificEntityId, role))
        } else {
            roleList.add(Pair(specificEntityId, role))
        }

        lock.unlock()
    }

    /**
     * Roles in the queue assigned to null are considered general assignations to whoever wants them.
     *
     * When setting strictAssignation to false and specifying a specificEntityId, this method will return the first
     * occurrence for a role assigned to the specific entity or null, whatever comes first.
     *
     * returned role will be removed from the queue
     */
    fun getFirstRole(specificEntityId: Int? = null, strictAssignation: Boolean = false): RolesEnum {
        var returningRole = RolesEnum.NoRole
        var returningRolePair: Pair<Int?, RolesEnum>? = null
        lock.lock()

        try {
            returningRolePair = roleList.first {
                if (strictAssignation)
                    it.first == specificEntityId
                else
                    it.first == specificEntityId || it.first == null
            }
            returningRole = returningRolePair.second
            roleList.remove(returningRolePair)
        } catch (e: NoSuchElementException) {
            // ignored
        }

        lock.unlock()
        return returningRole
    }
}