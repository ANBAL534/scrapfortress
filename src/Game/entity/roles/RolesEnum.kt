package Game.entity.roles

enum class RolesEnum {
    NoRole,
    EnergyOreMiner,
    Mover,
    Explorer,
    Raider,
    Defender,
    BuilderHelper,
    ExpanderFactory,
    ExpanderUpgrades,
    ExpanderArmory
}