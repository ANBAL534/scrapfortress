package Game.entity

enum class EntityCode(val entityName: String, val kclass: Class<*>?) {
//    S("Entity", Entity::class.java),
    W("Wall", Wall::class.java),
    E("Energy Ore", EnergyOre::class.java),
    D("Drone", Drone::class.java),
    F("Drone Factory", DroneFactory::class.java),
    R("Refinery", Refinery::class.java),
    A("Armoury", Armoury::class.java),
    Space("Empty Space", null)
}