package Game.entity

import Game.world.Coordinates

class Wall (override var id: Int,
            override var name: String,
            override val mapCode: String,
            override val isWalkable: Boolean,
//            override var inDungeon: Dungeon,
            override val inDungeonId: String,
            override var position: Coordinates
): Entity(id, name, mapCode, inDungeonId, /*inDungeon, */isWalkable, position) {

    @Volatile
    override var life = 5000
        set(value) {
            field =
                if (value <= 0) {
                    0
                } else
                    value
        }
    override var maxLife = 5000

}