package Game.entity

import Game.world.Coordinates
import Game.world.Dungeon
import Web.users.User
import com.beust.klaxon.Json
import java.util.*

abstract class Entity (open var id: Int,
                       open var name: String,
                       open val mapCode: String,
//                       @Json(ignored = true) open val inDungeon: Dungeon,
                       open val inDungeonId: String,
                       open val isWalkable: Boolean,
                       open var position: Coordinates,
                       open val user: User? = null) {

    @Json(ignored = true)
    var newTurn = true

    @Volatile
    var toBeRemoved = false

    open var life = 1000
        set(value) {
            field =
                if (value <= 0) {
                    0
                } else
                    value
        }
    open var maxLife = 1000

    @Json(ignored = true)
    open val fow = 15

    var turnsPassed = 0
    var dataStack = mutableListOf<String>()

    /**
     * Modifies this entity's id for a randomly generated one not duplicaetd in the giving dungeon
     */
    open fun generateId (dungeon: Dungeon?): Entity {
        var valid = dungeon == null
        do {
            val finalId =  Random().nextInt(100000000)
            dungeon?.gridEntities?.forEach {
                valid = !it.filter { it != null }.any { it?.id == finalId }
                if (!valid)
                    return@forEach
            }
            if (valid)
                id = finalId
        } while (!valid)

        return this
    }

    /**
     * Checks if provided position is within fow of this entity
     */
    fun isAround(coordinates: Coordinates): Boolean {
        if (position-coordinates <= fow)
            return true
        return false
    }

    fun findEntityByPosition (coordinates: Coordinates, dungeon: Dungeon? = Dungeon.findDungeonFromEntity(this)): Entity? {
        if (dungeon == null)
            return null

        return Entity.findEntityByPosition(coordinates, dungeon)
    }

    override fun toString(): String {
        return "${javaClass.simpleName}-$id@$position"
    }

    companion object {

        /**
         * @return Entity in the given coordinates from the given dungeon or null if not found.
         */
        fun findEntityByPosition (coordinates: Coordinates, dungeon: Dungeon): Entity?
                = try {
                    dungeon.gridEntities[coordinates.y][coordinates.x]
                } catch (e: IndexOutOfBoundsException) {
                    null
                }

        fun findEntityById (id: Int, dungeon: Dungeon? = null): Entity? {
            var entity: Entity? = null
            if (dungeon != null) {
                Dungeon.dungeons.toMutableList().apply { add(dungeon) }
            } else {
                Dungeon.dungeons
            }.forEach {
                if (entity != null)
                    return@forEach

                if (dungeon == null || it == dungeon) {
                    it.gridEntities.forEach {
                        it.forEach {
                            if (it != null && it.id == id) {
                                entity = it
                                return entity
                            }
                        }
                    }
                    return@forEach
                }
            }
            return entity
        }

        fun findEntitiesByUserId (id: Int, dungeon: Dungeon? = null): List<Entity> {
            val entities = mutableListOf<Entity>()
            if (dungeon != null) {
                Dungeon.dungeons.toMutableList().apply { add(dungeon) }
            } else {
                Dungeon.dungeons
            }.forEach {
                if (it.players.any { it.id == id }) {
                    it.gridEntities.forEach {
                        val notNulled = it.filter { it != null }
                        val withUsers = notNulled.filter { it!!.user != null }
                        val ourDamnUser = withUsers.filter { it!!.user!!.id == id }
                        ourDamnUser.forEach {
                            entities.add(it!!)
                        }
                    }
                    return@forEach
                }
            }
            return entities
        }
    }
}