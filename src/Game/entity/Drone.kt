package Game.entity

import Game.entity.roles.RolesEnum
import Game.world.Coordinates
import Game.world.Dungeon
import Web.users.User
import Web.utils.TimeFormat
import com.beust.klaxon.Json
import java.util.*
import kotlin.math.roundToInt


class Drone (override var id: Int,
             override var name: String,
             override val mapCode: String,
             override val isWalkable: Boolean,
             override val user: User,
//             override var inDungeon: Dungeon,
             override val inDungeonId: String,
             var isDead: Boolean = false,
             override var position: Coordinates
): Entity(id, name, mapCode, inDungeonId, /*inDungeon, */isWalkable, position, user), Energizable, EnergyOreHolder, PlayerOwned {

    var level = 1
    var equipped: Boolean = false
    var assignedRefinery: Refinery? = null

    override var maxEnergy: Int = 500
    override var storedEnergy = maxEnergy
        set(value) {
            if (value in 0..maxEnergy)
                field = value
            else if (value > maxEnergy)
                field = maxEnergy
        }

    override var storedEnergyOre = 0
    override var maxEnergyOre = 250

    var maxAttack = 100
    var maxRepair = 50
    var solarRechargeRate = 10
    var neededEnergyToMove = 10
    var neededEnergyToBuild = 10
    var neededEnergyToAttack = 10
    var neededEnergyToMine = 10
    var equipmentBonus = 1.5f

    var role: RolesEnum = RolesEnum.NoRole

    @Json(ignored = true) var failedMoves = 0  // Sanity check helper

    /**
     * @return true if moved, false otherwise
     */
    fun move (destination: Coordinates): Boolean {
//        println("${TimeFormat.formattedTime} $id drone wants to move to $destination")
        if (position - destination == 1 && storedEnergy >= neededEnergyToMove && Dungeon.findDungeonFromEntity(this)!!.gridEntities[destination.y][destination.x]?.isWalkable != false) {
//            println("${TimeFormat.formattedTime} $id drone will move, using 1 energy, from $storedEnergy -> ${storedEnergy-1}")
            storedEnergy -= neededEnergyToMove
            Dungeon.dungeons.find { it.dungeonId == inDungeonId }!!.gridEntities[destination.y][destination.x] = this
            Dungeon.dungeons.find { it.dungeonId == inDungeonId }!!.gridEntities[position.y][position.x] = null
            position = destination

            failedMoves = 0
            return true
        }
//        println("${TimeFormat.formattedTime} $id drone wont move")
        failedMoves++
        if (failedMoves > 10)
            println("${TimeFormat.formattedTime} $id drone has failed to move $failedMoves consecutive times")
        return false
    }

    /**
     * Will move one tile closer to given destination using some pathfinding algorithm
     *
     * @return 1 if the given destination has been reached, 0 if not, -1 if the work was launched but not yet computed, -2 if no energy is left
     */
    fun moveTowards (destination: Coordinates): Int {
        if (failedMoves > 10)
            println("${TimeFormat.formattedTime} $id drone wants to move towards $destination")

        if (storedEnergy < neededEnergyToMove)
            return -2

        val path: List<Coordinates>?

        path = Dungeon.dungeons.find { it.dungeonId == inDungeonId }!!.pathfinder.findShortestPath(position, destination)

        if (path.isNotEmpty()) {
            val moveResult = move(path.first())

            // If we have moved we want to update the flood cache for the position we just left

            return if (moveResult && destination == position) 1 else 0
        }

        if (failedMoves > 10)
            println("${TimeFormat.formattedTime} $id drone couldn't move towards $destination as the path is $path")

        return 0
    }

    /**
     * If given a refinery will try to recharge to max.
     * If a refinery is not given will gain solarRechargeRate energy using it's solar panels
     */
    fun recharge (refinery: Refinery? = null): Int {
//        println("${TimeFormat.formattedTime} $id drone wants to recharge")

        if (refinery != null && refinery.position - this.position != 1) {
            return -1
        }

        var recharged = 0
        if (refinery != null && refinery.position - position == 1 && refinery.storedEnergy >= 10) {
            while (storedEnergy < maxEnergy && refinery.storedEnergy >= 10) {
                storedEnergy += refinery.rechargeRate
                refinery.storedEnergy -= refinery.rechargeRate
                recharged += refinery.rechargeRate
            }
        }else {
            // Use solar panel
//            println("${TimeFormat.formattedTime} $id drone is going to recharge using it's solar panels")
            if (storedEnergy < maxEnergy) {
                storedEnergy += solarRechargeRate
                recharged += solarRechargeRate
            }
        }
//        println("${TimeFormat.formattedTime} $id drone have recharged $recharged units of energy: ${storedEnergy-recharged} -> $storedEnergy")
        return recharged
    }

    /**
     * If given a armoury will try to equip.
     */
    fun getEquipment (armoury: Armoury): Boolean {
//        println("${TimeFormat.formattedTime} $id drone wants to recharge")

        if (equipped)
            return true

        if (armoury.position - this.position != 1) {
            return false
        } else if (armoury.storedEquipments > 0) {
            armoury.storedEquipments--
            this.maxAttack = (this.maxAttack*equipmentBonus).roundToInt()
            this.maxLife = (this.maxLife*equipmentBonus).roundToInt()
            this.equipped = true

            return true
        }
//        println("${TimeFormat.formattedTime} $id drone have recharged $recharged units of energy: ${storedEnergy-recharged} -> $storedEnergy")
        return false
    }

    /**
     * Try to mine some EnergyOre
     * @return Number of units mined or -1 if no EnergyOre was at position or we were too far or -2 if not enough energy
     */
    fun mineEnergyOre (ore: EnergyOre): Int {
//        println("${TimeFormat.formattedTime} $id drone wants to mine ore from ${Klaxon().toJsonString(ore)}")
        if (ore.position - this.position != 1) {
            return -1
        }

        if (storedEnergy < neededEnergyToMine)
            return -2

        storedEnergy -= neededEnergyToMine

        val randomMine = Random().nextInt(maxEnergyOre- storedEnergyOre +1)
        ore.energyOreRemaining -= randomMine
        storedEnergyOre += randomMine
//        println("${TimeFormat.formattedTime} $id drone has mined $randomMine units of ore: ${energyOre-randomMine} -> $energyOre")
        return randomMine
    }

    /**
     * Remove one build counter for an on-build building in position position
     * @return Build counters left on building or -1 if too far away or already built or not enough storedEnergy
     */
    fun buildBuilding(building: Building): Int {
        if (building.buildProgress == 0 || storedEnergy < neededEnergyToBuild) {
            return -1
        }
        if (building.position - this.position != 1) {
            return -1
        }
        storedEnergy -= neededEnergyToBuild
        building.buildProgress--
        return building.buildProgress
    }

    /**
     * Try to store 1 energy of the drone into the building
     * @return true if 1 energy has been stored into the building, false otherwise
     */
    fun storeEnergyInBuilding (building: Building): Boolean {
        if (building.position - this.position != 1) {
            return false
        }
        if (building.storedEnergy == building.maxEnergy || storedEnergy < 1)
            return false
        storedEnergy -= 10
        building.storedEnergy += 10
        return true
    }

    /**
     * Try to store all energy ore of the drone into the refinery
     * @return true if energy ore has been stored into the refinery, false otherwise
     */
    fun storeEnergyOreInRefinery (refinery: Refinery): Boolean {
//        println("${TimeFormat.formattedTime} $id drone wants to store energy ore in $refinery")
        if (refinery.position - this.position != 1) {
            return false
        }
//        println("${TimeFormat.formattedTime} $id drone has stored $energyOre units of ore")
        refinery.storedEnergyOre += storedEnergyOre
        storedEnergyOre = 0

        recharge(refinery)  // After storing all ore we recharge from the refinery

        return true
    }

    /**
     * Tries to attack an entity, will substract a random ammount of life (0..maxAttack) if near
     * @return damage dealt or -1 if too far or no energy left for attacking
     */
    fun attack (entity: Entity): Int {
        if (entity.position - this.position != 1) {
            return -1
        }
        if (storedEnergy < neededEnergyToAttack)
            return -1

        storedEnergy -= neededEnergyToAttack

        val attack = Random().nextInt(maxAttack)
        entity.life -= attack
        return attack
    }

    /**
     * Tries to repair an entity, will add a random ammount of life (0..maxRepair) if near
     * @return repair dealt or -1 if too far or no energy left for attacking
     */
    fun repair (entity: Entity): Int {
        if (entity.position - this.position != 1) {
            return -1
        }
        if (storedEnergy <= 0)
            return -1
        val repair = Random().nextInt(maxRepair)
        entity.life += repair
        return repair
    }

    /**
     * Will level up this drone summing levels from this and given drone (will consume/kill given drone in the process; Handled at Server.kt)
     *
     * @return Newly upgraded level from this drone
     */
    fun levelUp (fusionDrone: Drone): Int {
        // Checking for rules about when fusing 2 drones is allowed will be done in Server.kt.
        // Ex. No 2 drones with different roles should ever be combined

        level += fusionDrone.level

        // Update all stats to be combined
        if (fusionDrone.equipped)
            equipped = true

        maxLife += fusionDrone.maxLife
        life += fusionDrone.life

        maxEnergy += fusionDrone.maxEnergy
        storedEnergy += fusionDrone.storedEnergy

        maxAttack += fusionDrone.maxAttack
        maxRepair += fusionDrone.maxRepair

        maxEnergyOre += fusionDrone.maxEnergyOre
        storedEnergyOre += fusionDrone.storedEnergyOre

        solarRechargeRate += fusionDrone.solarRechargeRate

        // Removing of fusionDrone will also be handled at Server.kt

        return level
    }

    companion object {
        val code = EntityCode.D
    }

}