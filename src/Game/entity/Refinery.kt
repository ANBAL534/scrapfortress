package Game.entity

import Game.world.Coordinates
import Web.users.User
import kotlin.random.Random

class Refinery (override var id: Int,
                override var name: String,
                override val mapCode: String,
                override val isWalkable: Boolean,
                override val user: User,
//                override var inDungeon: Dungeon,
                override val inDungeonId: String,
                override var position: Coordinates
): Building(id, name, mapCode, user, inDungeonId, /*inDungeon, */isWalkable, position), EnergyOreHolder, PlayerOwned {

    override var buildProgress = 5  // 0 means completed

    override var storedEnergyOre = 0
        get() {
            return if (buildProgress != 0)
                -1
            else
                field
        }
    override var maxEnergyOre = Integer.MAX_VALUE

    override var storedEnergy = 0
        get() {
            return if (buildProgress != 0)
                -1
            else
                field
        }
        set(value) {
            if (value in 0..maxEnergy)
                field = value
        }

    override var maxEnergy: Int = 1000
    var rechargeRate = 100

    var energyOreConsumption = 10
    var maxEnergyGeneration = 50

    fun refine(): Boolean {
        if (storedEnergyOre >= energyOreConsumption && storedEnergy != maxEnergy) {
            storedEnergyOre -= energyOreConsumption
            storedEnergy += Random.nextInt(1, maxEnergyGeneration)
            return true
        }
        return false
    }

    companion object {
        val code = EntityCode.R
    }
}