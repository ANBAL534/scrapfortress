package Game.entity

import Game.world.Coordinates
import Game.world.Dungeon
import Web.users.User

class DroneFactory (override var id: Int,
                    override var name: String,
                    override val mapCode: String,
                    override val isWalkable: Boolean,
                    override val user: User,
//                    override var inDungeon: Dungeon,
                    override val inDungeonId: String,
                    override var position: Coordinates
): Building(id, name, mapCode, user, inDungeonId, /*inDungeon, */isWalkable, position), PlayerOwned {

    override var buildProgress = 10//0  // 0 means completed

    override var storedEnergy = 0
        get() {
            return if (buildProgress != 0)
                -1
            else
                field
        }
        set(value) {
            if (value in 0..maxEnergy)
                field = value
        }

    override var maxEnergy: Int = 6500
    var energyNeededForDrone = 300
    var energyToManufacture = 10

    var droneProgress = 10//energyNeededForDrone  // at 0 a new drone should spawn
        set(value) {
            field =
                if (value < 0)
                    0
                else
                    value
        }

    fun manufacture(): Int {
        val dungeon = Dungeon.findDungeonFromEntity(this)!!

        if (dungeon.population[user]!! >= dungeon.populationLimit)
            return -1

        if (storedEnergy >= energyToManufacture) {
            storedEnergy -= energyToManufacture
            droneProgress -= 10
        }

        if (droneProgress == 0) {
            droneProgress = energyNeededForDrone
            val existingUp = dungeon.gridEntities[position.y-1][position.x]
            if (existingUp == null || existingUp::class == Wall::class)
                dungeon.spawnEntity(
                    (Drone(-1,
                        "${User.users.first{ it.id == user.id}.name}'s Drone",
                        EntityCode.D.name,
                        false,
                        user,
                        this.inDungeonId,
                        false,
                        Coordinates.getCoordinates(position.x, position.y - 1, 0)
                    )
                        .generateId(dungeon) as Drone).apply {
                        storedEnergy = 0  // New drones starts without energy
                        name = "${User.users.first{ it.id == user.id}.name}'s Drone ($id)"
                    }
                )

            else
                droneProgress = 0  // If the new drone didn't spawn for whatever reason we set to 0 drone progress
            return 0
        }
        return droneProgress
    }

    companion object {
        val code = EntityCode.F
    }
}