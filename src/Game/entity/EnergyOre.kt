package Game.entity

import Game.world.Coordinates
import java.util.*

class EnergyOre (override var id: Int,
                 override var name: String,
                 override val mapCode: String,
                 override val isWalkable: Boolean,
//                 override var inDungeon: Dungeon,
                 override val inDungeonId: String,
                 override var position: Coordinates
): Entity(id, name, mapCode, inDungeonId, /*inDungeon, */isWalkable, position) {

    var energyOreRemaining = Random().nextInt(19999)+1
        set(value) {
            field =
                if (value <= 0) {
                    0
                } else
                    value
        }

    override var life = 2500
        set(value) {
            field =
                if (value <= 0) {
                    0
                } else
                    value
        }

    override var maxLife = 2500
}