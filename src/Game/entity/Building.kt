package Game.entity

import Game.world.Coordinates
import Web.users.User

abstract class Building (override var id: Int,
                         override var name: String,
                         override val mapCode: String,
                         override val user: User,
//                         override val inDungeon: Dungeon,
                         override val inDungeonId: String,
                         override val isWalkable: Boolean,
                         @Volatile override var position: Coordinates
                         ): Entity(id, name, mapCode, inDungeonId, /*inDungeon, */isWalkable, position, user), Energizable {

    open var buildProgress = 5  // 0 means completed
}