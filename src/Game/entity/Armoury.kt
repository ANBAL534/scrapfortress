package Game.entity

import Game.world.Coordinates
import Web.users.User

class Armoury (override var id: Int,
               override var name: String,
               override val mapCode: String,
               override val isWalkable: Boolean,
               override val user: User,
//                    override var inDungeon: Dungeon,
               override val inDungeonId: String,
               override var position: Coordinates
): Building(id, name, mapCode, user, inDungeonId, /*inDungeon, */isWalkable, position), PlayerOwned {

    override var buildProgress = 10//0  // 0 means completed

    override var storedEnergy = 0
        get() {
            return if (buildProgress != 0)
                -1
            else
                field
        }
        set(value) {
            if (value in 0..maxEnergy)
                field = value
        }

    override var maxEnergy: Int = 6500

    val energyNeededForEquipment = 300

    var equipmentProgress = 1//energyNeededForEquipment  // at 0 a new equipment should be ready
        set(value) {
            field =
                if (value < 0)
                    0
                else
                    value
        }

    val maxStoredEquipments = 5
    var storedEquipments = 0
        get() {
            return if (buildProgress != 0)
                -1
            else
                field
        }
        set(value) {
            if (value in 0..maxStoredEquipments)
                field = value
        }

    fun manufacture(): Int {
        if (storedEnergy > 0) {
            storedEnergy--
            equipmentProgress--
        }

        if (equipmentProgress == 0 && storedEquipments < maxStoredEquipments) {
            equipmentProgress = energyNeededForEquipment
            storedEquipments++
            return 0
        }
        return equipmentProgress
    }

    companion object {
        val code = EntityCode.A
    }
}