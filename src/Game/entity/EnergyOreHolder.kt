package Game.entity

interface EnergyOreHolder {

    var storedEnergyOre: Int
    var maxEnergyOre: Int

}