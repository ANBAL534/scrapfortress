package Game.entity

interface Energizable {

    var storedEnergy: Int
    var maxEnergy: Int

}