package Game.world.pathfinding.astar

import Game.world.Coordinates
import Game.world.Dungeon
import java.util.*

class AStar(
    private val dungeon: Dungeon,
    private val xstart: Int,
    private val ystart: Int,
    private val diag: Boolean
) {
    private val open: LinkedList<AStarNode>
    private val closed: LinkedList<AStarNode>
    private var now: AStarNode? = null
    private var xend: Int = 0
    private var yend: Int = 0

    // Node class for convienience
    class AStarNode(var parent: AStarNode?, var x: Int, var y: Int, var g: Double, var h: Double) {
        // Compare by f value (g + h)
        fun compareToAny(o: Any): Int {
            val that = o as AStarNode
            return (this.g + this.h - (that.g + that.h)).toInt()
        }
    }

    init {
        this.open = LinkedList()
        this.closed = LinkedList()
        this.now = AStarNode(null, xstart, ystart, 0.0, 0.0)
    }

    /*
     ** Finds path to xend/yend or returns null
     **
     ** @param (int) xend coordinates of the target position
     ** @param (int) yend
     ** @return (List<Node> | null) the path
     */
    fun findPathTo(xend: Int, yend: Int): List<Coordinates>? {
        this.xend = xend
        this.yend = yend
        this.closed.add(this.now!!)
        addNeigborsToOpenList()
        while (this.now!!.x != this.xend || this.now!!.y != this.yend) {
            if (this.open.isEmpty()) { // Nothing to examine
                return null
            }
            if (Thread.currentThread().isInterrupted)
                break  // Out time given to compute the path has finished, return what we have
            this.now = this.open[0] // get first node (lowest f score)
            this.open.removeAt(0) // remove it
            this.closed.add(this.now!!) // and add to the closed
            addNeigborsToOpenList()
        }
        val path = mutableListOf<Coordinates>()
        path.add(0, Coordinates.getCoordinates(this.now!!.x, this.now!!.y, 0))
        while (this.now!!.x != this.xstart || this.now!!.y != this.ystart) {
            this.now = this.now!!.parent
            path.add(0, Coordinates.getCoordinates(this.now!!.x, this.now!!.y, 0))
        }
        return path
    }

    /*
     ** Calulate distance between this.now and xend/yend
     **
     ** @return (int) distance
     */
    private fun distance(dx: Int, dy: Int): Double {
        return if (this.diag) { // if diagonal movement is alloweed
            Math.hypot(
                (this.now!!.x + dx - this.xend).toDouble(),
                (this.now!!.y + dy - this.yend).toDouble()
            )
        } else {
            (Math.abs(this.now!!.x + dx - this.xend) + Math.abs(this.now!!.y + dy - this.yend)).toDouble() // else return "Manhattan distance"
        }
    }

    private fun addNeigborsToOpenList() {
        var node: AStarNode
        for (x in -1..1) {
            for (y in -1..1) {
                if (!this.diag && x != 0 && y != 0) {
                    continue // skip if diagonal movement is not allowed
                }
                node = AStarNode(this.now, this.now!!.x + x, this.now!!.y + y, this.now!!.g, this.distance(x, y))
                if ((x != 0 || y != 0) // not this.now

                    && this.now!!.x + x >= 0 && this.now!!.x + x < this.dungeon.size // check maze boundaries
                    && this.now!!.y + y >= 0 && this.now!!.y + y < this.dungeon.size

                    && this.dungeon.gridEntities[this.now!!.y + y][this.now!!.x + x] == null // check if square is walkable

                    && !findNeighborInList(this.open, node) && !findNeighborInList(this.closed, node)  // if not already done
                ) {
                    node.g = node.parent!!.g + 1.0 // Horizontal/vertical cost = 1.0
                    node.g += 1.0 // add movement cost for this square
                    this.open.add(node)
                }
            }
        }
        this.open.sortBy {that ->
            that.g + that.h
        }
    }

    companion object {
        /*
         ** Looks in a given List<> for a node
         **
         ** @return (bool) NeighbourInListFound
         */
        private fun findNeighborInList(array: List<AStarNode>, node: AStarNode): Boolean {
            return array.stream().anyMatch { n -> n.x == node.x && n.y == node.y }
        }
    }
}