package Game.world.pathfinding

import Game.entity.Entity
import Game.world.Coordinates
import Game.world.Dungeon
import Game.world.pathfinding.jump.org.ksdev.jps.Graph
import Game.world.pathfinding.jump.org.ksdev.jps.JPS
import Game.world.pathfinding.jump.org.ksdev.jps.Node
import java.util.*
import java.util.concurrent.Future


class Pathfinder(val dungeon: Dungeon) {

    private val nodeMap: MutableList<MutableList<Node>> by lazy {
        val nmap: MutableList<MutableList<Node>> = mutableListOf()
        for (i in 0 until dungeon.size)
            nmap.add(mutableListOf())
        for (row in nmap.withIndex())
            for (x in 0 until dungeon.size)
                row.value.add(Node(x, row.index).apply { isWalkable = dungeon.gridEntities[row.index][x] == null })
        nmap
    }

    /**
     * Will find the shortest path between the two points given and will return a list of the coordinates in the order to be followed.
     * Will return an empty list if no path was found.
     */
    fun findShortestPath(source: Coordinates, destination: Coordinates): List<Coordinates> {
        val destinationEntity = Entity.findEntityByPosition(destination, dungeon)

        if (source - destination == 0 || (source - destination == 1 && destinationEntity != null))
            return listOf()  // Final destination is the same source or the next cell that is not empty

        val finalDestination =
            if (destinationEntity == null)
                destination
            else {
                findNearestEmpty(source, destinationEntity.position)
            }

        // Update state of the dungeon
        for (row in nodeMap.withIndex())
            for (x in 0 until dungeon.size)
                row.value[x].isWalkable = dungeon.gridEntities[row.index][x] == null

        // Manually set source as walkable
        nodeMap[source.y][source.x].isWalkable = true

        val jps: JPS<Node> = JPS.JPSFactory.getJPS<Node>(Graph(nodeMap), Graph.Diagonal.NEVER)
        val futurePath: Future<Queue<Node>> = jps.findPath(nodeMap[source.y][source.x], nodeMap[finalDestination.y][finalDestination.x])
        val path: Queue<Node> = try { futurePath.get() } catch (e: IllegalStateException) { LinkedList<Node>().apply { add(nodeMap[source.y][source.x]) } }

        val foundPath = mutableListOf<Coordinates>()
        for (node in path)
            foundPath.add(Coordinates.getCoordinates(node.x, node.y, 0))

        println("Asked path from $source to $finalDestination computed -> ${foundPath.joinToString(",")}")

        return foundPath.drop(1)
    }

    /**
     * Will find the closest empty tile coordinates between the destination entity and the calling drone
     */
    fun findNearestEmpty(sourceCoords: Coordinates, destCoords: Coordinates): Coordinates {
        val multiClosest = mutableListOf(Coordinates.getCoordinates(0, 0, 0))
        var closest = Coordinates.getCoordinates(0, 0, 0)

        // Search for the closest empty tiles around the given entity
        dungeon.gridEntities.forEachIndexed { y, mutableList ->
            if (Math.abs(destCoords.y - y) < 10)  // If no use to search very far from our objective
                mutableList.forEachIndexed { x, entity ->
                    if (entity == null) {
                        val actualCoords = Coordinates.getCoordinates(x, y, 0)
                        if (actualCoords - destCoords < closest - destCoords) {
                            closest = actualCoords
                            multiClosest.removeAll { true }
                            multiClosest.add(actualCoords)
                        } else if (actualCoords - destCoords == closest - destCoords) {
                            multiClosest.add(actualCoords)
                        }
                    }
                }
        }

        // We may have multiple tiles at the same distance from the destination, we must choose the one closer to the drone
        var closestToDrone = closest
        multiClosest.forEach {
            if (it - sourceCoords < closestToDrone - sourceCoords)
                closestToDrone = it
        }

        return closestToDrone
    }
}