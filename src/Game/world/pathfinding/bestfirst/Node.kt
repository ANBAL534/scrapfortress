package Game.world.pathfinding.bestfirst

import Game.world.Coordinates

// Node class for convienience
internal class Node (
    var parent: Node?,
    var coords: Coordinates,
    var g: Double,
    var h: Double
) : Comparable<Any?> {

    // Compare by f value (g + h)
    override fun compareTo(o: Any?): Int {
        val that = o as Node?
        return (g + h - (that!!.g + that.h)).toInt()
    }

    override fun equals(o: Any?): Boolean {
        return if (o !is Node) false else coords.x == o.coords.x &&
                coords.y == o.coords.y
    }

}