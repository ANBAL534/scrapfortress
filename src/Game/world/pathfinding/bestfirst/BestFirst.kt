package Game.world.pathfinding.bestfirst

import Game.world.Coordinates
import java.util.*

internal class BestFirst(maze: Array<Array<Int>>, coords: Coordinates) {
    val open: LinkedList<Node>
    val closed: LinkedList<Node>
    val path: MutableList<Node>
    val maze: Array<Array<Int>>
    var now: Node
    val coords: Coordinates
    var numberGeneratedNodes = 0
    var numberExpandedNodes = 0
    var maxOpenNodes = 0
    var cost = 0

    init {
        open = LinkedList()
        closed = LinkedList()
        path = mutableListOf()
        this.maze = maze
        now = Node(null, coords, 0.0, 0.0)
        this.coords = coords
    }

    /**
     * Given a set of possible destinations, this methods finds a path to any of them and returns a list of ordered nodes to be followed
     * @param destination
     * @return
     */
    fun findPathTo(destination: Coordinates): List<Node>? {
        closed.add(now)
        addNeigborsToOpenList()
        while (now.coords != destination) {
            if (open.isEmpty()) { // Nothing to examine
                return null
            }

//            if (Thread.currentThread().isInterrupted)
//                break  // Our time given to compute the path has finished, return what we have

            now = open[0] // get first node (lowest f score)
            open.removeAt(0) // remove it
            numberExpandedNodes++
            closed.add(now) // and add to the closed
            addNeigborsToOpenList()
        }
        path.add(0, now)
        while (now.coords.x != coords.x || now.coords.y != coords.y) {
            now = now.parent!!
            path.add(0, now)
            cost++
        }
        return path
    }

    private fun distance(coord1: Coordinates, coord2: Coordinates): Double {
        return Math.abs(coord2.x - coord1.x) + Math.abs(coord2.y - coord1.y).toDouble()
    }

    private fun addNeigborsToOpenList() {
        var node: Node
        for (x in -1..1) {
            for (y in -1..1) {
                if (x != 0 && y != 0) {
                    continue  // skip if diagonal movement is not allowed
                }
                node = Node(
                    now,
                    Coordinates.getCoordinates(now.coords.x + x, now.coords.y + y, 0),
                    0.0,
                    distance(now.coords, Coordinates.getCoordinates(now.coords.x + x, now.coords.y + y, 0))
                )
                if ((x != 0 || y != 0) // not now.coords
                    && now.coords.x + x >= 0 && now.coords.x + x < maze[0].size // check maze boundaries
                    && now.coords.y + y >= 0 && now.coords.y + y < maze.size && maze[now.coords.y + y][now.coords.x + x] != -1 // check if square is walkable
                    && !findNeighborInList(
                        open,
                        node
                    ) && !findNeighborInList(closed, node)
                ) { // if not already done
                    open.add(node)
                    numberGeneratedNodes++
                    if (open.size > maxOpenNodes) maxOpenNodes = open.size
                }
            }
        }
        open.sort()
    }

    companion object {
        /**
         * Looks in a given List<> for a node
         * This method is used to check when adding new neighbours to the open list that the given nod is not in the open nor closed list
         *
         * @return
         */
        private fun findNeighborInList(
            array: List<Node>,
            node: Node
        ): Boolean {
            for (n in array) {
                if (n == node) return true
            }
            return false
        }

        private fun findCoordinatesInList(
            array: List<Coordinates>,
            coordinates: Coordinates
        ): Boolean {
            for (n in array) {
                if (n == coordinates) return true
            }
            return false
        }
    }
}