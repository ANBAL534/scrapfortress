package Game.world.pathfinding.Flood

import Game.entity.Entity
import Game.world.Coordinates
import Game.world.Dungeon
import Web.utils.TimeFormat
import java.lang.Thread.sleep
import kotlin.math.abs

class Flood(val maze: Array<Array<Int>>, val origin: Coordinates) {

    // Based on http://roguebasin.roguelikedevelopment.org/index.php?title=The_Incredible_Power_of_Dijkstra_Maps
    fun findPathFrom(yourPosition: Coordinates, updateMaze: Boolean = true): List<Coordinates> {
        if (updateMaze)
            computeFlood()  // To prepare the maze cache var with the flood values (will try to get them from dungeon cache first)

        // Now maze has all the values set
        // If the caller is standing on a 0, there is no connection between the caller and the destination or is already on the destination
        if (maze[yourPosition.y][yourPosition.x] == 0) {
            println("${TimeFormat.formattedTime} $origin and $yourPosition are not connected, returning empty path")
            return listOf()
        }

        println("Pathfinding from $yourPosition to $origin")

        // Just follow the flood until caller is where he wants saving the path for returning it
        val path = mutableListOf<Coordinates>()
        var currentPos = yourPosition
        val possibleMoves = mutableListOf<Coordinates>()
        val possibleEqualMoves = mutableListOf<Coordinates>()
        while (currentPos != origin) {
            possibleMoves.removeAll { true }
            possibleEqualMoves.removeAll { true }

            for (yOffset in -1..1) {
                for (xOffset in -1..1) {
                    if (abs(xOffset) != abs(yOffset)) {  // Ignore diagonals and center
                        try {
                            when {
                                maze[currentPos.y+yOffset][currentPos.x+xOffset] < maze[currentPos.y][currentPos.x] -> {
                                    possibleMoves.add(Coordinates.getCoordinates(currentPos.x+xOffset, currentPos.y+yOffset, 0))
                                }
                                maze[currentPos.y+yOffset][currentPos.x+xOffset] <= maze[currentPos.y][currentPos.x] -> {
                                    possibleEqualMoves.add(Coordinates.getCoordinates(currentPos.x+xOffset, currentPos.y+yOffset, 0))
                                }
                            }
                        } catch (e: IndexOutOfBoundsException) {
                            // Ignored, out of the map
                        }
                    }
                }
            }
            possibleMoves.sortBy { maze[it.y][it.x] }
            possibleEqualMoves.sortBy { maze[it.y][it.x] }
            currentPos =
                if (possibleMoves.isEmpty()) {
                    try {
                        path.add(possibleEqualMoves.first())
                    } catch (e: Exception) {
                        // Error for no viable options to continue the pathfinding
                        println("ERROR WHEN PATHFINDING ROUTE FROM $yourPosition TO $origin")
                        println("Computed path up to the error: ${path.joinToString(",")}")
                        println("currenPos = $currentPos")
                        println("Entity at source: ${Entity.findEntityByPosition(currentPos, Dungeon.dungeons.first())}")
                        println("Entity at destination: ${Entity.findEntityByPosition(origin, Dungeon.dungeons.first())}")
                        println("Current map state:")
                        printMaze(currentPos)
                        sleep(10000)
                    }
                    possibleEqualMoves.first()
                } else {
                    path.add(possibleMoves.first())
                    possibleMoves.first()
                }
        }

        return path
    }

    /**
     * Computes a Dijkstra map to the origin given updating maze variable with the flood values
     */
    fun computeFlood() {
        maze[origin.y][origin.x] = -2

        // Start flooding and giving numbers to cells depending on how far are from the flood origin (destination coords)
        val open = mutableListOf(origin)
        var depth = 1
        while (open.isNotEmpty()) {
            for (coords in open.toList()) {
                open.remove(coords)
                for (yOffset in -1..1) {
                    for (xOffset in -1..1) {
                        if (abs(xOffset) != abs(yOffset)) {  // Ignore diagonals and center
                            try {
                                if (maze[coords.y+yOffset][coords.x+xOffset] == 0) {
                                    maze[coords.y+yOffset][coords.x+xOffset] = depth
                                    open.add(Coordinates.getCoordinates(coords.x+xOffset, coords.y+yOffset, 0))
                                }
                            } catch (e: IndexOutOfBoundsException) {
                                // Ignored
                            }
                        }
                    }
                }
            }
            depth++
        }
    }

    fun updateMazeFromDungeon(dungeon: Dungeon) {
        dungeon.gridEntities.forEachIndexed { y, mutableList ->
            mutableList.forEachIndexed { x, entity ->
                if (entity == null)
                    maze[y][x] = 0
                else
                    maze[y][x] = Int.MAX_VALUE
            }
        }
    }

    fun printMaze(specialCoords: Coordinates? = null) {
        println("Computed flood (G marking $specialCoords):")
        for (y in maze.indices) {
            for (x in maze.indices) {
                if (Coordinates.getCoordinates(x, y, 0) == specialCoords) {
                    print(" G")
                } else {
                    when (maze[y][x]) {
                        -1 -> print(" W")
                        -2 -> print(" O")
                        else -> print(" " +
                                if (maze[y][x] < 16)
                                    maze[y][x].toString(16)
                                else
                                    "+"
                        )
                    }
                }
            }
            println()
        }
    }
}