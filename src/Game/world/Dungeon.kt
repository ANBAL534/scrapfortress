package Game.world

import Game.entity.*
import Game.entity.roles.RoleQueue
import Game.noise4j.map.Grid
import Game.noise4j.map.generator.cellular.CellularAutomataGenerator
import Game.world.pathfinding.Pathfinder
import Web.users.User
import Web.utils.loadPyScript
import com.beust.klaxon.Json
import java.io.File
import java.util.*
import java.util.concurrent.locks.ReentrantLock
import kotlin.collections.HashMap


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class Dungeon (val size: Int, val location: Coordinates, val dungeonId: String, val roomSize: Int? = null) {

    /**
     * Creates a standard dungeon using provided parameters, if the parameters are not valid will fallback to valid ones
     *
     *  - max/min roomsizes must be odd
     *  - dungeon size must be far greater then room size
     */

    @Volatile
    var paused = false  // Only for reference to use in other classes

    @Json(ignored = true)
    var callerProcess: Process? = null

    // 1.0f is a wall
    @Json(ignored = true)
    var grid = Grid(size) // This algorithm likes odd-sized maps, although it works either way.
        private set
    var gridString = mutableListOf<MutableList<String>>()  // Original string for empty dungeon
    var gridEntities = mutableListOf<MutableList<Entity?>>()

    val pathfinder = Pathfinder(this)

    @Json(ignored = true)
    val gridChanges = mutableMapOf<User, MutableList<Entity>>()

    @Json(ignored = true)
    val dungeonLock = ReentrantLock(true)

    val energyOreExpansions = HashMap<Coordinates, MutableList<EnergyOre>>()
    val roleQueues = HashMap<User, RoleQueue>()
    val lastEnemiesSeenPositions = hashMapOf<User, HashMap<Entity, Coordinates>>()

    // This is a list of object references from gridEntities for a quicker iteration through
    val energyOres = mutableListOf<EnergyOre>()
    val drones = mutableListOf<Drone>()
    val buildings = mutableListOf<Building>()

    val populationLimit = 25
    val population = mutableMapOf<User, Int>()

    @Json(ignored = true)
    val players = mutableListOf<User>()

    @Json(ignored=true)
    val lastTurnTimes = IntArray(20)
    @Json(ignored = true)
    var lastTurnTimeIndex = 0
    @Json(ignored = true)
    @Volatile
    var averageTurnTime = 250

    init {
        for (i in 0..size) {
            val mlist = mutableListOf<String>()
            for (j in 0..size)
                mlist.add("")
            gridString.add(mlist)
        }
        for (i in 0..size) {
            val mlist = mutableListOf<Entity?>()
            for (j in 0..size)
                mlist.add(null)
            gridEntities.add(mlist)
        }
    }

    fun generateCavernString() {
        val cellularGenerator = CellularAutomataGenerator()
        cellularGenerator.aliveChance = 0.5f
        cellularGenerator.radius = 2
        cellularGenerator.birthLimit = 16
        cellularGenerator.deathLimit = 8
        cellularGenerator.iterationsAmount = 7
        cellularGenerator.generate(grid)

        for (y in 0 until size)
            for (x in 0 until size)
                if (grid[y, x] == 1.0f)
                    gridString[y][x] = EntityCode.W.name
                else
                    gridString[y][x] = " "
    }

    fun generateReservoirs(entityMapCode: String, clusteringRepetitions: Int = 3, frecuencyOfDeposits: Int = 2) {
        println("Creating material reservoirs")
        for (y in 0 until size)
            for (x in 0 until size)
                if (Random().nextInt(1000) < frecuencyOfDeposits)
                    gridString[y][x] = entityMapCode

        println("Clustering")
        for (i in 0..clusteringRepetitions) {
            for (y in 0 until gridString.size)
                for (x in 0 until gridString[0].size)
                    try {
                        if (gridString[y][x] == entityMapCode) {
                            if (Random().nextInt(100) < 20)
                                gridString[y + 1][x] = entityMapCode+entityMapCode
                            if (Random().nextInt(100) < 20)
                                gridString[y - 1][x] = entityMapCode+entityMapCode
                            if (Random().nextInt(100) < 20)
                                gridString[y][x + 1] = entityMapCode+entityMapCode
                            if (Random().nextInt(100) < 20)
                                gridString[y][x - 1] = entityMapCode+entityMapCode
                        }
                    }catch (e: Exception) {}
            for (y in 0 until gridString.size)
                for (x in 0 until gridString[0].size)
                    if (gridString[y][x] == entityMapCode+entityMapCode)
                        gridString[y][x] = entityMapCode
        }
    }

    fun insertGridStringEntities() {
        for (y in 0 until gridString.size)
            for (x in 0 until gridString[0].size)
                when (gridString[y][x]) {
                    EntityCode.W.name -> gridEntities[y][x] = Wall(-1, EntityCode.W.entityName, EntityCode.W.name, false,
                        dungeonId, Coordinates.getCoordinates(x, y, 0)
                    ).generateId(this)
                    EntityCode.E.name -> gridEntities[y][x] = EnergyOre(-1, EntityCode.E.entityName, EntityCode.E.name, false,
                        dungeonId, Coordinates.getCoordinates(x, y, 0)
                    ).generateId(this)
                }
    }

    fun exportText(): String {
        println("Exporting cavern to file")
        val stringBuilder = StringBuilder("")
        for (y in 0 until gridString.size) {
            stringBuilder.append(gridString[y] + "\n")
            println("Exporting lines $y/${gridString.size-1}")
        }
        val stringToFile = stringBuilder.toString().replace(", ", "").replace("]", "").replace("[", "").replace("\n\n", "\n")
        println("Saving file")
        File("exported_map/${stringToFile.hashCode()}.txt").writeText(stringToFile)
        return stringToFile
    }

    override fun toString(): String {
        val stringBuilder = StringBuilder("")
        for (y in 0 until gridEntities.size) {
            for (x in 0 until gridEntities.size) {
                stringBuilder.append(gridEntities[y][x]?.mapCode ?: " ")
            }
            stringBuilder.append("\n")
        }
        return stringBuilder.toString()
    }

    /**
     * Will search an adecuate spawnpoint for the given drones
     * Will also modify the viewpoint from the users in web mode
     * And will populate players list variable
     *
     * Once the drones has been spawned in the world will prepare the chages list for the first call from the Unity client
     *
     * Will also initialize some variables, arrays and hashmaps for the gameplay, such as a RoleQueue per user
     */
    fun spawnStartingDrones(droneList: List<Drone>) {
        // Populate players list
        droneList.forEach { players.add(it.user) }

        // Populate population limits
        droneList.forEach { population[it.user] = 0 }

        // Populate hashmap for the RoleQueue per user
        droneList.forEach { drone ->
            roleQueues[drone.user] = RoleQueue(File(User.users.find { it.name == "default" }!!.userPyPath).apply { mkdirs() }.listFiles()
                .first { it.nameWithoutExtension.startsWith("DefaultRoleQueue") }.readLines().dropLastWhile { it.isBlank() })
        }
        println("Populated RoleQueue from file")

        // Populate hashmap for the last enemies seen
        droneList.forEach { drone ->
            lastEnemiesSeenPositions[drone.user] = hashMapOf()
        }
        println("Initializated lastEnemiesSeenPositions")

        for (drone in droneList) {
            var trials = 0
            var valid = false
            while (!valid) {
                val x = Random().nextInt(gridString[0].size)
                val y = Random().nextInt(gridString.size)

                val coordxy = Coordinates.getCoordinates(x, y, 0)

                print("$coordxy checking valid coordinates")

                if (gridEntities[y][x] != null) {
                    println(" -> Not an empty position")
                    continue
                }
                // Get the closest energy ore from x, y

                var closest: EnergyOre = energyOres.first()
                for (energyOre in energyOres) {
                    if ((energyOre.position - coordxy < closest.position - coordxy))
                        closest = energyOre
                }

                if (drone.user.fow < x &&
                    drone.user.fow < y &&
                    x < gridString.size-drone.user.fow &&
                    y < gridString.size-drone.user.fow &&
                    (coordxy - closest.position) < (drone.user.fow-5) &&
                    (coordxy - closest.position) > 5 &&
                    !droneList.filter { it != drone }.any { it.position - coordxy < 50 }) {

                    // The space must be empty, not near the map borders, near a energy ore deposit, away from enemy drone

                    // Remove entities from a 5u radius around spawning point
                    for (yMap in gridEntities.indices)
                        for (xMap in gridEntities.indices)
                            if (gridEntities[yMap][xMap] != null && gridEntities[yMap][xMap]!!.position - coordxy <= 5)
//                                gridEntities[yMap][xMap] = null
                                removeEntity(gridEntities[yMap][xMap]!!)

                    valid = true
                    println(" -> Valid")
                    drone.position = Coordinates.getCoordinates(x, y, 0)
                    drone.user.viewPoint = drone.position

                    // Just before spawning the drones we have to set it's change list
                    val userEntities = Entity.findEntitiesByUserId(drone.user.id, this).toMutableList().apply { add(drone) }
                    // Initialize visible grid with nulls
                    val gridVisibleFinal = mutableListOf<MutableList<Entity?>>()
                    for (y in 0 until gridEntities.size) {
                        val mlist = mutableListOf<Entity?>()
                        for (x in 0 until gridEntities.size)
                            if (gridEntities[y][x] !is PlayerOwned)
                                mlist.add(gridEntities[y][x])
                            else
                                mlist.add(null)
                        gridVisibleFinal.add(mlist)
                    }

                    // Filter out non-visible entities and add them to the final visible grid
                    for (y in 0 until gridVisibleFinal.size)
                        for (x in 0 until gridVisibleFinal[y].size)
                            if (gridEntities[y][x] != null && userEntities.any { it.isAround(Coordinates.getCoordinates(x, y, 0)) })
                                gridVisibleFinal[y][x] = gridEntities[y][x]

                    val entities = mutableListOf<Entity>()
                    for (y in 0 until gridVisibleFinal.size) {
                        for (x in 0 until gridVisibleFinal[y].size)
                            if (gridVisibleFinal[y][x] != null)
                                entities.add(gridVisibleFinal[y][x]!!)
                    }

                    gridChanges[drone.user] = entities
                    println("Change list populated for Unity Client")
                }
                println()
                trials++
                if (trials > 500)
                    throw IllegalStateException("Exceeded number of spawn trials ($trials)")
            }
        }
        println("Drones has been set ready to be spawned spawned")

        droneList.forEach { spawnEntity(it) }
        println("Drones spawned")
    }

    fun spawnEntity(entity: Entity) {

        // Apply equipped cards to entity
        entity.user?.equippedCards?.forEach {
            it?.applyTo(entity)
        }

        gridEntities[entity.position.y][entity.position.x] = entity

        if (entity is Drone) {
            drones.add(entity)
            population[entity.user] = population[entity.user]!! + 1
        } else if (entity is Building)
            buildings.add(entity)

        // Try to load default script
        try {
            val script = File(User.users.find { it.name == "default" }!!.userPyPath).apply { mkdirs() }.listFiles()
                .first { it.nameWithoutExtension.startsWith("${entity::class.simpleName}-DEFAULT") }
            val fullPath = script.absolutePath
            val content = script.readText()
            loadPyScript(
                entity.user!!,
                entity.name,
                entity.id,
                entity::class.simpleName!!,
                script,
                this
            )

            File(fullPath).writeText(content)  // In loadPyScript the given script file is deleted after loading it, so we must rewrite it again
        } catch (e: NullPointerException) { /* This entity does not belong to any user */ }
        catch (e: NoSuchElementException) { /* No default script for the entity was found */ }

//        println("${entity.name} spawned at ${entity.position}")
    }

    fun removeEntity(entity: Entity) {
        entity.toBeRemoved = true
        gridEntities[entity.position.y][entity.position.x] = null

        when (entity) {
            is EnergyOre -> energyOres.remove(entity)
            is Drone -> drones.remove(entity)
            is Building -> buildings.remove(entity)
        }

        // If the entity is from an user, we should delete it's .py file too
        if (entity.user != null)
            File(entity.user!!.userPyPath).listFiles().find { it.nameWithoutExtension.contains("-${entity.id}_") }?.delete()
    }

    companion object {
        @Json(ignored = true)
        var dungeons: List<Dungeon> = listOf()
            private set(value) { field = value.sortedBy { it.dungeonId } }

        /**
         * Will add specified dungeon to dungeons List.
         *
         * @return false if it already exists and hasn't been added or true if it has been added
         */
        fun  addDungeon(dungeon: Dungeon): Boolean{
            if (dungeons.find { it.dungeonId == dungeon.dungeonId } != null)
                return false
            val dungeons = dungeons.toMutableList()
            dungeons.add(dungeon)
            Dungeon.dungeons = dungeons
            return true
        }
        fun generateRandomDungeon(size: Int = 256): Dungeon {
            val dungeon = Dungeon(size, Coordinates.getCoordinates(0, 0, 0), UUID.randomUUID().toString())
            try {
                // Generate dungeon and populate random entities
                dungeon.generateCavernString()
                dungeon.generateReservoirs(" ", 1)
                dungeon.generateReservoirs(EntityCode.E.name, 5)
                dungeon.insertGridStringEntities()

                // Populate energyOreClusters list for easy access
                val energyOreClusters: MutableList<MutableList<EnergyOre>> = mutableListOf()

                var cluster = mutableListOf<EnergyOre>()
                val toExplore = mutableListOf<EnergyOre>()
                val explored = mutableListOf<EnergyOre>()
                for (y in 0 until dungeon.gridEntities.size) {
                    for (x in 0 until dungeon.gridEntities.size) {
                        try {
                            if (dungeon.gridEntities[y][x] as EnergyOre !in toExplore &&
                                dungeon.gridEntities[y][x] as EnergyOre !in cluster &&
                                dungeon.gridEntities[y][x] as EnergyOre !in explored
                            ) {
                                toExplore.add(dungeon.gridEntities[y][x] as EnergyOre)

                                while (toExplore.isNotEmpty()) {
                                    cluster.add(toExplore.first())

                                    try {
                                        if (dungeon.gridEntities[toExplore.first().position.y+1][toExplore.first().position.x] as EnergyOre !in toExplore &&
                                            dungeon.gridEntities[toExplore.first().position.y+1][toExplore.first().position.x] as EnergyOre !in cluster &&
                                            dungeon.gridEntities[toExplore.first().position.y+1][toExplore.first().position.x] as EnergyOre !in explored
                                        ) toExplore.add(dungeon.gridEntities[toExplore.first().position.y+1][toExplore.first().position.x] as EnergyOre)
                                    } catch (e: Exception){}
                                    try {
                                        if (dungeon.gridEntities[toExplore.first().position.y-1][toExplore.first().position.x] as EnergyOre !in toExplore &&
                                            dungeon.gridEntities[toExplore.first().position.y-1][toExplore.first().position.x] as EnergyOre !in cluster &&
                                            dungeon.gridEntities[toExplore.first().position.y-1][toExplore.first().position.x] as EnergyOre !in explored
                                        ) toExplore.add(dungeon.gridEntities[toExplore.first().position.y-1][toExplore.first().position.x] as EnergyOre)
                                    } catch (e: Exception){}
                                    try {
                                        if (dungeon.gridEntities[toExplore.first().position.y][toExplore.first().position.x+1] as EnergyOre !in toExplore &&
                                            dungeon.gridEntities[toExplore.first().position.y][toExplore.first().position.x+1] as EnergyOre !in cluster &&
                                            dungeon.gridEntities[toExplore.first().position.y][toExplore.first().position.x+1] as EnergyOre !in explored
                                        ) toExplore.add(dungeon.gridEntities[toExplore.first().position.y][toExplore.first().position.x+1] as EnergyOre)
                                    } catch (e: Exception){}
                                    try {
                                        if (dungeon.gridEntities[toExplore.first().position.y][toExplore.first().position.x-1] as EnergyOre !in toExplore &&
                                            dungeon.gridEntities[toExplore.first().position.y][toExplore.first().position.x-1] as EnergyOre !in cluster &&
                                            dungeon.gridEntities[toExplore.first().position.y][toExplore.first().position.x-1] as EnergyOre !in explored
                                        ) toExplore.add(dungeon.gridEntities[toExplore.first().position.y][toExplore.first().position.x-1] as EnergyOre)
                                    } catch (e: Exception){}
                                    try {
                                        if (dungeon.gridEntities[toExplore.first().position.y-1][toExplore.first().position.x-1] as EnergyOre !in toExplore &&
                                            dungeon.gridEntities[toExplore.first().position.y-1][toExplore.first().position.x-1] as EnergyOre !in cluster &&
                                            dungeon.gridEntities[toExplore.first().position.y-1][toExplore.first().position.x-1] as EnergyOre !in explored
                                        ) toExplore.add(dungeon.gridEntities[toExplore.first().position.y-1][toExplore.first().position.x-1] as EnergyOre)
                                    } catch (e: Exception){}
                                    try {
                                        if (dungeon.gridEntities[toExplore.first().position.y+1][toExplore.first().position.x-1] as EnergyOre !in toExplore &&
                                            dungeon.gridEntities[toExplore.first().position.y+1][toExplore.first().position.x-1] as EnergyOre !in cluster &&
                                            dungeon.gridEntities[toExplore.first().position.y+1][toExplore.first().position.x-1] as EnergyOre !in explored
                                        ) toExplore.add(dungeon.gridEntities[toExplore.first().position.y+1][toExplore.first().position.x-1] as EnergyOre)
                                    } catch (e: Exception){}
                                    try {
                                        if (dungeon.gridEntities[toExplore.first().position.y-1][toExplore.first().position.x+1] as EnergyOre !in toExplore &&
                                            dungeon.gridEntities[toExplore.first().position.y-1][toExplore.first().position.x+1] as EnergyOre !in cluster &&
                                            dungeon.gridEntities[toExplore.first().position.y-1][toExplore.first().position.x+1] as EnergyOre !in explored
                                        ) toExplore.add(dungeon.gridEntities[toExplore.first().position.y-1][toExplore.first().position.x+1] as EnergyOre)
                                    } catch (e: Exception){}
                                    try {
                                        if (dungeon.gridEntities[toExplore.first().position.y+1][toExplore.first().position.x+1] as EnergyOre !in toExplore &&
                                            dungeon.gridEntities[toExplore.first().position.y+1][toExplore.first().position.x+1] as EnergyOre !in cluster &&
                                            dungeon.gridEntities[toExplore.first().position.y+1][toExplore.first().position.x+1] as EnergyOre !in explored
                                        ) toExplore.add(dungeon.gridEntities[toExplore.first().position.y+1][toExplore.first().position.x+1] as EnergyOre)
                                    } catch (e: Exception){}

                                    explored.add(toExplore.first())
                                    toExplore.removeAt(0)
                                }

                                // If we get here we have a full populated cluster set, add it to the dungeon list and reset
                                energyOreClusters.add(cluster)
                                cluster = mutableListOf()
                            }
                        } catch (e: Exception) {}
                    }
                }

                energyOreClusters.forEach { energyOreCluster ->
                    val midpoint = Coordinates.getCoordinates(dungeon.size/2, dungeon.size/2, 0)

                    var closestEnergyOre: EnergyOre? = null
                    energyOreCluster.forEach {
                        if (closestEnergyOre == null || midpoint - it.position < midpoint - closestEnergyOre!!.position)
                            closestEnergyOre = it
                    }

                    /*
                    We've got here the closest ore to the center, now prepare the 4 possible locations for the
                    expandPoint, choose the closest expandPoint to the center and erase a 4x4 area around it
                     */
                    var closestExpandPoint = Coordinates.getCoordinates(0,0,0)
                    mutableListOf<Coordinates>(
                        Coordinates.getCoordinates(closestEnergyOre!!.position.x + 4, closestEnergyOre!!.position.y + 4, 0),
                        Coordinates.getCoordinates(closestEnergyOre!!.position.x - 4, closestEnergyOre!!.position.y + 4, 0),
                        Coordinates.getCoordinates(closestEnergyOre!!.position.x + 4, closestEnergyOre!!.position.y - 4, 0),
                        Coordinates.getCoordinates(closestEnergyOre!!.position.x - 4, closestEnergyOre!!.position.y - 4, 0)
                    ).forEach {
                        if (it - midpoint < closestExpandPoint - midpoint)
                            closestExpandPoint = it
                    }

                    dungeon.energyOreExpansions[closestExpandPoint] = energyOreCluster

                    // Remove entities from a 4u radius around expand point
                    for (yMap in dungeon.gridEntities.indices)
                        for (xMap in dungeon.gridEntities.indices)
                            if (dungeon.gridEntities[yMap][xMap] != null && dungeon.gridEntities[yMap][xMap]!!.position - closestExpandPoint <= 4)
//                                dungeon.gridEntities[yMap][xMap] = null
                                dungeon.removeEntity(dungeon.gridEntities[yMap][xMap]!!)

                    // Check if there is a path from the selected expandpoint to the nearest ore in it's cluster, if not, increase delete radius
                    var validRadius = false
                    var extraRadius = 1
                    while (!validRadius) {
                        if (dungeon.pathfinder.findNearestEmpty(closestExpandPoint, closestEnergyOre!!.position) - closestEnergyOre!!.position > 1){
                            // Remove entities from a extra radius around expand point
                            for (yMap in dungeon.gridEntities.indices)
                                for (xMap in dungeon.gridEntities.indices)
                                    if (dungeon.gridEntities[yMap][xMap] != null && dungeon.gridEntities[yMap][xMap]!!.position - closestExpandPoint <= 4+extraRadius)
//                                        dungeon.gridEntities[yMap][xMap] = null
                                        dungeon.removeEntity(dungeon.gridEntities[yMap][xMap]!!)
                            extraRadius++
                        } else {
                            validRadius = true
                        }
                    }

                    // Then remove entities different from energy ore in 2u radius around the energy ore cluster
                    energyOreCluster.forEach {
                        for (yMap in dungeon.gridEntities.indices) {
                            for (xMap in dungeon.gridEntities.indices) {
                                val point = Coordinates.getCoordinates(xMap, yMap, 0)
                                if (dungeon.gridEntities[yMap][xMap] != null && it.position - point in 1..2 && dungeon.gridEntities[yMap][xMap] !is EnergyOre) {
                                    dungeon.removeEntity(dungeon.gridEntities[yMap][xMap]!!)
                                }
                            }
                        }
                    }
                }
            } catch (e: Exception) {
                println(e)
            }

            // Add all generated energy ores to the energyOres list
            for (y in 0 until size)
                for (x in 0 until size)
                    if (dungeon.gridEntities[y][x] is EnergyOre)
                        dungeon.energyOres.add(dungeon.gridEntities[y][x] as EnergyOre)

//            dungeon.pathfinder.computeDijkstraCache()

            println("Random dungeon generated (${dungeon.dungeonId})")
            return dungeon
        }

        /**
         * @return returns the dungeon object the provided entity is in, null if not found
         */
        fun findDungeonFromEntity (entity: Entity): Dungeon?
                = dungeons.find {
                    it.dungeonId == entity.inDungeonId
                }

        /**
         * @return removed dungeon
         */
        fun removeDungeonFromUserId(userId: Int): Dungeon {
            val userEntities = Entity.findEntitiesByUserId(userId)
            val userDungeon = findDungeonFromEntity(userEntities.first())!!
            dungeons = dungeons.toMutableList().apply { removeAll { it == userDungeon } }
            return userDungeon
        }
    }
}