package Game.world

import kotlin.math.abs

data class Coordinates (val x: Int,
                        val y: Int,
                        val z: Int) {

    override fun toString(): String {
        return "<Coordinates -> X:$x Y:$y Z:$z>"
    }

    fun toInt(): Int {
        // 1 XXX YYY ZZZ
        return 1000000000 + x*1000000 + y*1000 + z
    }

    operator fun minus(coordinates: Coordinates): Int {
        var difference = 0
        difference += abs(x - coordinates.x)
        difference += abs(y - coordinates.y)
        difference += abs(z - coordinates.z)
        return difference
    }

    companion object {
        private val coordinatesCache = mutableListOf<MutableList<MutableList<Coordinates>>>().apply {
            for (i in 0 until 128) {
                add(mutableListOf<MutableList<Coordinates>>().apply {
                    for (j in 0 until 128) {
                        add(mutableListOf<Coordinates>().apply {
//                            for (k in 0 until 128) {
//                                add(Coordinates(i, j, k))
//                            }
                            add(Coordinates(i, j, 0))
                        })
                    }
                })
            }
        }

        fun getCoordinates(x: Int, y: Int, z: Int): Coordinates {
            val coord: Coordinates = try {
                coordinatesCache[x][y][z]
            } catch (e: IndexOutOfBoundsException) {
                Coordinates(x, y, z)
            }

            // Sanity check
            if (coord.x != x || coord.y != y || coord.z != z)
                throw IllegalStateException("Inconsistent cached coordinate returned")

            return coord
        }
    }
}

fun Int.toCoordinates(): Coordinates {
    check(this >= 1000000000) { "Given number is not a codified Coordinates -> $this" }
    val x = Math.floor(((this.toFloat()-1000000000f)/1000000f).toDouble()).toInt()
    val y = Math.floor(((this.toFloat()-1000000000f+x.toFloat())/1000f).toDouble()).toInt()
    val z = this-1000000000+x+y

    return Coordinates.getCoordinates(x, y, z)
}