package Game.utils

import Game.entity.Entity
import Game.entity.EntityCode
import Game.world.Coordinates
import Game.world.Dungeon
import Web.users.User
import com.beust.klaxon.Json

class GetEnemyMapResponse (
    @Json(ignored = true)
    val user: User
) {
    val eCodes: MutableList<String> = mutableListOf()
    var mapString = ""

    init {
        val userEntities = Entity.findEntitiesByUserId(user.id)
        val dungeonFound = Dungeon.findDungeonFromEntity(userEntities[0])!!

        // Initialize visible grid with nulls
        val gridVisibleFinal = mutableListOf<MutableList<Char?>>()
        for (i in 0 .. dungeonFound.size) {
            val mlist = mutableListOf<Char?>()
            for (j in 0 .. dungeonFound.size)
                mlist.add(null)
            gridVisibleFinal.add(mlist)
        }

        // Filter out non-visible entities and add them to the final visible grid
        for (y in 0 until dungeonFound.size)
            for (x in 0 until dungeonFound.size)
                if (dungeonFound.gridEntities[y][x] != null &&
                        userEntities.any { it.isAround(Coordinates.getCoordinates(x, y, 0)) } &&
                        dungeonFound.gridEntities[y][x]?.user != null &&  // Race conditions are bad and I should feel bad
                        dungeonFound.gridEntities[y][x]?.user != userEntities.first().user)
                    gridVisibleFinal[y][x] = 'X'

        // Initialize small visible grid with nulls
        val gridVisibleVP = mutableListOf<MutableList<Char?>>()
        for (i in 0 .. user.fow*2+1) {  // a fow x fow grid
            val mlist = mutableListOf<Char?>()
            for (j in 0 .. user.fow*2+1)
                mlist.add(null)
            gridVisibleVP.add(mlist)
        }

        // Filter entities not around this user's viewPoint and add them to the smaller grid
        var relativeX = 0
        var relativeY = 0
        for (y in 0 until gridVisibleFinal.size) {
            for (x in 0 until gridVisibleFinal[y].size)
                if ((user.viewPoint.x - user.fow) <= x && x <= (user.viewPoint.x + user.fow) &&
                    (user.viewPoint.y - user.fow) <= y && y <= (user.viewPoint.y + user.fow)) {
                    gridVisibleVP[relativeY][relativeX] = gridVisibleFinal[y][x]
                    relativeX++
                }
            if (relativeX > 0) {
                relativeY++
                relativeX = 0
            }
        }

        val stringBuilder = StringBuilder("")
        for (y in 0 until gridVisibleVP.size) {
            for (x in 0 until gridVisibleVP[y].size)
                stringBuilder.append(gridVisibleVP[y][x] ?: " ")
            stringBuilder.append("\n")
        }
        mapString = stringBuilder.toString()
        for (eCode in EntityCode.values())
            eCodes.add(eCode.name)
    }
}