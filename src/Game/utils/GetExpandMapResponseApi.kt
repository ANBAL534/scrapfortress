package Game.utils

import Game.entity.Entity
import Game.entity.EntityCode
import Game.world.Coordinates
import Game.world.Dungeon
import Web.users.User
import com.beust.klaxon.Json

class GetExpandMapResponseApi (
    @Json(ignored = true)
    val user: User
) {
    val eCodes: MutableList<String> = mutableListOf()
    var mapString = ""

    init {
        val userEntities = Entity.findEntitiesByUserId(user.id)
        val dungeonFound = Dungeon.findDungeonFromEntity(userEntities[0])!!

        // Initialize grid with nulls
        val gridVisibleFinal = mutableListOf<MutableList<String?>>()
        for (i in 0 .. dungeonFound.size) {
            val mlist = mutableListOf<String?>()
            for (j in 0 .. dungeonFound.size)
                mlist.add(null)
            gridVisibleFinal.add(mlist)
        }

        // Filter out non-visible entities and add them to the final visible grid
        for (y in 0 until gridVisibleFinal.size)
            for (x in 0 until gridVisibleFinal[y].size)
                if (dungeonFound.energyOreExpansions.any { it.key == Coordinates.getCoordinates(x, y, 0) })
                    gridVisibleFinal[y][x] = "X"

        val stringBuilder = StringBuilder("")
        for (y in 0 until gridVisibleFinal.size) {
            for (x in 0 until gridVisibleFinal[y].size)
                stringBuilder.append(gridVisibleFinal[y][x] ?: " ")
            stringBuilder.append("\n")
        }
        mapString = stringBuilder.toString()
        for (eCode in EntityCode.values())
            eCodes.add(eCode.name)
    }
}