package Game.utils

import Game.entity.Drone
import Game.entity.Entity
import Game.world.Coordinates
import Game.world.Dungeon
import Web.users.User
import Web.utils.TimeFormat
import com.beust.klaxon.Klaxon
import java.io.File
import java.util.concurrent.locks.ReentrantLock
import kotlin.random.Random

object Matchmaking {

    @Volatile
    private var waiting1v1: User? = null

    private val lock = ReentrantLock(true)

    /**
     * Makes player search for a match, false if waiting or true if match found
     */
    fun match1v1(user: User): Boolean {
        lock.lock()
        println("${user.name} is matchmaking. waiting1v1=$waiting1v1")
        val result =
            if (Entity.findEntitiesByUserId(user.id).isNotEmpty())
                true
            else if (waiting1v1 == null || waiting1v1 == user) {
                waiting1v1 = user
                false
            } else {
                val matchingUser: User = waiting1v1!!
                var isValidDungeon = false
                while (!isValidDungeon) {
                    try {
                        val dungeon = Dungeon.generateRandomDungeon(99)
                        val drones = mutableListOf<Drone>()
                        drones.add(Drone(-1, "${user.name}'s drone", "D", false, user, dungeon.dungeonId, /*dungeon, */false,
                            Coordinates.getCoordinates(0, 0, 0)
                        ).generateId(dungeon) as Drone)
                        drones.add(Drone(-1, "${matchingUser.name}'s drone", "D", false, matchingUser, dungeon.dungeonId, /*dungeon, */false,
                            Coordinates.getCoordinates(0, 0, 0)
                        ).generateId(dungeon) as Drone)
                        dungeon.spawnStartingDrones(drones)

                        // If it hasn't throw an exception here it won't
                        Dungeon.addDungeon(dungeon)

                        // Launch caller.py limited to this users
                        dungeon.callerProcess = ProcessBuilder("python3 caller.py ${user.name},${matchingUser.name}".split(" "))
                            .directory(File("python/"))
                            .redirectOutput(ProcessBuilder.Redirect.INHERIT)
                            .redirectError(ProcessBuilder.Redirect.INHERIT)
                            .start()
                        isValidDungeon = true
                    } catch (e: IllegalStateException) {
                        // Ignore, just try again in the while
                        println("Invalid generated dungeon, generating a new one!")
                    }
                }

                waiting1v1 = null
                true
            }
        lock.unlock()
        return result
    }

    /**
     * Makes player search for a match, false if waiting or true if match found (Always true in training)
     */
    fun matchTraining(user: User): Boolean {
        if (Entity.findEntitiesByUserId(user.id).isNotEmpty())  // If already in a game
            return true

        var isValidDungeon = false
        while (!isValidDungeon) {
            try {
                val dungeon = Dungeon.generateRandomDungeon(99)
                val drones = mutableListOf<Drone>()
                drones.add(Drone(-1, "${user.name}'s drone", "D", false, user, dungeon.dungeonId, /*dungeon, */false,
                    Coordinates.getCoordinates(0, 0, 0)
                ).generateId(dungeon) as Drone)

                dungeon.spawnStartingDrones(drones)

                Dungeon.addDungeon(dungeon)

                // Launch caller.py limited to this users
                dungeon.callerProcess = ProcessBuilder("python3 caller.py ${user.name} afap".split(" "))
                    .directory(File("python/"))
                    .redirectOutput(ProcessBuilder.Redirect.INHERIT)
                    .redirectError(ProcessBuilder.Redirect.INHERIT)
                    .start()

                isValidDungeon = true
            } catch (e: IllegalStateException) {
                // Ignore, just try again in the while
                println("Invalid generated dungeon, generating a new one!")
            }
        }

        println("Game Started")
        return true
    }

    /**
     * Makes player search for a match, false if waiting or true if match found (Always true in training)
     */
    fun matchTrainingWithBot(user: User): Boolean {
        if (Entity.findEntitiesByUserId(user.id).isNotEmpty())  // If already in a game
            return true

        // Create a bot user to fight against
        var botId = Random.nextInt(10000, 99999)
        while (User.users.any { it.id == botId })
            botId = Random.nextInt(10000, 99999)
        val botUser = User("Bot-$botId", "5E965FB4AA9FC71B2CC25B16667583BDD589B9BC04F657DCC618D3F1363F06E9", botId)
        if (User.addUser(botUser))
            println("${TimeFormat.formattedTime} New bot registered: ${Klaxon().toJsonString(botUser)}")

        var isValidDungeon = false
        while (!isValidDungeon) {
            try {
                val dungeon = Dungeon.generateRandomDungeon(99)
                val drones = mutableListOf<Drone>()
                drones.add(Drone(-1, "${user.name}'s drone", "D", false, user, dungeon.dungeonId, /*dungeon, */false,
                    Coordinates.getCoordinates(0, 0, 0)
                ).generateId(dungeon) as Drone)
                drones.add(Drone(-1, "${botUser.name}'s drone", "D", false, botUser, dungeon.dungeonId, /*dungeon, */false,
                    Coordinates.getCoordinates(0, 0, 0)
                ).generateId(dungeon) as Drone)

                dungeon.spawnStartingDrones(drones)

                Dungeon.addDungeon(dungeon)

                // Launch caller.py limited to this users
                dungeon.callerProcess = ProcessBuilder("python3 caller.py ${user.name},${botUser.name} afap".split(" "))
                    .directory(File("python/"))
                    .redirectOutput(ProcessBuilder.Redirect.INHERIT)
                    .redirectError(ProcessBuilder.Redirect.INHERIT)
                    .start()

                isValidDungeon = true
            } catch (e: IllegalStateException) {
                // Ignore, just try again in the while
                println("Invalid generated dungeon, generating a new one!")
            }
        }

        println("Game Started")
        return true
    }
}