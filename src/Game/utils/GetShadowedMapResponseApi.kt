package Game.utils

import Game.entity.Entity
import Game.entity.EntityCode
import Game.world.Coordinates
import Game.world.Dungeon
import Web.users.User
import com.beust.klaxon.Json

class GetShadowedMapResponseApi (
    @Json(ignored = true)
    val user: User
) {
    val eCodes: MutableList<String> = mutableListOf()
    var mapString = ""

    init {
        val userEntities = Entity.findEntitiesByUserId(user.id)
        val dungeonFound = Dungeon.findDungeonFromEntity(userEntities[0])!!

        // Initialize visible grid with nulls
        val gridVisibleFinal = mutableListOf<MutableList<Entity?>>()
        for (i in 0 .. dungeonFound.size) {
            val mlist = mutableListOf<Entity?>()
            for (j in 0 .. dungeonFound.size)
                mlist.add(null)
            gridVisibleFinal.add(mlist)
        }

        // Filter out non-visible entities and add them to the final visible grid
        for (y in 0 until gridVisibleFinal.size)
            for (x in 0 until gridVisibleFinal[y].size)
                if (dungeonFound.gridEntities[y][x] != null && userEntities.any { it.isAround(Coordinates.getCoordinates(x, y, 0)) })
                    gridVisibleFinal[y][x] = dungeonFound.gridEntities[y][x]

        val stringBuilder = StringBuilder("")
        for (y in 0 until gridVisibleFinal.size) {
            for (x in 0 until gridVisibleFinal[y].size)
                stringBuilder.append(EntityCode.values().find { it.kclass?.isInstance(gridVisibleFinal[y][x]) ?: false }?.name ?: " ")
            stringBuilder.append("\n")
        }
        mapString = stringBuilder.toString()
        for (eCode in EntityCode.values())
            eCodes.add(eCode.name)
    }
}