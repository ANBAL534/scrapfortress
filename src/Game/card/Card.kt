package Game.card

import Game.entity.*
import Web.utils.TimeFormat
import Web.utils.sha256
import java.util.*
import kotlin.math.roundToInt

class Card (val cardHash: String) {

    var rarity = 0.0
    var bonus = mutableMapOf<String, Double>()
    var bonusLock = mutableMapOf<String, Boolean>()

    init {
        cardHash.forEachIndexed { index, it ->
            if (index > 12 && it == 'f') {
//                rarity *= it.toString().toInt(16).toDouble()/10.0
                rarity++
            }
        }
        rarity = rarity.roundToInt().toDouble()

        bonus["maxLifeBonus"] = 1.0
        bonus["maxEnergyBonus"] = 1.0
        bonus["maxAttackBonus"] = 1.0
        bonus["maxRepairBonus"] = 1.0
        bonus["solarRechargeRateBonus"] = 1.0
        bonus["neededEnergyToMoveBonus"] = 1.0
        bonus["neededEnergyToBuildBonus"] = 1.0
        bonus["neededEnergyToMineBonus"] = 1.0
        bonus["neededEnergyToAttackBonus"] = 1.0
        bonus["equipmentBonus"] = 1.0
        bonus["maxEnergyOreBonus"] = 1.0
        bonus["energyOreConsumptionBonus"] = 1.0
        bonus["maxEnergyGenerationBonus"] = 1.0
        bonus["energyNeededForDroneBonus"] = 1.0
        bonus["energyToManufactureBonus"] = 1.0

        // false means locked
        bonusLock["maxLifeBonus"] = cardHash[0].toString().toInt(16).toString(2).padStart(4, '0')[0] == '1' && bonusLock.toList().count { it.second } < 4
        bonusLock["maxEnergyBonus"] = cardHash[0].toString().toInt(16).toString(2).padStart(4, '0')[1] == '1' && bonusLock.toList().count { it.second } < 4
        bonusLock["maxAttackBonus"] = cardHash[0].toString().toInt(16).toString(2).padStart(4, '0')[2] == '1' && bonusLock.toList().count { it.second } < 4
        bonusLock["maxRepairBonus"] = cardHash[0].toString().toInt(16).toString(2).padStart(4, '0')[3] == '1' && bonusLock.toList().count { it.second } < 4
        bonusLock["solarRechargeRateBonus"] = cardHash[1].toString().toInt(16).toString(2).padStart(4, '0')[0] == '1' && bonusLock.toList().count { it.second } < 4
        bonusLock["neededEnergyToMoveBonus"] = cardHash[1].toString().toInt(16).toString(2).padStart(4, '0')[1] == '1' && bonusLock.toList().count { it.second } < 4
        bonusLock["neededEnergyToBuildBonus"] = cardHash[1].toString().toInt(16).toString(2).padStart(4, '0')[2] == '1' && bonusLock.toList().count { it.second } < 4
        bonusLock["neededEnergyToMineBonus"] = cardHash[1].toString().toInt(16).toString(2).padStart(4, '0')[3] == '1' && bonusLock.toList().count { it.second } < 4
        bonusLock["neededEnergyToAttackBonus"] = cardHash[2].toString().toInt(16).toString(2).padStart(4, '0')[0] == '1' && bonusLock.toList().count { it.second } < 4
        bonusLock["equipmentBonus"] = cardHash[2].toString().toInt(16).toString(2).padStart(4, '0')[1] == '1' && bonusLock.toList().count { it.second } < 4
        bonusLock["maxEnergyOreBonus"] = cardHash[2].toString().toInt(16).toString(2).padStart(4, '0')[2] == '1' && bonusLock.toList().count { it.second } < 4
        bonusLock["energyOreConsumptionBonus"] = cardHash[2].toString().toInt(16).toString(2).padStart(4, '0')[3] == '1' && bonusLock.toList().count { it.second } < 4
        bonusLock["maxEnergyGenerationBonus"] = cardHash[3].toString().toInt(16).toString(2).padStart(4, '0')[0] == '1' && bonusLock.toList().count { it.second } < 4
        bonusLock["energyNeededForDroneBonus"] = cardHash[3].toString().toInt(16).toString(2).padStart(4, '0')[1] == '1' && bonusLock.toList().count { it.second } < 4
        bonusLock["energyToManufactureBonus"] = cardHash[3].toString().toInt(16).toString(2).padStart(4, '0')[2] == '1' && bonusLock.toList().count { it.second } < 4

        var order = 0
        for (i in 12 until cardHash.length) {
            if (cardHash[i] == '0') {
                order++
                continue
            }

            val bonusNum = (cardHash[i].toString().toInt(16).toDouble()/10.0)+0.1
            when (order) {
                0 -> bonus["maxLifeBonus"] = bonus["maxLifeBonus"]!! * bonusNum
                1 -> bonus["maxEnergyBonus"] = (bonus["maxEnergyBonus"]!! * bonusNum)
                2 -> bonus["maxAttackBonus"] = (bonus["maxAttackBonus"]!! * bonusNum)
                3 -> bonus["maxRepairBonus"] = (bonus["maxRepairBonus"]!! * bonusNum)
                4 -> bonus["solarRechargeRateBonus"] = (bonus["solarRechargeRateBonus"]!! * bonusNum)
                5 -> bonus["neededEnergyToMoveBonus"] = (bonus["neededEnergyToMoveBonus"]!! * bonusNum)
                6 -> bonus["neededEnergyToBuildBonus"] = (bonus["neededEnergyToBuildBonus"]!! * bonusNum)
                7 -> bonus["neededEnergyToMineBonus"] = (bonus["neededEnergyToMineBonus"]!! * bonusNum)
                8 -> bonus["neededEnergyToAttackBonus"] = (bonus["neededEnergyToAttackBonus"]!! * bonusNum)
                9 -> bonus["equipmentBonus"] = (bonus["equipmentBonus"]!! * bonusNum)
                10 -> bonus["maxEnergyOreBonus"] = (bonus["maxEnergyOreBonus"]!! * bonusNum)
                11 -> bonus["energyOreConsumptionBonus"] = (bonus["energyOreConsumptionBonus"]!! * bonusNum)
                12 -> bonus["maxEnergyGenerationBonus"] = (bonus["maxEnergyGenerationBonus"]!! * bonusNum)
                13 -> bonus["energyNeededForDroneBonus"] = (bonus["energyNeededForDroneBonus"]!! * bonusNum)
                14 -> bonus["energyToManufactureBonus"] = (bonus["energyToManufactureBonus"]!! * bonusNum)
                else -> order = -1
            }
            order++
        }
    }

    fun applyTo(entity: Entity) {
        if (bonusLock["maxLifeBonus"]!!) {
            print("${TimeFormat.formattedTime} maxLife modified by card: [${entity.maxLife}] -> ")
            entity.maxLife = (entity.maxLife.toDouble() * bonus["maxLifeBonus"]!!).roundToInt()
            if (entity.maxLife < 1)
                entity.maxLife = 1
            println("[${entity.maxLife}]")
            entity.life = (entity.life.toDouble() * bonus["maxLifeBonus"]!!).roundToInt()
        }

        if (entity is Energizable)
            if (bonusLock["maxEnergyBonus"]!!) {
                print("${TimeFormat.formattedTime} maxEnergy modified by card: [${entity.maxEnergy}] -> ")
                entity.maxEnergy = (entity.maxEnergy.toDouble() * bonus["maxEnergyBonus"]!!).roundToInt()
                if (entity.maxEnergy < 1)
                    entity.maxEnergy = 1
                println("[${entity.maxEnergy}]")
                entity.storedEnergy = (entity.storedEnergy.toDouble() * bonus["maxEnergyBonus"]!!).roundToInt()
            }
        if (entity is Drone) {
            if (bonusLock["maxAttackBonus"]!!) {
                print("${TimeFormat.formattedTime} maxAttackBonus modified by card: [${entity.maxAttack}] -> ")
                entity.maxAttack = (entity.maxAttack.toDouble() * bonus["maxAttackBonus"]!!).roundToInt()
                if (entity.maxAttack < 1)
                    entity.maxAttack = 1
                println("[${entity.maxAttack}]")
            }
            if (bonusLock["maxRepairBonus"]!!) {
                print("${TimeFormat.formattedTime} maxRepair modified by card: [${entity.maxRepair}] -> ")
                entity.maxRepair = (entity.maxRepair.toDouble() * bonus["maxRepairBonus"]!!).roundToInt()
                if (entity.maxRepair < 1)
                    entity.maxRepair = 1
                println("[${entity.maxRepair}]")
            }
            if (bonusLock["solarRechargeRateBonus"]!!) {
                print("${TimeFormat.formattedTime} solarRechargeRate modified by card: [${entity.solarRechargeRate}] -> ")
                entity.solarRechargeRate = (entity.solarRechargeRate.toDouble() * bonus["solarRechargeRateBonus"]!!).roundToInt()
                if (entity.solarRechargeRate < 1)
                    entity.solarRechargeRate = 1
                println("[${entity.solarRechargeRate}]")
            }
            if (bonusLock["neededEnergyToMoveBonus"]!!) {
                print("${TimeFormat.formattedTime} neededEnergyToMove modified by card: [${entity.neededEnergyToMove}] -> ")
                entity.neededEnergyToMove = (entity.neededEnergyToMove.toDouble() * bonus["neededEnergyToMoveBonus"]!!).roundToInt()
                if (entity.neededEnergyToMove < 1)
                    entity.neededEnergyToMove = 1
                println("[${entity.neededEnergyToMove}]")
            }
            if (bonusLock["neededEnergyToBuildBonus"]!!) {
                print("${TimeFormat.formattedTime} neededEnergyToBuild modified by card: [${entity.neededEnergyToBuild}] -> ")
                entity.neededEnergyToBuild = (entity.neededEnergyToBuild.toDouble() * bonus["neededEnergyToBuildBonus"]!!).roundToInt()
                if (entity.neededEnergyToBuild < 1)
                    entity.neededEnergyToBuild = 1
                println("[${entity.neededEnergyToBuild}]")
            }
            if (bonusLock["neededEnergyToMineBonus"]!!) {
                print("${TimeFormat.formattedTime} neededEnergyToMine modified by card: [${entity.neededEnergyToMine}] -> ")
                entity.neededEnergyToMine = (entity.neededEnergyToMine.toDouble() * bonus["neededEnergyToMineBonus"]!!).roundToInt()
                if (entity.neededEnergyToMine < 1)
                    entity.neededEnergyToMine = 1
                println("[${entity.neededEnergyToMine}]")
            }
            if (bonusLock["neededEnergyToAttackBonus"]!!) {
                print("${TimeFormat.formattedTime} neededEnergyToAttack modified by card: [${entity.neededEnergyToAttack}] -> ")
                entity.neededEnergyToAttack = (entity.neededEnergyToAttack.toDouble() * bonus["neededEnergyToAttackBonus"]!!).roundToInt()
                if (entity.neededEnergyToAttack < 1)
                    entity.neededEnergyToAttack = 1
                println("[${entity.neededEnergyToAttack}]")
            }
            if (bonusLock["equipmentBonus"]!!) {
                print("${TimeFormat.formattedTime} equipmentBonus modified by card: [${entity.equipmentBonus}] -> ")
                entity.equipmentBonus = (entity.equipmentBonus.toDouble() * bonus["equipmentBonus"]!!).toFloat()
                if (entity.equipmentBonus < 0.1f)
                    entity.equipmentBonus = 0.1f
                println("[${entity.equipmentBonus}]")
            }
        }
        if (entity is EnergyOreHolder)
            if (bonusLock["maxEnergyOreBonus"]!!) {
                print("${TimeFormat.formattedTime} maxEnergyOre modified by card: [${entity.maxEnergyOre}] -> ")
                entity.maxEnergyOre = (entity.maxEnergyOre.toDouble() * bonus["maxEnergyOreBonus"]!!).roundToInt()
                entity.storedEnergyOre = (entity.storedEnergyOre.toDouble() * bonus["maxEnergyOreBonus"]!!).roundToInt()
                if (entity.maxEnergyOre < 1)
                    entity.maxEnergyOre = 1
                println("[${entity.maxEnergyOre}]")
            }
        if (entity is Refinery) {
            print("${TimeFormat.formattedTime} energyOreConsumption modified by card: [${entity.energyOreConsumption}] -> ")
            if (bonusLock["energyOreConsumptionBonus"]!!) {
                entity.energyOreConsumption = (entity.energyOreConsumption.toDouble() * bonus["energyOreConsumptionBonus"]!!).roundToInt()
                if (entity.energyOreConsumption < 1)
                    entity.energyOreConsumption = 1
                println("[${entity.energyOreConsumption}]")
            }
            if (bonusLock["maxEnergyGenerationBonus"]!!) {
                print("${TimeFormat.formattedTime} maxEnergyGeneration modified by card: [${entity.maxEnergyGeneration}] -> ")
                entity.maxEnergyGeneration = (entity.maxEnergyGeneration.toDouble() * bonus["maxEnergyGenerationBonus"]!!).roundToInt()
                if (entity.maxEnergyGeneration < 1)
                    entity.maxEnergyGeneration = 1
                println("[${entity.maxEnergyGeneration}]")
            }
        }
        if (entity is DroneFactory) {
            if (bonusLock["energyNeededForDroneBonus"]!!) {
                print("${TimeFormat.formattedTime} energyNeededForDrone modified by card: [${entity.energyNeededForDrone}] -> ")
                entity.energyNeededForDrone = (entity.energyNeededForDrone.toDouble() * bonus["energyNeededForDroneBonus"]!!).roundToInt()
                if (entity.energyNeededForDrone < 1)
                    entity.energyNeededForDrone = 1
                println("[${entity.energyNeededForDrone}]")
            }
            if (bonusLock["energyToManufactureBonus"]!!) {
                print("${TimeFormat.formattedTime} energyToManufacture modified by card: [${entity.energyToManufacture}] -> ")
                entity.energyToManufacture = (entity.energyToManufacture.toDouble() * bonus["energyToManufactureBonus"]!!).roundToInt()
                if (entity.energyToManufacture < 1)
                    entity.energyToManufacture = 1
                println("[${entity.energyToManufacture}]")
            }
        }
    }

    /**
     * Will return a new Card object resulting from the combination of this card's hash and the provided one.
     * The function used to produce the new hash is deterministic.
     */
    fun combineWith(card: Card): Card {
        var newHash = ""
        cardHash.forEachIndexed { index, c ->
            newHash += when {
                index < 12 && c.toString().toInt(16) % 2 == 0 -> {
                    c.toString()
                }
                index < 12 && c.toString().toInt(16) % 2 != 0 -> {
                    card.cardHash[index].toString()
                }
                else -> {
                    val a = c.toString().toInt(16)
                    val b = try {
                        card.cardHash[index].toString().toInt(16)
                    } catch (e: StringIndexOutOfBoundsException) {
                        a
                    }
                    val r = ((a+b).toDouble()/1.9).roundToInt()
                    if (r > 16) "f" else r.toString(16)
                }
            }
        }
        return Card(newHash)
    }

    override fun toString(): String {
        var toStringString = "Card $cardHash\n" +
                "    Rarity: $rarity\n" +
                "    Bonuses:\n"

        for (singleBonus in bonus.toList()) {
            toStringString += "        ${singleBonus.first}: ${singleBonus.second}\n"
        }

        return toStringString
    }
}

fun main (args: Array<String>) {
//    for (j in 0..10) {
//        val cards = mutableListOf<Card>()
//        val seed = Random().nextInt()
//        for (i in 0..1_000_000) {
//            val hashCard = "${seed+i}".sha256()
//            cards.add(Card(hashCard))
//        }
////        cards.add(Card("ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"))
//
//        println("Less rarity card rarity: ${cards.minBy { it.rarity }!!.rarity}")
//        println("Most rarity card rarity: ${cards.maxBy { it.rarity }!!.rarity}")
//        println("Average rarity card rarity: ${cards.sumByDouble { it.rarity }/cards.size.toDouble()}")
//        println("Number of cards with rarity greater than 1: ${cards.count { it.rarity >= 1.0 }}")
//        println()
//        println("Most rarity card:")
//        println(cards.maxBy { it.rarity }!!)
//        println()
//    }
    val seed = Random().nextInt()
    var card1: Card
    var card3 = Card("${Random().nextInt()}".sha256())
    for (i in 0..50) {
        card1 = Card("${seed+i}".sha256())
        if (card1.bonus["maxLifeBonus"]!! > card3.bonus["maxLifeBonus"]!! )
            card3 = card1.combineWith(card3)
    }
    println(card3)
}