package Web.webapi

import Game.card.Card
import Web.utils.TimeFormat
import Web.utils.WebLogin
import Web.utils.WebLogin.users
import Web.utils.sha256
import io.ktor.application.ApplicationCall
import io.ktor.response.respondRedirect
import java.util.*

object GenerateNewCard {
    suspend fun answer (call: ApplicationCall) {
        val user = users[WebLogin.getSessionName(call)]!!

        val seed = Random().nextInt()
        var mod = 0
        var card = Card("${seed + mod}".sha256())
        mod++
        while (card.rarity < 10) {
            card = Card("${seed + mod}".sha256())
            mod++
        }
        user.cards.add(card)

        println("${TimeFormat.formattedTime} (generate_new_card) card (${card.cardHash}) generated and added to ${user.name}'s inventory")

        call.respondRedirect("/inventory", permanent = false)
    }
}