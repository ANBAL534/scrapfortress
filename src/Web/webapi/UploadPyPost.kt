package Web.webapi

import Game.entity.Entity
import Game.entity.EntityCode
import Web.utils.TimeFormat
import Web.utils.WebLogin
import Web.utils.WebLogin.users
import Web.utils.copyToSuspend
import Web.utils.loadPyScript
import io.ktor.application.ApplicationCall
import io.ktor.http.content.PartData
import io.ktor.http.content.readAllParts
import io.ktor.http.content.streamProvider
import io.ktor.request.receiveMultipart
import io.ktor.response.respondRedirect
import java.io.File

object UploadPyPost {

    val uploadDir = "temp"  // Change this route in production

    suspend fun answer (call: ApplicationCall) {
        println("${TimeFormat.formattedTime} ${WebLogin.getSessionName(call)} is uploading a new script")

        val multipart = call.receiveMultipart()
        var pythonFile: File? = null
        lateinit var klassName: String
        lateinit var entityName: String
        var entityId = -1

        val parts = multipart.readAllParts()
        // Processes each part of the multipart input content of the user
        parts.forEach { part ->
            if (part is PartData.FormItem) {
                if (part.name == "code") {
                    try {
                        klassName = part.value.split("-")[0]
                        entityId = part.value.split("-")[1].toInt()
                        if (!Entity.findEntitiesByUserId(users[WebLogin.getSessionName(call)]!!.id).any { it.id == entityId })
                            throw Exception("Modified ID")
                        if (!EntityCode.values().any { it.entityName == klassName })
                            throw Exception("Modified Entity")
                    } catch (e: Exception) {
                        // Someone modified upload html form and changed the title
                        println("${TimeFormat.formattedTime} ${WebLogin.getSessionName(call)} gave an error -> ${e.message}")
                        call.respondRedirect("/play")
                    }
                } else if (part.name == "name") {
                    entityName = part.value  // Just save the value and update later as the fields may come disordered
                }
            } else if (part is PartData.FileItem) {
                val ext = File(part.originalFileName).extension
                if (ext == "py" && entityId != -1) {
                    val file = File(
                        uploadDir,
                        "$klassName-$entityId.$ext"
                    )
                    file.writeText("")
                    //TODO Limit file max size before upload/on it
                    part.streamProvider().use { its ->
                        file.outputStream().buffered().use {
                            its.copyToSuspend(it, 1*1024*1024)
                        }
                    }
                    if (file.length() < 1 * 1024 * 1024)
                        pythonFile = file
                }else  // Wrong file type
                    call.respondRedirect("/play")
            }

            part.dispose()
        }

        loadPyScript(
            users[WebLogin.getSessionName(call)]!!,
            entityName,
            entityId,
            klassName,
            pythonFile!!
        )

        call.respondRedirect("/play")
    }
}