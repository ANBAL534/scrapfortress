package Web.webapi

import Game.entity.*
import Web.utils.WebLogin
import Web.utils.WebLogin.users
import io.ktor.application.ApplicationCall
import io.ktor.response.respondText
import kotlinx.html.*
import kotlinx.html.stream.createHTML

object EntityStats {
    suspend fun answer (call: ApplicationCall) {
        val html =
            createHTML().div {
                hr()
                val entities = Entity.findEntitiesByUserId(users[WebLogin.getSessionName(call)]!!.id).sortedBy { it.name }
                for (entity in entities) {
                    div (classes = "form-group") {
                        p {
                            label {
                                +entity.name
                            }
                            small (classes = "form-text") { +"Turns: ${entity.turnsPassed}" }
                            small (classes = "form-text") { +"Life: ${entity.life}/${entity.maxLife}" }

                            try {
                                // If the entity is a Drone
                                small (classes = "form-text") { +"Energy: ${(entity as Drone).storedEnergy}/${entity.maxEnergy}" }
                                small (classes = "form-text") { +"Ore: ${(entity as Drone).storedEnergyOre}/${entity.maxEnergyOre}" }
                                small (classes = "form-text") { +"Role: ${(entity as Drone).role}" }
                                small (classes = "form-text") { +"Level: ${(entity as Drone).level}" }
                            } catch (e: Exception) {}
                            try {
                                // If the entity is a Building
                                if ((entity as Building).buildProgress > 0)
                                    small (classes = "form-text") { +"Build Progress: ${(entity as Building).buildProgress}" }
                            } catch (e: Exception) {}
                            try {
                                // If the entity is a Refinery
                                small (classes = "form-text") { +"Energy: ${(entity as Refinery).storedEnergy}/${entity.maxEnergy}" }
                                small (classes = "form-text") { +"Ore: ${(entity as Refinery).storedEnergyOre}" }
                            } catch (e: Exception) {}
                            try {
                                // If the entity is a Drone Factory
                                small (classes = "form-text") { +"Energy: ${(entity as DroneFactory).storedEnergy}/${entity.maxEnergy}" }
                                small (classes = "form-text") { +"Drone Progress: ${(entity as DroneFactory).droneProgress}" }
                            } catch (e: Exception) {}
                        }
                    }
                    hr()
                }
            }
        call.respondText { html }
    }
}