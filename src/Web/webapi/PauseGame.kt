package Web.webapi

import Game.entity.Entity
import Game.world.Dungeon
import Web.utils.WebLogin
import Web.utils.WebLogin.users
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import kotlinx.html.body

object PauseGame {
    suspend fun answer(call: ApplicationCall) {
        Dungeon.findDungeonFromEntity(Entity.findEntitiesByUserId(users[WebLogin.getSessionName(call)]!!.id).first())?.paused = true
        call.respondHtml {
            body {
                +"OK"
            }
        }
    }
}