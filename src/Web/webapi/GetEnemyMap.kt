package Web.webapi

import Game.utils.GetEnemyMapResponse
import Web.utils.WebLogin
import Web.utils.WebLogin.users
import com.beust.klaxon.Klaxon
import io.ktor.application.ApplicationCall
import io.ktor.response.respondText

object GetEnemyMap {
    suspend fun answer(call: ApplicationCall) {
        call.respondText {
            try {
                Klaxon().toJsonString(GetEnemyMapResponse(users[WebLogin.getSessionName(call)]!!))
            } catch (e: IndexOutOfBoundsException) {
                println("Sending game finished...")
                "Game Finished"
            }
        }
    }
}