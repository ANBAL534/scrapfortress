package Web.webapi

import Web.utils.WebLogin
import Web.utils.WebLogin.users
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import kotlinx.html.body

object MoreFow {
    suspend fun answer (call: ApplicationCall) {
        users[WebLogin.getSessionName(call)]!!.fow += 5
        call.respondHtml {
            body {
                +"OK"
            }
        }
    }
}