package Web.webapi

import Game.world.Dungeon
import Web.utils.TimeFormat
import Web.utils.WebLogin
import Web.utils.WebLogin.users
import io.ktor.application.ApplicationCall
import io.ktor.response.respondRedirect

object EndPlay {
    suspend fun answer(call: ApplicationCall) {
        println("${TimeFormat.formattedTime} (end_play) ${WebLogin.getSessionName(call)} requested to end it's current game")

        val removedDungeon = Dungeon.removeDungeonFromUserId(users[WebLogin.getSessionName(call)]!!.id)

        removedDungeon.players.forEach {
            it.inGame = false
        }

        println("${TimeFormat.formattedTime} (end_play) games from users: ${removedDungeon.players.joinToString { it.name }} have ended by ${WebLogin.getSessionName(call)}'s request")

        call.respondRedirect("/menu", permanent = false)
    }
}