package Web.webapi

import Web.utils.TimeFormat
import Web.utils.WebLogin
import Web.utils.WebLogin.users
import Web.utils.copyToSuspend
import io.ktor.application.ApplicationCall
import io.ktor.http.content.PartData
import io.ktor.http.content.readAllParts
import io.ktor.http.content.streamProvider
import io.ktor.request.receiveMultipart
import io.ktor.response.respondRedirect
import java.io.File
import java.util.*

object UploadDefaultPyPost {

    const val uploadDir = "temp"  // Change this route in production

    suspend fun answer (call: ApplicationCall) {
        println("${TimeFormat.formattedTime} ${WebLogin.getSessionName(call)} is uploading a new script")

        val multipart = call.receiveMultipart()
        var pythonFile: File? = null
        lateinit var entityName: String
        val randomId = Random().nextInt(8999)+1000

        val parts = multipart.readAllParts()
        // Processes each part of the multipart input content of the user
        parts.forEach { part ->
            if (part is PartData.FormItem) {
                if (part.name == "name") {
                    entityName = part.value.replace(" ", "")  // Just save the value and update later as the fields may come disordered
                }
            } else if (part is PartData.FileItem) {
                val ext = File(part.originalFileName).extension
                if (ext == "py") {
                    val file = File(
                        uploadDir,
                        "$entityName-DEFAULT-$randomId.$ext"
                    )
                    file.writeText("")
                    //TODO Limit file max size before upload/on it
                    part.streamProvider().use { its ->
                        file.outputStream().buffered().use {
                            its.copyToSuspend(it, 1*1024*1024)
                        }
                    }
                    if (file.length() < 1 * 1024 * 1024)
                        pythonFile = file
                }else  // Wrong file type
                    call.respondRedirect("/menu")
            }
            part.dispose()
        }

//                println("${TimeFormat.formattedTime} ${WebLogin.getSessionName(call)}'s default script is for entity: $entityName")

        if (pythonFile != null) {
            var fileString = pythonFile!!.readText().replace("\r\n", "\n")

            // Check for structure in file
            if (
                fileString.contains("import game\\n\\n\\nclass GameObject\\(game\\.Game, game\\.\\w+\\):\\n\\n    def __init__\\(self\\):\\n        super\\(\\).__init__\\(game\\.Game\\.entity_id, game\\.Game\\.user_id\\)".toRegex())
                && fileString.contains("def start\\(self\\):".toRegex())
                && fileString.contains("def loop\\(self\\):".toRegex())
            ) {
                // TODO check for illegal code
                // Do not Hard-code variables here as this are default generics

                // Create it's folders if needed
                File(users[WebLogin.getSessionName(call)]!!.userPyPath).mkdirs()  // TODO change for quotas http://souptonuts.sourceforge.net/quota_tutorial.html

                // Move the modified file to it's final folder with a new version number
                val filesToDelete = File(users[WebLogin.getSessionName(call)]!!.userPyPath).list()  // Get a list of this entity's scripts ordered by version
                    .filter { it.startsWith("$entityName-DEFAULT") }
                    .sortedBy {
                        it.split("_").last().removeSuffix(".py").toInt()
                    }
                val lastIndex =
                    try {
                        filesToDelete.last().split("_").last().removeSuffix(".py").toInt() + 1  // Get next version number
                    } catch (e: NoSuchElementException) {
                        1
                    }
                filesToDelete.forEach { File(users[WebLogin.getSessionName(call)]!!.userPyPath, it).delete() }  // Delete other versions, hopefully only one
                File(users[WebLogin.getSessionName(call)]!!.userPyPath, "$entityName-DEFAULT-${randomId}_$lastIndex.py").writeText(fileString)  // Write new script with new version number

                // Remove temporal original uploaded file
                pythonFile!!.delete()
            }
        }

        call.respondRedirect("/uploadDefaultPy?upload=$entityName")
    }
}