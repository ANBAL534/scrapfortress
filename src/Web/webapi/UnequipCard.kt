package Web.webapi

import Web.utils.WebLogin
import Web.utils.WebLogin.users
import io.ktor.application.ApplicationCall
import io.ktor.request.receiveParameters
import io.ktor.response.respondRedirect

object UnequipCard {
    suspend fun answer (call: ApplicationCall) {
        val user = users[WebLogin.getSessionName(call)]!!
        val post = call.receiveParameters()

        if (user.inGame) {
            call.respondRedirect("/play", permanent = false)
            return
        }

        val card = user.cards.find { it.cardHash == post["hash"]!! && it in user.equippedCards }
        user.equippedCards[user.equippedCards.indexOf(card)] = null

        call.respondRedirect("/inventory", permanent = false)
    }
}