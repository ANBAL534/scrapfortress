package Web.webapi

import Web.utils.WebLogin
import Web.utils.WebLogin.users
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import kotlinx.html.body

object MoveCameraLeft {
    suspend fun answer (call: ApplicationCall) {
        users[WebLogin.getSessionName(call)]!!.viewPoint = users[WebLogin.getSessionName(call)]!!.viewPoint.copy(x=users[WebLogin.getSessionName(call)]!!.viewPoint.x-2)
        call.respondHtml {
            body {
                +"OK"
            }
        }
    }
}