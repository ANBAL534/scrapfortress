package Web.webapi

import Web.utils.WebLogin
import Web.utils.WebLogin.users
import io.ktor.application.ApplicationCall
import io.ktor.request.receiveParameters
import io.ktor.response.respondRedirect

object CombineCard {
    suspend fun answer (call: ApplicationCall) {
        val user = users[WebLogin.getSessionName(call)]!!
        val post = call.receiveParameters()

        val card = user.cards.find { it.cardHash == post["hash"]!! }!!

        if (user.toCombineCards.isEmpty()) {
            user.toCombineCards.add(card)
        } else {
            val combinedCard = user.toCombineCards.first()!!.combineWith(card)
            user.cards.remove(card)
            user.cards.remove(user.toCombineCards.first()!!)
            try {
                user.equippedCards[user.equippedCards.indexOf(card)] = null
                user.equippedCards[user.equippedCards.indexOf(user.toCombineCards.first()!!)] = null
            } catch (e: IndexOutOfBoundsException) {
                // Ignored
            }
            user.cards.add(combinedCard)
            user.toCombineCards = mutableListOf()
        }

        call.respondRedirect("/inventory", permanent = false)
    }
}