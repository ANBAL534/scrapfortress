package Web.webapi

import Game.entity.Entity
import Game.world.Dungeon
import Web.utils.WebLogin
import Web.utils.WebLogin.users
import com.beust.klaxon.Klaxon
import io.ktor.application.ApplicationCall
import io.ktor.response.respondText

object AverageTurnTime {

    private val klaxon = Klaxon()

    suspend fun answer (call: ApplicationCall) {
        val user = users[WebLogin.getSessionName(call)]!!
        val dungeonFound = Dungeon.findDungeonFromEntity(Entity.findEntitiesByUserId(user.id).first())!!

        call.respondText { klaxon.toJsonString(dungeonFound.averageTurnTime) }
    }
}