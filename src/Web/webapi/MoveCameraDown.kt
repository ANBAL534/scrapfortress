package Web.webapi

import Web.utils.WebLogin
import Web.utils.WebLogin.users
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import kotlinx.html.body

object MoveCameraDown {
    suspend fun answer (call: ApplicationCall) {
        users[WebLogin.getSessionName(call)]!!.viewPoint = users[WebLogin.getSessionName(call)]!!.viewPoint.copy(y=users[WebLogin.getSessionName(call)]!!.viewPoint.y+2)
        call.respondHtml {
            body {
                +"OK"
            }
        }
    }
}