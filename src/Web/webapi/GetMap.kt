package Web.webapi

import Game.utils.GetMapResponse
import Web.utils.WebLogin
import Web.utils.WebLogin.users
import com.beust.klaxon.Klaxon
import io.ktor.application.ApplicationCall
import io.ktor.response.respondText

object GetMap {
    suspend fun answer(call: ApplicationCall) {
        call.respondText {
            try {
                Klaxon().toJsonString(GetMapResponse(users[WebLogin.getSessionName(call)]!!))
            } catch (e: IndexOutOfBoundsException) {
                println("Sending game finished...")
                "Game Finished"
            }
        }
    }
}