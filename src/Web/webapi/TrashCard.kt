package Web.webapi

import Web.utils.WebLogin
import Web.utils.WebLogin.users
import io.ktor.application.ApplicationCall
import io.ktor.request.receiveParameters
import io.ktor.response.respondRedirect

object TrashCard {
    suspend fun answer (call: ApplicationCall) {
        val user = users[WebLogin.getSessionName(call)]!!
        val post = call.receiveParameters()

        val card = user.cards.find { it.cardHash == post["hash"]!! }!!

        user.cards.remove(card)
        try {
            user.equippedCards[user.equippedCards.indexOf(card)] = null
        } catch (e: IndexOutOfBoundsException) {
            // Ignored
        }

        call.respondRedirect("/inventory", permanent = false)
    }
}