package Web.utils

import Game.entity.Entity
import Game.world.Dungeon
import Web.users.User
import java.io.File
import java.util.*

/**
 * For a given generic python script for a specific scriptable entity, this method will:
 *      - Update the entity's name
 *      - Check and prepare the given generic script for the specific entity
 *      - Write the modified script to the user folder and replace any existing script for the entity
 *      - Remove the file from temporal folder if it exist
 */
fun loadPyScript(
    user: User,
    entityName: String,
    entityId: Int,
    klassName: String,
    pythonFile: File,
    dungeon: Dungeon? = null
) {
    println("${TimeFormat.formattedTime} ${user.name}'s script is for entity: $entityName ($entityId) of type: $klassName")

    // Update entity's name
    Entity.findEntityById(entityId, dungeon)!!.name = entityName

    var fileString = pythonFile.readText().replace("\r\n", "\n")

    // Check for structure in file
    if (
        fileString.contains("import game\\n\\n\\nclass GameObject\\(game\\.Game, game\\.\\w+\\):\\n\\n    def __init__\\(self\\):\\n        super\\(\\).__init__\\(game\\.Game\\.entity_id, game\\.Game\\.user_id\\)".toRegex())
        && fileString.contains("def start\\(self\\):".toRegex())
        && fileString.contains("def loop\\(self\\):".toRegex())
    ) {
        // TODO check for illegal code
        // Hard-code variables
        fileString = fileString.replace("game.Game.entity_id", "$entityId")
        fileString = fileString.replace("game.Game.user_id", "${user.id}")

        // Create it's folders if needed
        File(user.userPyPath).mkdirs()  // TODO change for quotas http://souptonuts.sourceforge.net/quota_tutorial.html

        // Move the modified file to it's final folder with a new version number
        val filesToDelete =
            File(user.userPyPath).list()  // Get a list of this entity's scripts ordered by version
                .filter { it.startsWith("${klassName.replace(" ", "")}-$entityId") }
                .sortedBy {
                    it.split("_").last().removeSuffix(".py").toInt()
                }
        val lastIndex =
            try {
                filesToDelete.last().split("_").last().removeSuffix(".py").toInt() + 1  // Get next version number
            } catch (e: NoSuchElementException) {
                1
            }
        filesToDelete.forEach {
            File(
                user.userPyPath,
                it
            ).delete()
        }  // Delete other versions, hopefully only one
        File(
            user.userPyPath,
            "${klassName.replace(" ", "")}-${entityId}_$lastIndex.py"
        ).writeText(fileString)  // Write new script with new version number

        // Remove temporal original uploaded file
        pythonFile.delete()
    }
}