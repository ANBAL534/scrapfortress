package Web.utils

import Web.users.User
import Web.users.WebSession
import io.ktor.application.ApplicationCall
import io.ktor.sessions.get
import io.ktor.sessions.sessions
import java.util.*

object WebLogin {
    val users = Collections.synchronizedMap(
        listOf(User("default", "5E965FB4AA9FC71B2CC25B16667583BDD589B9BC04F657DCC618D3F1363F06E9", 0))
            .associateBy { it.name }
            .toMutableMap()
    )

    val loggedUsers = Collections.synchronizedMap(
        emptyList<WebSession>()
            .associateBy { it.name }
            .toMutableMap()
    )

    fun isLoggedIn(call: ApplicationCall): Boolean {
        val session = loggedUsers.getOrDefault(call.sessions.get<WebSession>()?.name ?: "", null)
        session ?: return false
        if (session.value != call.sessions.get<WebSession>()!!.value)
            return false
        return true
    }

    /**
     * Return username from the cookie or null if it is invalid or nonexistant
     */
    fun getSessionName(call: ApplicationCall): String? {
        val session = loggedUsers.getOrDefault(call.sessions.get<WebSession>()?.name ?: "", null)
        session ?: return null
        if (session.value != call.sessions.get<WebSession>()!!.value)
            return null
        return call.sessions.get<WebSession>()?.name
    }
}