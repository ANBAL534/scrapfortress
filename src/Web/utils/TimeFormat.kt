package Web.utils

import java.text.SimpleDateFormat
import java.util.*

object TimeFormat {

    val formattedTime: String
        get() {
            return try {
                val sdf = SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS")
                val netDate = Date(System.currentTimeMillis())
                ("[${sdf.format(netDate)}]")
            } catch (e: Exception) {
                e.toString()
            }
        }
}