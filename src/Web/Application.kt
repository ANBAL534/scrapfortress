@file:Suppress("DEPRECATION")

package Web

import Game.server.Server
import Web.api.*
import Web.pages.*
import Web.pages.Match
import Web.pages.TrainingAlone
import Web.pages.TrainingBot
import Web.users.SimpleJWT
import Web.users.WebSession
import Web.utils.TimeFormat
import Web.utils.WebLogin
import Web.webapi.*
import Web.webapi.AverageTurnTime
import Web.webapi.EndPlay
import Web.webapi.GetMap
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.auth.UserIdPrincipal
import io.ktor.auth.authenticate
import io.ktor.auth.jwt.jwt
import io.ktor.auth.principal
import io.ktor.features.*
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.content.default
import io.ktor.http.content.files
import io.ktor.http.content.static
import io.ktor.http.content.staticRootFolder
import io.ktor.response.respondRedirect
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.sessions.Sessions
import io.ktor.sessions.cookie
import io.ktor.sessions.duration
import java.io.File
import java.time.Duration

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {

    install(Compression) {
        gzip {
            priority = 1.0
        }
        deflate {
            priority = 10.0
            minimumSize(1024) // condition
        }
    }

    install(DefaultHeaders) {
        header("X-Engine", "Ktor") // will send this header with each response
    }

    // Configure CORS for the API to be accessible from other domains
    install(CORS) {
//        method(HttpMethod.Options)
        method(HttpMethod.Get)
        method(HttpMethod.Post)
//        method(HttpMethod.Put)
//        method(HttpMethod.Delete)
//        method(HttpMethod.Patch)
        header(HttpHeaders.Authorization)
        allowCredentials = true
        anyHost()  // Change this to implement banning / anti DDOS
    }

    // Configure JWT for auth
    val simpleJwt = SimpleJWT("my-super-secret-for-jwt")
    install(Authentication) {
        jwt {
            verifier(simpleJwt.verifier)
            validate {
                UserIdPrincipal(it.payload.getClaim("name").asString())
            }
        }
    }
    install(Sessions) {
        cookie<WebSession>("SF_Session") {
            cookie.path = "/"
            cookie.duration = Duration.ofDays(7)
        }
    }

    (Server() as Thread).start()

    routing {
        static("static") {
            staticRootFolder = File("resources${File.separator}static")
            files("images")
            files("scripts")
            default("/")
        }

        get("/") {
            Index.answer(call)
        }

        post("/login") {
            Login.answer(call, simpleJwt)
        }

        get("/menu") {
            if (WebLogin.isLoggedIn(call))
                MainMenu.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get("/match") {
            if (WebLogin.isLoggedIn(call))
                Match.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get("/training") {
            if (WebLogin.isLoggedIn(call))
                TrainingAlone.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get("/trainingBot") {
            if (WebLogin.isLoggedIn(call))
                TrainingBot.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get ("/resume_game") {
            if (WebLogin.isLoggedIn(call))
                ResumeGame.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get ("/pause_game") {
            if (WebLogin.isLoggedIn(call))
                PauseGame.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get ("/more_fow") {
            if (WebLogin.isLoggedIn(call))
                MoreFow.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get ("/less_fow") {
            if (WebLogin.isLoggedIn(call))
                LessFow.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get ("/move_cam_up") {
            if (WebLogin.isLoggedIn(call))
                MoveCameraUp.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get ("/move_cam_down") {
            if (WebLogin.isLoggedIn(call))
                MoveCameraDown.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get ("/move_cam_left") {
            if (WebLogin.isLoggedIn(call))
                MoveCameraLeft.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get ("/move_cam_right") {
            if (WebLogin.isLoggedIn(call))
                MoveCameraRight.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get("/play") {
            if (WebLogin.isLoggedIn(call))
                Play.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get("/get_map") {
            if (WebLogin.isLoggedIn(call))
                GetMap.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get("/get_enemy_map") {
            if (WebLogin.isLoggedIn(call))
                GetEnemyMap.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get("/get_last_seen_map") {
            if (WebLogin.isLoggedIn(call))
                GetLastSeenMap.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get("/end_play") {
            if (WebLogin.isLoggedIn(call))
                EndPlay.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get ("/entityStats") {
            if (WebLogin.isLoggedIn(call))
                EntityStats.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get ("/uploadPy") {
            if (WebLogin.isLoggedIn(call))
                UploadPyGet.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        post("/uploadPy") {
            if (WebLogin.isLoggedIn(call))
                UploadPyPost.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get ("/uploadDefaultPy") {
            if (WebLogin.isLoggedIn(call))
                UploadDefaultPyGet.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        post("/uploadDefaultPy") {
            if (WebLogin.isLoggedIn(call))
                UploadDefaultPyPost.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get("/average_turn_time") {
            if (WebLogin.isLoggedIn(call))
                AverageTurnTime.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get("/generate_new_card") {
            if (WebLogin.isLoggedIn(call))
                GenerateNewCard.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        post("/equip_card") {
            if (WebLogin.isLoggedIn(call))
                EquipCard.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        post("/unequip_card") {
            if (WebLogin.isLoggedIn(call))
                UnequipCard.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        post("/combine_card") {
            if (WebLogin.isLoggedIn(call))
                CombineCard.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        post("/trash_card") {
            if (WebLogin.isLoggedIn(call))
                TrashCard.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        get("/inventory") {
            if (WebLogin.isLoggedIn(call))
                Inventory.answer(call)
            else
                call.respondRedirect("/", permanent = false)
        }

        // From login screen
        post("/api/login-register") {
            LoginRegister.answer(call, simpleJwt)
        }

        get("/api/ping") {
            println("${TimeFormat.formattedTime} (api/ping) called")
            call.respondText { "{\"OK\": \"true\"}" }
        }

        authenticate {
            get("/api/test-login") {
                println("${TimeFormat.formattedTime} (api/test-login) ${call.principal<UserIdPrincipal>()?.name} called")
                call.respondText { "{\"OK\": \"true\"}" }
            }

            get("/api/logoff") {
                LogOff.answer(call)
            }

            get("/api/match") {
                Web.api.Match.answer(call)
            }

            get("/api/training") {
                Web.api.TrainingAlone.answer(call)
            }

            get("/api/trainingBot") {
                Web.api.TrainingBot.answer(call)
            }

            get("/api/end_play") {
                Web.api.EndPlay.answer(call)
            }

            get("/api/get_map") {
                Web.api.GetMap.answer(call)
            }

            get("/api/get_expand_map") {
                GetExpandMap.answer(call)
            }

            get("/api/get_shadow_map") {
                GetShadowMap.answer(call)
            }

            post("/api/entity-info") {
                EntityInfo.answer(call)
            }

            post("/api/entity-info-by-position") {
                EntityInfoByPosition.answer(call)
            }

            get("/api/entity-list-info") {
                EntityListInfo.answer(call)
            }

            get("/api/entity-changed-info") {
                EntityChangeInfo.answer(call)
            }

            get("/api/average_turn_time") {
                Web.api.AverageTurnTime.answer(call)
            }

            get("/api/user_info") {
                UserInfo.answer(call)
            }
        }
    }
}
