package Web.api

import Web.users.SimpleJWT
import Web.users.User
import Web.utils.TimeFormat
import Web.utils.WebLogin.users
import com.beust.klaxon.Klaxon
import io.ktor.application.ApplicationCall
import io.ktor.auth.UserIdPrincipal
import io.ktor.auth.principal
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveParameters
import io.ktor.response.respondText

object LoginRegister {

    private val klaxon = Klaxon()

    suspend fun answer (call: ApplicationCall, simpleJWT: SimpleJWT) {
        try {
            println("${TimeFormat.formattedTime} (api/login-register) ${call.principal<UserIdPrincipal>()?.name} called")
            val post = call.receiveParameters()

            if (post["user"]!!.length < 5 || post["password"]!!.length < 6) {
                throw IllegalArgumentException("Invalid User/Password")
            }

            val user = users.getOrPut(post["user"]) { User(post["user"]!!, post["password"]!!, User.users.last().id+1) }
            if (User.addUser(user))
                println("${TimeFormat.formattedTime} New user registered: ${Klaxon().toJsonString(user)}")
            if (user.password != post["password"]) error("Invalid credentials")
            val map = mapOf("token" to simpleJWT.sign(user.name))
            call.respondText { klaxon.toJsonString(map) }
            println("${TimeFormat.formattedTime} User logged in: ${Klaxon().toJsonString(user)}")
        } catch (e: Exception) {
            call.respondText(status = HttpStatusCode.Unauthorized) { "{\"ERROR\": \"${e.message}\"}" }
            e.printStackTrace()
            println("${TimeFormat.formattedTime} (api/login-register) error -> ${e.message}")
            return
        }
    }
}