package Web.api

import Game.entity.Entity
import Game.world.Dungeon
import Web.users.User
import Web.utils.TimeFormat
import Web.utils.WebLogin.users
import io.ktor.application.ApplicationCall
import io.ktor.auth.UserIdPrincipal
import io.ktor.auth.principal
import io.ktor.http.HttpStatusCode
import io.ktor.response.respondText

object EndPlay {
    suspend fun answer (call: ApplicationCall) {
        try {
            println("${TimeFormat.formattedTime} (api/end_play) ${call.principal<UserIdPrincipal>()?.name} requested to end it's current game")

            val removedDungeon = Dungeon.removeDungeonFromUserId(users[call.principal<UserIdPrincipal>()?.name]!!.id)

            val allEntities = mutableListOf<Entity>()
            removedDungeon.gridEntities.forEach {
                it.forEach {
                    if (it != null)
                        allEntities.add(it)
                }
            }
            val allUsers = mutableListOf<User>()
            allEntities.forEach {
                if (it.user != null && it.user!! !in allUsers)
                    allUsers.add(it.user!!)
            }

            // We remove all dungeon's participants from play (erase it's .py files)
            allUsers.forEach {
                it.inGame = false
            }

            println("${TimeFormat.formattedTime} (api/end_play) games from users: ${allUsers.joinToString { it.name }} have ended by ${call.principal<UserIdPrincipal>()?.name}'s request")

            call.respondText { "{\"OK\": \"true\"}" }
        }catch (e: Exception) {
            call.respondText(status = HttpStatusCode.Unauthorized) { "{\"ERROR\": \"${e.message}\"}" }
            e.printStackTrace()
            println("${TimeFormat.formattedTime} (api/end_play) ${call.principal<UserIdPrincipal>()?.name} gave an error -> ${e.message}")
        }
    }
}