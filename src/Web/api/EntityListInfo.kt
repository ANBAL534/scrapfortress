package Web.api

import Game.entity.Entity
import Game.entity.PlayerOwned
import Game.world.Coordinates
import Game.world.Dungeon
import Web.utils.TimeFormat
import Web.utils.WebLogin.users
import com.beust.klaxon.Klaxon
import io.ktor.application.ApplicationCall
import io.ktor.auth.UserIdPrincipal
import io.ktor.auth.principal
import io.ktor.http.HttpStatusCode
import io.ktor.response.respondText

object EntityListInfo {

    private val klaxon = Klaxon()

    suspend fun answer (call: ApplicationCall) {
        try {
            val user = users[call.principal<UserIdPrincipal>()?.name]!!
            val userEntities = Entity.findEntitiesByUserId(user.id)
            val dungeonFound = Dungeon.findDungeonFromEntity(userEntities[0])!!
            dungeonFound.dungeonLock.lock()

            // Initialize visible grid with nulls
            val gridVisibleFinal = mutableListOf<MutableList<Entity?>>()
            for (y in 0 until dungeonFound.gridEntities.size) {
                val mlist = mutableListOf<Entity?>()
                for (x in 0 until dungeonFound.gridEntities.size)
                    if (dungeonFound.gridEntities[y][x] !is PlayerOwned)
                        mlist.add(dungeonFound.gridEntities[y][x])
                    else
                        mlist.add(null)
                gridVisibleFinal.add(mlist)
            }

            // Filter out non-visible entities and add them to the final visible grid
            for (y in 0 until gridVisibleFinal.size)
                for (x in 0 until gridVisibleFinal[y].size)
                    if (dungeonFound.gridEntities[y][x] != null && userEntities.any { it.isAround(Coordinates.getCoordinates(x, y, 0)) })
                        gridVisibleFinal[y][x] = dungeonFound.gridEntities[y][x]

            val entities = mutableListOf<Entity>()
            for (y in 0 until gridVisibleFinal.size) {
                for (x in 0 until gridVisibleFinal[y].size)
                    if (gridVisibleFinal[y][x] != null)
                        entities.add(gridVisibleFinal[y][x]!!)
            }

            call.respondText { klaxon.toJsonString(entities) }
            dungeonFound.dungeonLock.unlock()
        }catch (e: IllegalStateException){
            call.respondText(status = HttpStatusCode.Unauthorized) { "{\"ERROR\": \"${e.message}\"}" }
            e.printStackTrace()
            println("${TimeFormat.formattedTime} (api/entity-list-info) error -> ${e.message}")
            val user = users[call.principal<UserIdPrincipal>()?.name]!!
            val userEntities = Entity.findEntitiesByUserId(user.id)
            val dungeonFound = Dungeon.findDungeonFromEntity(userEntities[0])!!
            dungeonFound.dungeonLock.unlock()
        }
    }
}