package Web.api

import Game.entity.Entity
import Game.world.Dungeon
import Web.utils.TimeFormat
import Web.utils.WebLogin.users
import com.beust.klaxon.Klaxon
import io.ktor.application.ApplicationCall
import io.ktor.auth.UserIdPrincipal
import io.ktor.auth.principal
import io.ktor.http.HttpStatusCode
import io.ktor.response.respondText

object EntityChangeInfo {

    private val klaxon = Klaxon()

    suspend fun answer (call: ApplicationCall) {
        try {
            val user = users[call.principal<UserIdPrincipal>()?.name]!!
            val userEntities = Entity.findEntitiesByUserId(user.id)
            val dungeonFound = Dungeon.findDungeonFromEntity(userEntities[0])!!
            dungeonFound.dungeonLock.lock()

            call.respondText { klaxon.toJsonString(dungeonFound.gridChanges[user]!!) }

            dungeonFound.gridChanges[user] = mutableListOf()
            dungeonFound.dungeonLock.unlock()
        }catch (e: IllegalStateException){
            call.respondText(status = HttpStatusCode.Unauthorized) { "{\"ERROR\": \"${e.message}\"}" }
            e.printStackTrace()
            println("${TimeFormat.formattedTime} (api/entity-list-info) error -> ${e.message}")

            val user = users[call.principal<UserIdPrincipal>()?.name]!!
            val userEntities = Entity.findEntitiesByUserId(user.id)
            val dungeonFound = Dungeon.findDungeonFromEntity(userEntities[0])!!
            dungeonFound.dungeonLock.unlock()
        }
    }
}