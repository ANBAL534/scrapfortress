package Web.api

import Web.users.User
import Web.utils.TimeFormat
import io.ktor.application.ApplicationCall
import io.ktor.auth.UserIdPrincipal
import io.ktor.auth.principal
import io.ktor.http.HttpStatusCode
import io.ktor.response.respondText

object LogOff {
    suspend fun answer (call: ApplicationCall) {
        try{
            println("${TimeFormat.formattedTime} (api/logoff) ${call.principal<UserIdPrincipal>()?.name} called")
            val user = User.users.find {
                it.name == call.principal<UserIdPrincipal>()?.name ?: error("No principal")
            }!!  // As this is an authenticated endpoint, it will always find the user
            call.respondText { "{\"OK\": \"true\"}" }
            println("${TimeFormat.formattedTime} ${user.name} logged off")
        }catch (e: IllegalStateException){
            call.respondText(status = HttpStatusCode.Unauthorized) { "{\"ERROR\": \"${e.message}\"}" }
            e.printStackTrace()
            println("${TimeFormat.formattedTime} (api/logoff) error -> ${e.message}")
        }
    }
}