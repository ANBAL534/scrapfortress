package Web.api

import Game.world.Dungeon
import Web.utils.TimeFormat
import Web.utils.WebLogin.users
import com.beust.klaxon.Klaxon
import io.ktor.application.ApplicationCall
import io.ktor.auth.UserIdPrincipal
import io.ktor.auth.principal
import io.ktor.http.HttpStatusCode
import io.ktor.response.respondText

object AverageTurnTime {

    private val klaxon = Klaxon()

    suspend fun answer (call: ApplicationCall) {
        try {
            val user = users[call.principal<UserIdPrincipal>()?.name]!!
            val dungeonFound = Dungeon.removeDungeonFromUserId(user.id)!!

            call.respondText { klaxon.toJsonString(dungeonFound.averageTurnTime) }
        }catch (e: IllegalStateException){
            call.respondText(status = HttpStatusCode.Unauthorized) { "{\"ERROR\": \"${e.message}\"}" }
            e.printStackTrace()
            println("${TimeFormat.formattedTime} (api/average_turn_time) error -> ${e.message}")
        }
    }
}