package Web.api

import Game.utils.Matchmaking.matchTrainingWithBot
import Web.utils.TimeFormat
import Web.utils.WebLogin.users
import io.ktor.application.ApplicationCall
import io.ktor.auth.UserIdPrincipal
import io.ktor.auth.principal
import io.ktor.http.HttpStatusCode
import io.ktor.response.respondText

object TrainingBot {
    suspend fun answer (call: ApplicationCall) {
        try {
            println("${TimeFormat.formattedTime} (api/trainingBot) ${call.principal<UserIdPrincipal>()?.name} called")
            val match = matchTrainingWithBot(users[call.principal<UserIdPrincipal>()?.name]!!)
            if (match) {
                call.respondText {
                    "{\"found\": \"true\"}"
                }
            } else {
                call.respondText {
                    "{\"found\": \"false\"}"
                }
            }
        }catch (e: Exception) {
            call.respondText(status = HttpStatusCode.Unauthorized) { "{\"ERROR\": \"${e.message}\"}" }
            e.printStackTrace()
            println("${TimeFormat.formattedTime} (api/trainingBot) ${call.principal<UserIdPrincipal>()?.name} gave an error -> ${e.message}")
        }
    }
}