package Web.pages

import Game.utils.Matchmaking.matchTrainingWithBot
import Web.utils.WebLogin
import Web.utils.WebLogin.users
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import kotlinx.html.*

object TrainingBot {
    /**
     * Make sure to only call this method on a logged in user
     */
    suspend fun answer(call: ApplicationCall) {
        val match = matchTrainingWithBot(users[WebLogin.getSessionName(call)]!!)  // As it's against a bot, it will always return true
        if (match) {
            call.respondHtml {
                head {
                    // Bootstrap meta+css
                    meta {
                        charset="utf-8"
                    }
                    meta {
                        name="viewport"
                        content="width=device-width, initial-scale=1, shrink-to-fit=no"
                    }
                    link {
                        rel="stylesheet"
                        href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
                    }
                }
                body {
                    div {
                        style =
                            """
                                    width: 600px;
                                    height: 400px;

                                    position:absolute; /*it can be fixed too*/
                                    left:0; right:0;
                                    top:0; bottom:0;
                                    margin:auto;

                                    /*this to solve "the content will not be cut when the window is smaller than the content": */
                                    max-width:100%;
                                    max-height:100%;
                                    overflow:auto;
                                    """.trimIndent()
                        ul (classes = "list-group") {
                            li (classes = "list-group-item") {
                                h1 {
                                    style = "text-align: center"
                                    +"Scrap Fortress"
                                }
                                h2 {
                                    style = "text-align: center"
                                    +"Finishing Loading..."
                                }
                            }
                        }
                    }

                    script { +"""
                                var delay = ( function() {
                                    var timer = 0;
                                    return function(callback, ms) {
                                        clearTimeout (timer);
                                        timer = setTimeout(callback, ms);
                                    };
                                })();
                                delay(function(){
                                    window.location.href = '/play';
                                }, 1000 ); // end delay
                            """.trimIndent() }

                    // Bootstrap JS
                    script { src="https://code.jquery.com/jquery-3.3.1.slim.min.js" }
                    script { src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" }
                    script { src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" }
                }
            }
        }
    }
}