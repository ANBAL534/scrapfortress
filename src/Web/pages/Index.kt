package Web.pages

import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import kotlinx.html.*

object Index {
    suspend fun answer(call: ApplicationCall) {
        call.respondHtml {
            head {
                // Bootstrap meta+css
                meta {
                    charset="utf-8"
                }
                meta {
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                }
                link {
                    rel="stylesheet"
                    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
                }
            }
            body {
                div {
                    style =
                        """
                                width: 600px;
                                height: 400px;

                                position:absolute; /*it can be fixed too*/
                                left:0; right:0;
                                top:0; bottom:0;
                                margin:auto;

                                /*this to solve "the content will not be cut when the window is smaller than the content": */
                                max-width:100%;
                                max-height:100%;
                                overflow:auto;
                                """.trimIndent()
                    ul (classes = "list-group") {
                        li (classes = "list-group-item") {
                            h1 { +"Scrap Fortress" }
                            form("/login", classes = "form-group", encType = FormEncType.multipartFormData, method = FormMethod.post) {
                                acceptCharset = "utf-8"
                                p {
                                    label { +"Username: " }
                                    textInput (classes = "form-control") { name = "username" }
                                }
                                p {
                                    label { +"Password: " }
                                    passwordInput (classes = "form-control") { name = "password" }
                                }
                                p {
                                    submitInput (classes = "btn btn-primary btn-lg btn-block") {
                                        name = "Login"
                                        value = "Login / Register"
                                    }
                                }
                                if ( call.request.queryParameters["login"] != null )
                                    p {
                                        small (classes = "form-text text-muted") {
                                            +"Erroneous login credentials (Wrong Username/Password)"
                                        }
                                    }
                            }
                        }
                    }
                }

                // Bootstrap JS
                script { src="https://code.jquery.com/jquery-3.3.1.slim.min.js" }
                script { src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" }
                script { src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" }
            }
        }
    }

}