package Web.pages

import Web.utils.WebLogin
import Web.utils.WebLogin.users
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import kotlinx.html.*
import java.util.*
import kotlin.math.roundToInt

object Inventory {
    suspend fun answer (call: ApplicationCall) {
        val user = users[WebLogin.getSessionName(call)]!!

        call.respondHtml {
            head {
                // Bootstrap meta+css
                meta {
                    charset="utf-8"
                }
                meta {
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                }
                link {
                    rel="stylesheet"
                    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
                }
            }
            body {
                div {
                    style =
                        """
                                width: 70%;
                                height: 70%;

                                position:absolute; /*it can be fixed too*/
                                left:0; right:0;
                                top:0; bottom:0;
                                margin:auto;

                                /*this to solve "the content will not be cut when the window is smaller than the content": */
                                max-width:100%;
                                max-height:100%;
                                overflow:auto;
                                """.trimIndent()
                    ul (classes = "list-group") {
                        li (classes = "list-group-item") {
                            h1 { +"Scrap Fortress - ${WebLogin.getSessionName(call)}" }
                            br()
                            h2 { +"Inventory" }
                            hr {  }
                            div {
                                p {
                                    a(href = "/menu") { +"<< Back to the Menu" }
                                }
                                p {
                                    a(href = "/generate_new_card") { +"Generate a new card" }
                                }
                                hr {  }
                                p {
                                    +"Cards: ${user.cards.size}"
                                }
                                if (user.toCombineCards.isNotEmpty()) {
                                    hr {  }
                                    p {
                                        +"To Combine Card: ${user.toCombineCards.first()!!.cardHash}"
                                    }
                                }
                                hr {  }
                                hr {  }
                                p {
                                    div (classes = "form-group") {
                                        try {
                                            div {
                                                p {
                                                    val bonusList = user.equippedCards.first { it != null }!!.bonus.toMutableMap()
                                                    var cardIndex = 1  // Skip first card as we are using it of base

                                                    b {
                                                        +"Total effects from equipped cards: "
                                                    }
                                                    br()

                                                    // Reset al locked bonuses for our base
                                                    bonusList.toList().forEachIndexed { bonusIndex, pair ->
                                                        if (!user.equippedCards.first { it != null }!!.bonusLock[bonusList.keys.elementAt(bonusIndex)]!!) {
                                                            bonusList[bonusList.keys.elementAt(bonusIndex)] = 1.0
                                                        }
                                                    }

                                                    // Compute the base with the other equipped cards
                                                    user.equippedCards.filter { it != null && it != user.equippedCards.first { it != null }!! }.toList().forEachIndexed { _, _ ->
                                                        bonusList.toList().forEachIndexed { bonusIndex, pair ->
                                                            bonusList[bonusList.keys.elementAt(bonusIndex)] =
                                                                bonusList[bonusList.keys.elementAt(bonusIndex)]!! * (if (user.equippedCards[cardIndex] != null && user.equippedCards[cardIndex]!!.bonusLock[bonusList.keys.elementAt(bonusIndex)]!!) {
                                                                    user.equippedCards[cardIndex]!!.bonus[bonusList.keys.elementAt(bonusIndex)]!!
                                                                } else {
                                                                    1.0
                                                                })
                                                        }
                                                        cardIndex++
                                                    }

                                                    bonusList.toList().forEach { bonusPair ->
                                                        if (bonusPair.second != 1.0) {
                                                            when {
                                                                bonusPair.second >= 1.0
                                                                        && !((bonusPair.first == "neededEnergyToMoveBonus")
                                                                        || (bonusPair.first == "neededEnergyToBuildBonus")
                                                                        || (bonusPair.first == "energyOreConsumptionBonus")
                                                                        || (bonusPair.first == "neededEnergyToMineBonus")
                                                                        || (bonusPair.first == "neededEnergyToAttackBonus")
                                                                        || (bonusPair.first == "energyNeededForDroneBonus")
                                                                        || (bonusPair.first == "energyToManufactureBonus"))-> {
                                                                    div {
                                                                        style = "color:green;"
                                                                        +"${bonusPair.first}: ${((bonusPair.second-1.0)*100).roundToInt()}% (${bonusPair.second})"
                                                                    }
                                                                }
                                                                bonusPair.second < 1.0  // Some bonuses are better with reduction values (<1)
                                                                        && ((bonusPair.first == "neededEnergyToMoveBonus")
                                                                        || (bonusPair.first == "neededEnergyToBuildBonus")
                                                                        || (bonusPair.first == "energyOreConsumptionBonus")
                                                                        || (bonusPair.first == "neededEnergyToMineBonus")
                                                                        || (bonusPair.first == "neededEnergyToAttackBonus")
                                                                        || (bonusPair.first == "energyNeededForDroneBonus")
                                                                        || (bonusPair.first == "energyToManufactureBonus")) -> {
                                                                    div {
                                                                        style = "color:green;"
                                                                        +"${bonusPair.first}: ${((bonusPair.second-1.0)*100).roundToInt()}% (${bonusPair.second})"
                                                                    }
                                                                }
                                                                else -> {
                                                                    div {
                                                                        style = "color:red;"
                                                                        +"${bonusPair.first}: ${((bonusPair.second-1.0)*100).roundToInt()}% (${bonusPair.second})"
                                                                    }
                                                                }
                                                            }
                                                            br()
                                                        }
                                                    }
                                                }
                                            }
                                            hr { }
                                        } catch (e: NoSuchElementException) {
                                            // Ignored
                                        }
                                        for (card in user.cards) {
                                            div {
                                                p {
                                                    if (card in user.equippedCards)
                                                        +"[EQUIPPED] "
                                                    +"Hash: ${card.cardHash}"
                                                }
                                                p {
                                                    +"Rarity: ${card.rarity}"
                                                }
                                                for (bonusPair in card.bonus.toList()) {
                                                    if (card.bonusLock[bonusPair.first]!!) {  // Comment this to show locked bonuses in inventary
                                                        label {
                                                            if (!card.bonusLock[bonusPair.first]!!){
                                                                div {
                                                                    style = "color:orange;"
                                                                    + "[LOCKED] "
                                                                }
                                                            }
                                                            when {
                                                                bonusPair.second >= 1.0
                                                                        && !((bonusPair.first == "neededEnergyToMoveBonus")
                                                                        || (bonusPair.first == "neededEnergyToBuildBonus")
                                                                        || (bonusPair.first == "energyOreConsumptionBonus")
                                                                        || (bonusPair.first == "neededEnergyToMineBonus")
                                                                        || (bonusPair.first == "neededEnergyToAttackBonus")
                                                                        || (bonusPair.first == "energyNeededForDroneBonus")
                                                                        || (bonusPair.first == "energyToManufactureBonus"))-> {
                                                                    div {
                                                                        style = "color:green;"
                                                                        +"${bonusPair.first}: ${((bonusPair.second-1.0)*100).roundToInt()}% (${bonusPair.second})"
                                                                    }
                                                                }
                                                                bonusPair.second < 1.0  // Some bonuses are better with reduction values (<1)
                                                                        && ((bonusPair.first == "neededEnergyToMoveBonus")
                                                                        || (bonusPair.first == "neededEnergyToBuildBonus")
                                                                        || (bonusPair.first == "energyOreConsumptionBonus")
                                                                        || (bonusPair.first == "neededEnergyToMineBonus")
                                                                        || (bonusPair.first == "neededEnergyToAttackBonus")
                                                                        || (bonusPair.first == "energyNeededForDroneBonus")
                                                                        || (bonusPair.first == "energyToManufactureBonus")) -> {
                                                                    div {
                                                                        style = "color:green;"
                                                                        +"${bonusPair.first}: ${((bonusPair.second-1.0)*100).roundToInt()}% (${bonusPair.second})"
                                                                    }
                                                                }
                                                                else -> {
                                                                    div {
                                                                        style = "color:red;"
                                                                        +"${bonusPair.first}: ${((bonusPair.second-1.0)*100).roundToInt()}% (${bonusPair.second})"
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        br()
                                                    }
                                                }
                                                br()
                                            }
                                            if (card in user.equippedCards) {
                                                form(
                                                    "/unequip_card",
                                                    method = FormMethod.post
                                                ) {
                                                    acceptCharset = "utf-8"

                                                    label { +"Slot: " }
                                                    for (i in 1..5) {
                                                        radioInput {
                                                            disabled = true
                                                            id = "slot$i"
                                                            name = "slot"
                                                            value = "$i"
                                                            if (i == user.equippedCards.indexOf(card)+1) {
                                                                checked = true
                                                            }
                                                            +" $i "
                                                        }
                                                    }
                                                    br()
                                                    radioInput {
                                                        id = "unequip-${card.cardHash}"
                                                        name = "hash"
                                                        value = card.cardHash
                                                        checked = true
                                                        style = "display: none;"
                                                    }
                                                    submitInput ( classes = "btn btn-danger btn-sm btn-block" ) { value = "Unequip" }
                                                }
                                            } else {
                                                form(
                                                    "/equip_card",
                                                    method = FormMethod.post
                                                ) {
                                                    acceptCharset = "utf-8"

                                                    label { +"Slot: " }
                                                    for (i in 1..5) {
                                                        radioInput {
                                                            id = "slot$i"
                                                            name = "slot"
                                                            value = "$i"
                                                            if (i == user.equippedCards.indexOf(card)+1) {
                                                                checked = true
                                                            }
                                                            +" $i "
                                                        }
                                                    }
                                                    br()
                                                    radioInput {
                                                        id = "equip-${card.cardHash}"
                                                        name = "hash"
                                                        value = card.cardHash
                                                        checked = true
                                                        style = "display: none;"
                                                    }
                                                    submitInput ( classes = "btn btn-secondary btn-sm btn-block" ) { value = "Equip" }
                                                }
                                            }
                                            form(
                                                "/combine_card",
                                                method = FormMethod.post
                                            ) {
                                                radioInput {
                                                    id = "combine-${card.cardHash}"
                                                    name = "hash"
                                                    value = card.cardHash
                                                    checked = true
                                                    style = "display: none;"
                                                }
                                                submitInput ( classes = "btn btn-secondary btn-sm btn-block" ) {
                                                    if (card in user.toCombineCards)
                                                        style = "visibility:hidden;"
                                                    value = "Combine"
                                                }
                                            }
                                            form(
                                                "/trash_card",
                                                method = FormMethod.post
                                            ) {
                                                radioInput {
                                                    id = "trash-${card.cardHash}"
                                                    name = "hash"
                                                    value = card.cardHash
                                                    checked = true
                                                    style = "display: none;"
                                                }
                                                submitInput ( classes = "btn btn-secondary btn-sm btn-block" ) { value = "Trash" }
                                            }
                                            hr()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // Bootstrap JS
                script { src="https://code.jquery.com/jquery-3.3.1.slim.min.js" }
                script { src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" }
                script { src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" }
            }
        }
    }
}