package Web.pages

import Game.entity.EntityCode
import Web.utils.WebLogin
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import kotlinx.html.*

object UploadDefaultPyGet {
    suspend fun answer (call: ApplicationCall) {
        call.respondHtml {
            head {
                // Bootstrap meta+css
                meta {
                    charset="utf-8"
                }
                meta {
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                }
                link {
                    rel="stylesheet"
                    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
                }
            }
            body {
                div {
                    style =
                        """
                                width: 70%;
                                height: 70%;

                                position:absolute; /*it can be fixed too*/
                                left:0; right:0;
                                top:0; bottom:0;
                                margin:auto;

                                /*this to solve "the content will not be cut when the window is smaller than the content": */
                                max-width:100%;
                                max-height:100%;
                                overflow:auto;
                                """.trimIndent()
                    ul (classes = "list-group") {
                        li (classes = "list-group-item") {
                            h1 { +"Scrap Fortress - ${WebLogin.getSessionName(call)}" }
                            br()
                            h2 { +"Set default scripts" }
                            hr {  }
                            div {
                                p {
                                    a(href = "/menu") { +"<< Back to the Menu" }
                                }
                                hr {  }
                                p {
                                    if ( call.request.queryParameters["upload"] != null ) {
                                        p {
                                            small (classes = "alert-success") {
                                                +"Upload default script for ${call.request.queryParameters["upload"]} was successfull."
                                            }
                                        }
                                        hr {  }
                                    }

                                    for (entity in EntityCode.values().dropLast(1).drop(2)) {  // TODO modify this so we can handle better what entity classes are shown for default scripts
                                        div (classes = "form-group") {
                                            form(
                                                "/uploadDefaultPy",
                                                encType = FormEncType.multipartFormData,
                                                method = FormMethod.post
                                            ) {
                                                acceptCharset = "utf-8"

                                                label {
                                                    htmlFor = "name"; +"Type: ${entity.entityName}"
                                                    textInput {
                                                        name = "name"
                                                        id = "name"
                                                        value = entity.entityName
                                                        style = "display: None;"
                                                    }
                                                }
                                                br()
                                                fileInput (classes = "form-control-file") { name = "file" }
                                                small (classes = "form-text text-muted") { +"Upload only .py files with a max size of 1MB" }
                                                br()

                                                submitInput ( classes = "btn btn-secondary btn-sm btn-block" ) { value = "Upload" }
                                            }
                                        }
                                        hr()
                                    }
                                }
                            }
                        }
                    }
                }

                // Bootstrap JS
                script { src="https://code.jquery.com/jquery-3.3.1.slim.min.js" }
                script { src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" }
                script { src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" }
            }
        }
    }
}