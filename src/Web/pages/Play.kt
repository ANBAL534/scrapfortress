package Web.pages

import Game.entity.EntityCode
import Web.utils.WebLogin
import Web.utils.WebLogin.users
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import io.ktor.response.respondRedirect
import kotlinx.html.*

object Play {
    suspend fun answer(call: ApplicationCall) {
        if (!users[WebLogin.getSessionName(call)]!!.inGame)
            call.respondRedirect(permanent = false, url = "/match")
        call.respondHtml {
            head {
                // Bootstrap meta+css
                meta {
                    charset="utf-8"
                }
                meta {
                    name="viewport"
                    content="width=device-width, height=device-height, initial-scale=1, shrink-to-fit=yes"
                }
                link {
                    rel="stylesheet"
                    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
                }
                script { src="/static/updateCanvas.js" }
                /*
                * This script doesn't want to load when linking the file because *javascript* so we append it.
                */
                script { +"""
function modifyListStats(parsed) {
    var c = document.getElementById('statsList');
    if (c.innerHTML !== parsed)
        c.innerHTML = parsed;
}

function updateStats() {
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState === XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
            if (xmlhttp.status === 200) {
                modifyListStats(xmlhttp.responseText);
            }
            else if (xmlhttp.status === 400) {
                alert('There was an error 400');
            }
            else {
                alert('something else other than 200 was returned');
            }
        }
    };

    xmlhttp.open('GET', '/entityStats', true);
    xmlhttp.send();
}

function bucleFS() {
    var delay = ( function() {
        var timer = 0;
        return function(callback, ms) {
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();
    delay(function(){
        updateStats();
        bucleFS();
    }, 600 ); // end delay
}

bucleFS();
                        """.trimIndent() }
                script { src="/static/updateForm.js" }
                script { src="/static/elCreadorDeCSSEsPutoSubnormal.js" }
                script { src="/static/moveView.js" }
                script { src="/static/updateTurnTime.js" }
                // Bootstrap JS
                script { src="https://code.jquery.com/jquery-3.3.1.slim.min.js" }
                script { src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" }
                script { src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" }
            }
            body {
                style =
                    """
                            height: 100%;
                            padding-top: 30px;
                            padding-right: 30px;
                            padding-left: 30px;
                            """.trimIndent()  // See elCreadorDeCSSEsPutoSubnormal.js

                for (eCode in EntityCode.values()) {
                    img {
                        id = "${eCode.name}-letter"
                        src = "/static/${eCode.name}-letter.png"
                        style = "display: None"
                    }
                }
                // Also add reset image
                img {
                    id = "reset-image"
                    src = "/static/resetBackground.png"
                    style = "display: None"
                }
                // Also add enemy-overlay image
                img {
                    id = "enemy-overlay"
                    src = "/static/enemy-overlay.png"
                    style = "display: None"
                }
                // Also add enemy-last-seen image
                img {
                    id = "enemy-last-seen"
                    src = "/static/enemy-last-seen.png"
                    style = "display: None"
                }

                div(classes = "container-fluid h-100") {
                    div (classes = "row") {
                        // Average turn time
                        div (classes = "col-12") {
                            p {
                                id = "turn_time"
                            }
                        }
                    }
                    div (classes = "row h-100") {
                        // Mapa en la parte izquierda
                        div (classes = "col-7") {
//                                    style = "background: black;"
                            canvas {
                                id = "map"
                            }
                        }
                        div (classes = "col-2") {
                            div (classes = "row h-100") {
                                // Stats/Listado de entidades
                                style="width: 100%; height: 100%; overflow-y: scroll;"
                                id = "statsList"
                            }
                        }
                        // Columna con controles y formulario en la parte derecha
                        div (classes = "col-3") {
                            div (classes = "row") {
                                style = "height: 200px"
                                // Controles camara en el mapa
                                // Primera fila
                                div (classes = "col-3") {
                                    button(classes = "btn btn-dark btn-lg btn-block") {
                                        onClick = "moveView('lessFow')"
                                        +" - "
                                    }
                                }
                                div (classes = "col-6") {
                                    button(classes = "btn btn-secondary btn-lg btn-block") {
                                        onClick = "moveView('up')"
                                        +" ▲ "
                                    }
                                }
                                div (classes = "col-3") {
                                    button(classes = "btn btn-dark btn-lg btn-block") {
                                        onClick = "moveView('moreFow')"
                                        +" + "
                                    }
                                }

                                // Segunda fila
                                div (classes = "col-4") {
                                    button(classes = "btn btn-secondary btn-lg btn-block") {
                                        onClick = "moveView('left')"
                                        +" ◀ "
                                    }
                                }
                                div (classes = "col-4") {
                                    button(classes = "btn btn-secondary btn-lg btn-block") {
                                        onClick = "moveView('down')"
                                        +" ▼ "
                                    }
                                }
                                div (classes = "col-4") {
                                    button(classes = "btn btn-secondary btn-lg btn-block") {
                                        onClick = "moveView('right')"
                                        +" ▶ "
                                    }
                                }

                                // Tercera fila
                                div (classes = "col-6") {
                                    button(classes = "btn btn-secondary btn-lg btn-block") {
                                        onClick = "moveView('resume')"
                                        +" Resume "
                                    }
                                }
                                div (classes = "col-6") {
                                    button(classes = "btn btn-secondary btn-lg btn-block") {
                                        onClick = "moveView('pause')"
                                        +" Pause "
                                    }
                                }

                                // Cuarta fila
                                div (classes = "col-12") {
                                    button (classes = "btn btn-dark btn-lg btn-block") {
                                        onClick = "window.location.assign('/end_play')"
                                        +"End Game"
                                    }
                                }
                            }
                            div (classes = "row") {
                                // Formulario/Listado de entidades
                                style="width: 100%; height: 100%; overflow-y: scroll;"
                                id = "formList"
                            }
                        }
                    }
                }
            }
        }
    }
}