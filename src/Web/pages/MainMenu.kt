package Web.pages

import Web.utils.WebLogin
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import kotlinx.html.*

object MainMenu {
    suspend fun answer(call: ApplicationCall) {
        call.respondHtml {
            head {
                // Bootstrap meta+css
                meta {
                    charset="utf-8"
                }
                meta {
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                }
                link {
                    rel="stylesheet"
                    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
                }
            }
            body {
                div {
                    id = "menu-block"
                    style =
                        """
                                width: 600px;
                                height: 400px;

                                position:absolute; /*it can be fixed too*/
                                left:0; right:0;
                                top:0; bottom:0;
                                margin:auto;

                                /*this to solve "the content will not be cut when the window is smaller than the content": */
                                max-width:100%;
                                max-height:100%;
                                overflow:auto;
                                
                                display: Block;
                                """.trimIndent()
                    ul (classes = "list-group") {
                        li (classes = "list-group-item") {
                            h1 { +"Scrap Fortress - ${WebLogin.getSessionName(call)}" }
                            hr {  }
                            div {
                                p {
                                    a(href = "/match") {
                                        onClick = "showLoading()"
                                        +"Match 1v1"
                                    }
                                }
                                p {
                                    a(href = "/training") {
                                        onClick = "showLoading()"
                                        +"Training Match (Alone)"
                                    }
                                }
                                p {
                                    a(href = "/trainingBot") {
                                        onClick = "showLoading()"
                                        +"Training Match (vs Bot)"
                                    }
                                }
                                p {
                                    a(href = "/uploadDefaultPy") {
                                        onClick = "showLoading()"
                                        +"Set Default Scripts"
                                    }
                                }
                                p {
                                    a(href = "/inventory") {
                                        onClick = "showLoading()"
                                        +"Show Inventory"
                                    }
                                }
                            }
                        }
                    }
                }

                div {
                    id = "loading-block"
                    style =
                        """
                                    width: 600px;
                                    height: 400px;

                                    position:absolute; /*it can be fixed too*/
                                    left:0; right:0;
                                    top:0; bottom:0;
                                    margin:auto;

                                    /*this to solve "the content will not be cut when the window is smaller than the content": */
                                    max-width:100%;
                                    max-height:100%;
                                    overflow:auto;
                                    
                                    display: None;
                                    """.trimIndent()
                    ul (classes = "list-group") {
                        li (classes = "list-group-item") {
                            h1 {
                                style = "text-align: center"
                                +"Scrap Fortress"
                            }
                            h2 {
                                style = "text-align: center"
                                +"Loading..."
                            }
                        }
                    }
                }

                script {
                    +"""
                                function showLoading() {
                                    document.getElementById('menu-block').style.display = 'None';
                                    document.getElementById('loading-block').style.display = 'Block';
                                }
                            """.trimIndent()
                }

                // Bootstrap JS
                script { src="https://code.jquery.com/jquery-3.3.1.slim.min.js" }
                script { src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" }
                script { src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" }
            }
        }
    }
}