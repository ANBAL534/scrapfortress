package Web.pages

import Web.users.SimpleJWT
import Web.users.User
import Web.users.WebSession
import Web.utils.TimeFormat
import Web.utils.WebLogin.loggedUsers
import Web.utils.WebLogin.users
import Web.utils.sha256
import com.beust.klaxon.Klaxon
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveParameters
import io.ktor.response.respondRedirect
import io.ktor.response.respondText
import io.ktor.sessions.sessions
import io.ktor.sessions.set
import kotlinx.html.*
import java.util.*

object Login {
    suspend fun answer(call: ApplicationCall, simpleJWT: SimpleJWT) {
        try {
            val parameters = call.receiveParameters()
            val user = users.getOrPut(parameters["username"]) {
                val lastUser = try {
                    User.users.last()
                } catch (e: NoSuchElementException){
                    null
                }
                User(parameters["username"]!!, parameters["password"]!!.sha256(), (lastUser?.id ?: 0)+1)
            }
            if (User.addUser(user))
                println("${TimeFormat.formattedTime} New user registered: ${Klaxon().toJsonString(user)}")
            if (user.password != parameters["password"]!!.sha256())
                call.respondRedirect (permanent = false, url = "/?login=error")
            val map = mapOf("token" to simpleJWT.sign(user.name))
            val session = WebSession(name = user.name, value = map["token"] ?: error("${user.name} had an invalid token"))
            call.sessions.set(session)
            loggedUsers.getOrPut(user.name) { session }
            loggedUsers[user.name] = session
            call.respondHtml {
                head {
                    // Bootstrap meta+css
                    meta {
                        charset="utf-8"
                    }
                    meta {
                        name="viewport"
                        content="width=device-width, initial-scale=1, shrink-to-fit=no"
                    }
                    link {
                        rel="stylesheet"
                        href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
                    }
                }
                body {
                    div {
                        style =
                            """
                                width: 600px;
                                height: 400px;

                                position:absolute; /*it can be fixed too*/
                                left:0; right:0;
                                top:0; bottom:0;
                                margin:auto;

                                /*this to solve "the content will not be cut when the window is smaller than the content": */
                                max-width:100%;
                                max-height:100%;
                                overflow:auto;
                                """.trimIndent()
                        ul (classes = "list-group") {
                            li (classes = "list-group-item") {
                                h1 {
                                    style = "text-align: center"
                                    +"Scrap Fortress"
                                }
                                h2 {
                                    style = "text-align: center"
                                    +"Redirecting to Menu..."
                                }
                                script { +"""
                                                window.location.href = '/menu';
                                              """.trimIndent() }
                            }
                        }
                    }

                    // Bootstrap JS
                    script { src="https://code.jquery.com/jquery-3.3.1.slim.min.js" }
                    script { src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" }
                    script { src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" }
                }
            }
            println("${TimeFormat.formattedTime} User logged in: ${Klaxon().toJsonString(user)}")
        }catch (e: IllegalStateException){
            call.respondText(status = HttpStatusCode.Unauthorized) { "{\"ERROR\": \"${e.message}\"}" }
            println("${TimeFormat.formattedTime} ${e.message}")
        }
    }
}