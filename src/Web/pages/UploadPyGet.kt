package Web.pages

import Game.entity.Entity
import Game.entity.EntityCode
import Web.utils.WebLogin
import Web.utils.WebLogin.users
import io.ktor.application.ApplicationCall
import io.ktor.response.respondText
import kotlinx.html.*
import kotlinx.html.stream.createHTML

object UploadPyGet {
    suspend fun answer (call: ApplicationCall) {
        val html =
            createHTML().div {
                hr()
                val entities = Entity.findEntitiesByUserId(users[WebLogin.getSessionName(call)]!!.id).sortedBy { it.name }
                for (entity in entities) {
                    div (classes = "form-group") {
                        form(
                            "/uploadPy",
                            encType = FormEncType.multipartFormData,
                            method = FormMethod.post
                        ) {
                            acceptCharset = "utf-8"

                            label {
                                htmlFor = "name"; +"Name: "
                                textInput (classes = "form-control") {
                                    name = "name"
                                    id = "name"
                                    value = entity.name
                                }
                            }
                            textInput {
                                name = "code"
                                id = "code"
                                val realClassEntity = EntityCode.values().find { it.kclass?.isInstance(entity) ?: false }!!.kclass
                                value = "${realClassEntity!!.simpleName}-${entity.id}"
                                style = "display: None;"
                            }
                            br()
                            fileInput (classes = "form-control-file") { name = "file" }
                            small (classes = "form-text text-muted") { +"Upload only .py files with a max size of 1MB" }
                            br()

                            submitInput ( classes = "btn btn-secondary btn-sm btn-block" ) { value = "Upload" }
                        }
                    }
                    hr()
                }
            }
        call.respondText { html }
    }
}