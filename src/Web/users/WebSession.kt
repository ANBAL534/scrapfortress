package Web.users

data class WebSession(val name: String, val value: String)