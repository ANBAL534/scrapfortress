package Web.users

import Game.card.Card
import Game.entity.Entity
import Game.world.Coordinates
import Game.world.Dungeon
import com.beust.klaxon.Json
import java.io.File
import kotlin.math.abs

data class User(val name: String, @Json(ignored = true) val password: String, val id: Int){

    @Json(ignored = true)
    val userPyPath = "python${File.separator}upload${File.separator}$name"  // TODO Change this route in production

    @Json(ignored = true)
    var inGame: Boolean = false
        get() = !Entity.findEntitiesByUserId(id).isEmpty()
        set(value) {
            if (!value) {
                // Game just finished, remove py files
                File(userPyPath).listFiles()!!.forEach {
                    if (!it.nameWithoutExtension.contains("-DEFAULT-"))
                        it.delete()
                }

                // Stop caller.py process
                // This action must be done where game end is called (Server.kt)

                // TODO this is just a temporal workaround, do this correctly
                if (name.startsWith("Bot-", ignoreCase = true))
                    File(userPyPath).delete()
            }
            field = value
        }

    @Json(ignored = true)
    @Volatile
    var viewPoint = Coordinates.getCoordinates(0, 0, 0)
        set(value) {
            field =
                if (inGame) {
                    if (value.x-fow >= 0 &&
                        value.x+fow <= Dungeon.findDungeonFromEntity(Entity.findEntitiesByUserId(id)[0])!!.size &&
                        value.y-fow >= 0 &&
                        value.y+fow <= Dungeon.findDungeonFromEntity(Entity.findEntitiesByUserId(id)[0])!!.size)
                        value
                    else
                        field
                }else
                    value
        }

    @Json(ignored = true)
    var fow = 30  // Remember to reset this number after each game
        set(value) {
            field = when {
                value < 1 -> 1
                inGame -> {
                    if (viewPoint.x-value > 0 &&
                        viewPoint.x+value < Dungeon.findDungeonFromEntity(Entity.findEntitiesByUserId(id)[0])!!.size &&
                        viewPoint.y-value > 0 &&
                        viewPoint.y+value < Dungeon.findDungeonFromEntity(Entity.findEntitiesByUserId(id)[0])!!.size)
                        value
                    else 
                        field
                }
                else -> value
            }
        }

    @Volatile
    var computedActions = 0

    val cards = mutableListOf<Card>()
    val equippedCards = mutableListOf<Card?>(null, null, null, null, null)
    var toCombineCards = mutableListOf<Card?>()

    /**
     * Checks if provided position is within fow of this user's viewpoint
     */
    fun isAroundViewPoint(coordinates: Coordinates): Boolean {
        val difference: Int
        val differenceX = abs(viewPoint.x - coordinates.x)
        val differenceY = abs(viewPoint.y - coordinates.y)
        difference =
                if (differenceX > differenceY)
                    differenceX
                else
                    differenceY
        if (difference <= fow)
            return true
        return false
    }

    companion object {
        @Json(ignored = true)
        var users: List<User> = listOf()
            private set(value) { field = value.sortedBy { it.id } }

        /**
         * Will add specified user to users List.
         *
         * @return false if it already exists and hasn't been added or true if it ahs been added
         */
        fun addUser(user: User): Boolean{
            if (users.find { it.id == user.id || it.name == user.name } != null)
                return false
            val users = users.toMutableList()
            users.add(user)
            Companion.users = users
            return true
        }
    }

}
